<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblproduct".
 *
 * @property int $pid
 * @property string $pname
 * @property int $cname
 * @property string $productcode
 * @property int $price
 * @property int $saleprice
 * @property int $quantity
 * @property int $porder
 * @property string $pstatus
 * @property string $created_at
 * @property string $updated_at
 */
class Tblproduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tblproduct';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pname', 'cname', 'productcode', 'price', 'saleprice', 'quantity', 'porder', 'pstatus'], 'required'],
            [['cname', 'price', 'saleprice', 'quantity', 'porder'], 'integer'],
            [['pstatus'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['pname'], 'string', 'max' => 50],
            [['productcode'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pid' => 'Pid',
            'pname' => 'Pname',
            'cname' => 'Cname',
            'productcode' => 'Productcode',
            'price' => 'Price',
            'saleprice' => 'Saleprice',
            'quantity' => 'Quantity',
            'porder' => 'Porder',
            'pstatus' => 'Pstatus',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
