// category section start
$(document).ready(function () {

    $('#status').change(function () {
        var status = $('#status').val();
        $.ajax({
            url: "search.php",
            method: "POST",
            data: {
                status: status
            },
            success: function (data) {
                $("tbody").html(data);
            }
        });
    });



});

    //check the category name is database or not 
    $("#catname").blur(function (e) {
        
        let catname = $(this).val();
        if (catname != "") {
            $("#CatNameCheck").show();
            $.ajax({
                type: "POST",
                url: "c_chkcatname.php",
                data: {
                    cat_name: catname
                },
                success: function (response) {
                    $("#CatNameCheck").html(response);
                }
            });
        }
    });
    $("#reset").click(function () {
        $("#CatNameCheck").hide();
        $("#catname").focus();
    });

var search = document.getElementById("searchcat");
search.addEventListener("keydown", function (e) {
    if (e.keyCode === 13) {
        SearchCat();
    }
});

function SearchCat() {
    var searchcat = $('#searchcat').val();
    $.ajax({
        url: "search.php",
        method: "POST",
        data: {
            searchcat: searchcat
        },
        success: function (data) {
            $("tbody").html(data);
        }
    });
}


function CatDelete(del) {

    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this record!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                var id = del;
                $.ajax({
                    type: "POST",
                    url: "cat_delete.php",
                    data: {
                        id: id
                    },
                    success: function (value) {
                        $("#data_table").html(value);
                    }
                });
                swal("Your record has been deleted!", {
                    icon: "success",
                });
            } else {
                swal("Your record is not deleted!", {
                    icon: "error",
                });
            }
        })
}

//end of the category section

function imgValidate() {

    var fileName = document.getElementById("imgpath").value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    let extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile == "jpg" || extFile == "png" || extFile == "jpeg") {

    }
    else {
        document.getElementById("imgpath").value = "";
        alert("Only jpg and png allowed...");
    }
}
