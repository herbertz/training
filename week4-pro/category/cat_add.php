<?php
session_start();
if ($_SESSION['email'] == null && $_SESSION['email'] == "") {
    header("location:../login/login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>add new category</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/custome.css" href="">
    <script src="../js/jquery-3.5.1.min.js"></script>
</head>

<body>
    <h1 class="text-center">Add new Category</h1>
    <form action="cat_add_new.php" method="POST" enctype="multipart/form-data">
        <div class="container">
            <div class="form-group">
                <label for="catname">Cat Name</label>
                <input type="text" class="form-control form-rounded" name="catname" id="catname" required autofocus>
                <h5 id="CatNameCheck"></h5>
            </div>
            <div class="form-group">
                <label for="order">Category order</label>
                <select class="form-control form-rounded" name="order" id="order" required>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
            <div class="form-group">
                <label for="catstatus">Category status:</label>
                <input type="radio" value="Active" name="catstatus" id="active" checked>
                <label for="catstatus">Active</label>
                <input type="radio" value="Inactive" name="catstatus" id="inactive">
                <label for="female">Inactive</label>
            </div>
            <div class="form-group">
                <label for="imgpath">Upload image</label>
                <input type="file" accept="image/*" onchange="imgValidate()" class="form-control form-rounded" name="imgpath" id="imgpath" required>
            </div>
            <input type="submit" id="submit" value="Add Category" class="btn btn-primary form-rounded">
            <input type="reset" value="Reset" id="reset"  class="btn btn-warning form-rounded">
        </div>
    </form>
    <script src="../js/validation.js"></script>
</body>

</html>
