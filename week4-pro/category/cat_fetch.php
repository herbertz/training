<?php
session_start();
if ($_SESSION['email'] == null && $_SESSION['email'] == "") {
    header("location:../login/login.php");
}
include_once("../conn/db.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Old Category</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/custome.css">
</head>

<body>
    <h1 class="text-center">Update Old Category</h1>
    <?php
    $id = $_GET['id'];
    $_SESSION['id'] = $id;
    $sql = "SELECT catid, catimg, catname, catorder, catstatus FROM category WHERE catid = '$id'";
    $res =  mysqli_query($conn, $sql) or die("fail to connect" . mysqli_error($conn));
    if (mysqli_num_rows($res)>0) {
    ?>
        <form action="cat_update.php" method="POST" enctype="multipart/form-data">
            <div class="container">
                <?php
                while ($row = mysqli_fetch_assoc($res)) {
                ?>
                    <div class="form-group">
                        <label for="catname">Cat Name</label>
                        <input type="text" class="form-control form-rounded" value="<?php echo $row['catname'] ?>" name="catname" id="catname" required>
                    </div>
                    <div class="form-group">
                        <label for="order">Category order</label>
                        <select class="form-control form-rounded" name="order" id="order" required>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Category Status:</label>
                        <label for="catstatus">Active</label>
                        <input type="radio" value="Active" <?php echo ($row['catstatus'] == "Active") ? 'checked' : '' ?> name="catstatus" id="active" checked>
                        <label for="Inactive">Inactive</label>
                        <input type="radio" value="Inactive" <?php echo ($row['catstatus'] == "Inactive") ? 'checked' : '' ?> name="catstatus" id="inactive">
                    </div>
                    <div class="form-group">
                        <label for="imgpath">Upload image</label>
                        <input type="file" accept="image/*" onchange="imgValidate()" class="form-control form-rounded" name="imgpath" id="imgpath">
                    </div>
                    <div class="form-group">
                        <label for="catimage">Category Image</label>
                        <img width="200px" height="200px" src="<?php echo "../img/category/" . $row["catimg"] ?>" alt="Category Img">
                    </div>
                    <input type="submit" value="Update Category" class="btn btn-primary form-rounded">
                    <a href="category.php" class="btn btn-warning form-rounded">Cancel</a>
            </div>
        <?php } ?>
        </form>
    <?php } else {
        echo "<script>";
        echo "alert('No any record for this id')";
        echo "</script>";
    } ?>
    <script src="../js/validation.js"></script>
</body>

</html>