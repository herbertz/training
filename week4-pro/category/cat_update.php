<?php
session_start();
include_once("../conn/db.php");
$catId = $_SESSION['id'];
$filename =  $_FILES['imgpath']['name'];
$tempname = $_FILES['imgpath']['tmp_name'];
$catname = trim($_POST['catname']);
$order =  $_POST['order'];
$catstatus =  $_POST['catstatus'];
$catUpdateDate = date('Y-m-d');

// catid, catimg, catname, cataddeaddate, catmodifydate, catorder, catstatus
if ($filename == "") {
    $catUpdate = "UPDATE category SET catname = '{$catname}', catmodifydate = '$catUpdateDate',
    catorder = {$order}, catstatus = '$catstatus' WHERE catid = {$catId}";
} else {
    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

    $filename = time().mt_rand().'.'.$ext;

    $folder = "../img/category/" . $filename;
    if (move_uploaded_file($tempname, $folder)) {
        
        $catUpdate = "UPDATE category SET catimg = '{$filename}', catname = '{$catname}', catmodifydate = '$catUpdateDate',
        catorder = {$order}, catstatus = '$catstatus' WHERE catid = {$catId}";
    } else {
        $msg = "Failed to upload image";
        echo "<script>";
        echo "alert('something is wrong {$msg}')";
        echo "</script>";
    }
}

$catUpdateRes = mysqli_query($conn, $catUpdate) or die("query is not exicute".mysqli_error($conn));

if ($catUpdateRes) {
    header("Location:../category/category.php");
} else {
    echo "<script>";
    echo "alert('category is not updated')";
    echo "</script>";
}
