<?php
include_once("../conn/db.php");
include_once("../header/header.php");
session_start();
if ($_SESSION['email'] == null && $_SESSION['email'] == "") {
    header("location:../login/login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>category section</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="../js/jquery-3.5.1.min.js"></script>
    <script src="../js/sweetalert.min.js"></script>
</head>
<body>
    <div class="container">
        <h2 class="text-center"> All Category Record </h2>
            <div class="form-row">
                <div class="col">
                    <a href="cat_add.php" style="margin-bottom: 10px" class="btn btn-outline-primary">Add New Category</a>
                </div>
                <div class="col-8">
                    <input type="text" name="searchcat"  class="form-control" id="searchcat" autofocus>
                </div>
                <div class="col">
                    <select class="btn btn-outline-danger float-right" name="status" id="status">
                        <option selected>all</option>
                        <option value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                    </select>
                </div>
            </div>
        
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Cat Img</th>
                    <th>Cat Name</th>
                    <th>cat order</th>
                    <th>No of product</th>
                    <th>cat status</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody id="data_table">
                <?php
                $catListing = "SELECT catid, catimg, catname, cataddeaddate, catmodifydate, catorder, catstatus 
                        FROM category
                        ORDER BY catorder ASC";
                
                $res = mysqli_query($conn, $catListing) or die("query is not exicute" + mysqli_error($conn));
                if (mysqli_num_rows($res) > 0) {
                ?>
                    <?php
                    while ($row = mysqli_fetch_assoc($res)) {
                        $id  = $row["catid"];
                        $noOfProduct = "SELECT COUNT(prodid) AS noOfProduct FROM product 
                        WHERE prodstatus = 'Active' AND catid = $id";
                        $resOfProduct = mysqli_query($conn,$noOfProduct) or die("not find the no of product");      
                        $rowOfProduct = mysqli_fetch_assoc($resOfProduct);
        
                    ?>  
                        <tr>
                            <td id="<?php $row['catid']; ?>" style="display:none;"><?php echo $row['catid']; ?></td>
                            <td> <img src="../img/category/<?php echo $row['catimg'] ?>" width="50px" height="50px" alt="notdisplay"> </td>
                            <td><?php echo $row['catname']; ?></td>
                            <td><?php echo $row['catorder'] ?></td>
                            <td><?php echo $rowOfProduct['noOfProduct'] ?></td>
                            <td><?php echo $row['catstatus']; ?></td>
                            <td><a class="btn btn-outline-danger" href="../category/cat_fetch.php?id=<?php echo $row['catid']; ?>">Edit</a></td>
                            <td><button class="btn btn-outline-danger" id="btndelete" onclick="CatDelete(<?php echo $row['catid']; ?>)" ?>Delete</button></td>
                        </tr>
                <?php }
                } else {
                    echo "<script>";
                    echo "alert('No any record please add record')";
                    echo "</script>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <script src="../js/validation.js"></script>
</body>
</html>