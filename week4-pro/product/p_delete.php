<?php
include_once("../conn/db.php");
$id = $_POST['id'];

$delimg = "SELECT p_img FROM p_image WHERE prodid = {$id}";
$res1 = mysqli_query($conn, $delimg) or die("Id not found");
$path = "../img/product/";

foreach ($res1 as $key => $value) {
    unlink($path . $value["p_img"]);
}

$sql = "DELETE FROM product WHERE prodid = '$id'";
$res = mysqli_query($conn, $sql) or die("something is wrong");

if ($res) {
    $query = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, 
        p.prodprice, p.prodqty, p.prodcreatedate,p.prodmodifydate,p.prodorder, c.catname 
        FROM product p 
        INNER JOIN p_image pi ON pi.prodid = p.prodid 
        LEFT JOIN category c ON c.catid = p.catid 
        WHERE pi.p_img_status = 1 AND c.catstatus = 'Active'
        ORDER BY prodorder ASC";
    $path = "../img/product/";
    
    $res = mysqli_query($conn, $query);
    
    if (mysqli_num_rows($res) > 0) {
    
        foreach ($res as $key => $value) {
            echo "<tr>";
            echo "<td><img src =" . $path . $value['p_img'] . " width=70px height=70px> </td>";
            echo "<td>" . $value['prodname'] . "</td>";
            echo "<td>" . $value['prodcode'] . "</td>";
            echo "<td>" . $value['catname'] . "</td>";
            echo "<td>" . $value['prodprice'] . "</td>";
            echo "<td>" . $value['prodqty'] . "</td>";
            echo "<td>" . $value['prodorder'] . "</td>";
            if ($value['prodstatus'] == "Active") {
                echo '<td><button class="btn btn-primary" onclick = ActiveInActive(' . $value["prodid"] . '); >Active</button></td>';
            } else {
                echo '<td><button class="btn btn-danger" onclick = ActiveInActive(' . $value["prodid"] . ');>InActive</button></td>';
            }
            echo "<td> <a href=../product/prod_fetchbyid.php?id=" . $value['prodid'] . " class='btn btn-outline-danger' > Edit </a> </td>";
            echo "<td><button type='button' class='btn btn-outline-danger' onclick='ProdDelete(" . $value['prodid'] . ")'>Delete</button></td>";
            echo "</tr>";
        }
    }
    
} else {
    echo "somethings is wrong";
}
