<?php
session_start();
if ($_SESSION['email'] == null && $_SESSION['email'] == "") {
    header("location:../login/login.php");
}
include_once("../conn/db.php");
include_once("../header/header.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Old Product</title>
    <link rel="stylesheet" href="../css/custome.css">
</head>

<body>
    <div class="container">
        <h1 class="text-center">Update Old Product</h1>
        <!-- proid,catid,prodname,prodcode,prodstatus,prodprice,prodqty,prodcreatedate,prodmodifydate,prodorder -->
        <?php
        $id = $_GET['id'];
        $_SESSION['id'] = $id;
        $sql = "SELECT prodid, prodname, prodcode, catid,prodstatus, prodprice, prodqty, prodcreatedate,
                prodmodifydate,prodorder 
                FROM product WHERE prodid = {$id}";
        
        $resProdByID =  mysqli_query($conn, $sql) or die("fail to connect " . mysqli_errno($conn));

        if (mysqli_num_rows($resProdByID)>0) {
        ?>
            <form action="prod_updata.php" method="POST" enctype="multipart/form-data">
                <?php
                while ($row = mysqli_fetch_assoc($resProdByID)) {
                ?>
                    <div class="form-group">
                        <label for="prodname">Product Name</label>
                        <input type="text" class="form-control form-rounded" name="prodname" id="prodname" value="<?php echo $row['prodname'] ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="prodcode">Product Code</label>
                        <input type="text" class="form-control form-rounded" value="<?php echo $row['prodcode'] ?>" name="prodcode" id="prodcode" disabled required>
                    </div>
                    <div class="form-group">
                        <label for="prodprice">Product Price</label>
                        <input type="text" class="form-control form-rounded" value="<?php echo $row['prodprice'] ?>" name="prodprice" id="prodprice" required>
                    </div>
                    <div class="form-group">
                        <label for="prodqty">Product Quantity</label>
                        <input type="text" class="form-control form-rounded" value="<?php echo $row['prodqty'] ?>" name="prodqty" id="prodqty" required>
                    </div>

                    <div class="form-group">
                        <label for="prodqty">select category </label>
                        <?php
                        $sql = "SELECT catid, catname FROM category WHERE catstatus = 'Active'";
                        $res = mysqli_query($conn, $sql);
                        echo '<select class="form-control form-rounded" name="catid">';
                        foreach ($res as $key => $value) {
                            echo '<option value = ' . $value["catid"] . '> ' . $value["catid"] . ' </option>';
                        }
                        echo '</select>';
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="prodorder">Product order</label>
                        <select class="form-control form-rounded" name="prodorder" id="prodorder" required>
                            <option value="1" name="prodorder">1</option>
                            <option value="2" name="prodorder">2</option>
                            <option value="3" name="prodorder">3</option>
                            <option value="4" name="prodorder">4</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="prodstatus">Product status:</label>
                        <input type="radio" value="Active" <?php echo ($row['prodstatus'] == "Active") ? 'checked' : '' ?> name="prodstatus" id="Active" checked>
                        <label for="prodstatus">Active</label>
                        <input type="radio" value="Inactive" <?php echo ($row['prodstatus'] == "Inactive") ? 'checked' : '' ?> name="prodstatus" id="Inactive">
                        <label for="Inactive">Inactive</label>
                    </div>
                <?php } ?>

                <?php
                $sqlProdImgByID= "SELECT pi.p_imageId, pi.prodid, pi.p_img, pi.p_img_status
                 FROM p_image pi WHERE pi.prodid = {$id}";
                $resProdImg = mysqli_query($conn, $sqlProdImgByID) or die("connection failed");
                if ($resProdImg) {
                    foreach ($resProdImg as $key => $value1) {
                ?>
                        <?php if ($value1['p_img_status'] == 1) {
                            echo '<img class="form-rounded" style="width:200px; height:200px;  border:5px solid green;  margin:10px" src="../img/product/' . $value1["p_img"] . '">';
                        } else {
                            echo "<div>";
                            echo '<img style="width:200px; height:200px;  margin-bottom:0px;" src="../img/product/' . $value1["p_img"] . '">';
                            echo "<a href='../product/pi_deleteupdate.php?imgUpdateId=" . $value1['p_imageId'] . "&imgstatus=2' class='btn btn-outline-danger form-rounded' > Active </a> ";
                            echo "<a href='../product/pi_deleteupdate.php?imgDeleteId=" . $value1['p_imageId'] . "&imgstatus=5' class='btn btn-outline-danger form-rounded' > Delete </a> ";
                            echo "</div>";
                        } ?>
                <?php }
                } ?>
                </br>
                <input type="submit" class="btn btn-primary form-rounded" value="Update Product">
                <a href="../product/product.php" class="btn btn-warning form-rounded">Cancel</a>
            </form>
        <?php } else {
            echo "<script>";
            echo "alert('No any record for this id')";
            echo "</script>";
        } ?>
    </div>
</body>

</html>