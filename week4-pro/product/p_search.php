<?php
include_once("../conn/db.php");

// proid,catid,prodname,prodcode,prodstatus,prodprice,prodqty,prodcreatedate,prodmodifydate,prodorder
if (isset($_POST['status']) && $_POST['status'] != "") {
    $status = $_POST['status'];
    if ($status == "Active") {
        $query = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, p.prodprice, 
        p.prodqty, p.prodcreatedate,p.prodmodifydate,p.prodorder, pi.p_img, c.catname 
        FROM product p 
        INNER JOIN p_image pi ON pi.prodid = p.prodid 
        LEFT JOIN category c ON c.catid = p.catid 
        WHERE pi.p_img_status = 1 AND c.catstatus = 'Active' AND p.prodstatus ='$status'
        ORDER BY prodorder ASC";
    } else if ($status == "Inactive") {
        $query = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, 
        p.prodprice, p.prodqty, p.prodcreatedate,p.prodmodifydate,p.prodorder, pi.p_img, c.catname 
        FROM product p 
        INNER JOIN p_image pi ON pi.prodid = p.prodid 
        LEFT JOIN category c ON c.catid = p.catid 
        WHERE pi.p_img_status = 1 AND c.catstatus = 'Active' AND p.prodstatus ='$status'
        ORDER BY prodorder ASC";
    } else {
        $query = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, 
        p.prodprice, p.prodqty, p.prodcreatedate,p.prodmodifydate,p.prodorder, pi.p_img, c.catname 
        FROM product p 
        INNER JOIN p_image pi ON pi.prodid = p.prodid 
        LEFT JOIN category c ON c.catid = p.catid 
        WHERE pi.p_img_status = 1 AND c.catstatus = 'Active'
        ORDER BY prodorder ASC";
    }
} else if (isset($_POST["minPrice"]) && isset($_POST["maxPrice"])) {

    $minPrice = $_POST["minPrice"];
    $maxPrice = $_POST["maxPrice"];

    $query = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, 
        p.prodprice, p.prodqty, p.prodcreatedate,p.prodmodifydate,p.prodorder, pi.p_img, c.catname 
        FROM product p 
        INNER JOIN p_image pi ON pi.prodid = p.prodid 
        LEFT JOIN category c ON c.catid = p.catid 
        WHERE pi.p_img_status = 1 AND c.catstatus = 'Active'
        AND p.prodprice BETWEEN $minPrice AND $maxPrice
        ORDER BY p.prodprice ASC";
} else if (isset($_POST["minQuantity"]) && isset($_POST["maxQuantity"])) {

    $minQuantity = $_POST["minQuantity"];
    $maxQuantity = $_POST["maxQuantity"];

    $query = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, 
        p.prodprice, p.prodqty, p.prodcreatedate,p.prodmodifydate,p.prodorder, pi.p_img, c.catname 
        FROM product p 
        INNER JOIN p_image pi ON pi.prodid = p.prodid 
        LEFT JOIN category c ON c.catid = p.catid 
        WHERE pi.p_img_status = 1 AND c.catstatus = 'Active'
        AND p.prodqty BETWEEN $minQuantity AND $maxQuantity
        ORDER BY p.prodqty ASC";
} 

else {
    
    if (isset($_POST['searchpro'])) {
        $search = $_POST['searchpro'];
        $query = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, 
        p.prodprice, p.prodqty, p.prodcreatedate,p.prodmodifydate,p.prodorder, pi.p_img, c.catname 
        FROM product p 
        INNER JOIN p_image pi ON pi.prodid = p.prodid 
        LEFT JOIN category c ON c.catid = p.catid 
        WHERE pi.p_img_status = 1 AND c.catstatus = 'Active' AND prodname LIKE '%$search%'
        ORDER BY prodorder ASC";
    } else {
        
        $query = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, 
        p.prodprice, p.prodqty, p.prodcreatedate,p.prodmodifydate,p.prodorder, c.catname 
        FROM product p 
        INNER JOIN p_image pi ON pi.prodid = p.prodid 
        LEFT JOIN category c ON c.catid = p.catid 
        WHERE pi.p_img_status = 1 AND c.catstatus = 'Active'
        ORDER BY prodorder ASC";
    }
}


$path = "../img/product/";
$res = mysqli_query($conn, $query);

if (mysqli_num_rows($res) > 0) {

    foreach ($res as $key => $value) {
        echo "<tr>";
        echo "<td><img src =" . $path . $value['p_img'] . " width=70px height=70px> </td>";
        echo "<td>" . $value['prodname'] . "</td>";
        echo "<td>" . $value['prodcode'] . "</td>";
        echo "<td>" . $value['catname'] . "</td>";
        echo "<td>" . $value['prodprice'] . "</td>";
        echo "<td>" . $value['prodqty'] . "</td>";
        echo "<td>" . $value['prodorder'] . "</td>";
        if ($value['prodstatus'] == "Active") {
            echo '<td><button class="btn btn-primary" onclick = ActiveInActive(' . $value["prodid"] . '); >Active</button></td>';
        } else {
            echo '<td><button class="btn btn-danger" onclick = ActiveInActive(' . $value["prodid"] . ');>InActive</button></td>';
        }
        echo "<td> <a href=../product/prod_fetchbyid.php?id=" . $value['prodid'] . " class='btn btn-outline-danger' > Edit </a> </td>";
        echo "<td><button type='button' class='btn btn-outline-danger' onclick='ProdDelete(" . $value['prodid'] . ")'>Delete</button></td>";
        echo "</tr>";
    }
}
