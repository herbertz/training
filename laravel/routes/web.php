<?php

use App\Models\Category;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('login', function () {
//     return view('login');
// });


Route::get('login','loginController@index')->name('login');
Route::get('register','RegisterController@index')->name('register');
Route::post('register','RegisterController@userRegister')->name('register');
Route::post('loginData','loginController@authenticate')->name('loginData');
Route::get('loginData','loginController@authenticate')->name('loginData');
Route::get('logout','loginController@logout')->name('logout');

Route::group(['middleware' => ['admin']], function () {
    // Category route start
    Route::get('categoryListing','CategoryController@index')->name('categoryListing');
    Route::get('addNewCategory','CategoryController@create')->name('addNewCategory');
    Route::post('addNewCategory','CategoryController@store')->name('addNewCategory');
    Route::get('/catEdit/{id}','CategoryController@edit')->name('catEdit');
    Route::post('/catEdit/{id}','CategoryController@update')->name('catEdit');
    Route::get('/catDelete/{id}','CategoryController@destroy')->name('catDelete');
    Route::get('/catname','CategoryController@catShow')->name('catname');
    
    // prodct route start
    Route::get('/addNewProduct','ProdController@create')->name('addNewProduct');
    Route::post('/addNewProduct','ProdController@store')->name('addNewProduct');
    Route::get('/prodListing','ProdController@index')->name('prodListing');
    Route::get('/prodDelete/{id}','ProdController@destroy')->name('prodDelete');
    Route::get('/prodEdit/{id}','ProdController@edit')->name('prodEdit');
    Route::post('updateProduct/{id}','ProdController@update')->name('updateProduct');
    Route::get('prodActiveInactive/{p_id}/{id}','ProdController@prodStatus')->name('prodActiveInactive');
    Route::get('prodImgDelete/{i_id}','ProdController@prodImgDelete')->name('prodImgDelete');
    Route::post('prodSearch','ProdController@show')->name('prodSearch');
    
});
