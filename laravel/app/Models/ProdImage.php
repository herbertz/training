<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdImage extends Model
{
    public $table = 'p_image';
    public $timestamps = false;
    public $fillable = ['id', 'p_id', 'p_img', 'status'];
}