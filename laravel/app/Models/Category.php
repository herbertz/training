<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = 'categorys';
    // public $timestamps = false;

    public $fillable = [
        'c_name',
        'status',
        'order',
        'c_file',
        
    ];
}
