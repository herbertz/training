<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // pname 	cid	p_code	p_price	p_saleprice	p_quantity	p_order	status
    public $table = 'products';
    public $fillable = ['pname','p_code','cid','p_code','p_price','p_saleprice','p_order','status'];
}
