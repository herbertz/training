<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //
    public function index()
    {
        return view("register");
    }

    public function userRegister(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|unique:users|email',
            'password' => 'required',
        ]);
        
        if ($request->isMethod('post')) {
            $data = $request->all();
            $register = new User;
            $register->name = $data['name'];
            $register->email = $data['email'];
            $register->password = bcrypt($data['password']);
            $register->save();
            return view('login');
        }
    }
}
