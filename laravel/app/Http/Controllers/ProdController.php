<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\ProdImage;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProdController extends Controller
{

    public function index()
    {
        $prodRecord = DB::table('products')
            ->join('categorys', 'categorys.id', '=', 'products.cid')
            ->join('p_image', 'p_image.p_id', '=', 'products.id')
            ->select('categorys.c_name', 'categorys.id', 'p_image.*', 'products.*')
            ->where('p_image.status', '=', 'Active')
            ->get();

        return view("product.p_listing", ['prodRecord' => $prodRecord]);
    }

    public function create()
    {
        //this fetch the name and category id from category table
        $cat_record = Category::select('id', 'c_name')->get();
        return view('product.p_add', ['cat_record' => $cat_record]);
    }

    public function store(Request $request)
    {
        // pname cid	p_code	p_price	p_saleprice	p_quantity	p_order	status	created_at	updated_at
        $proValidate = $request->validate([
            'pname' => 'required',
            'p_price' => 'required',
            'p_saleprice' => 'required',
            'p_quantity' => 'required',
            'p_order' => 'required',
            'status' => 'required',
        ]);
        $img = $request->file('p_img');
        $count = 0;
        foreach ($img as $key => $value) {
            $extension = $value->getClientOriginalExtension();
            $allowFileExtension = ['jpeg', 'png', 'jpg'];
            if (in_array($extension, $allowFileExtension)) {
            } else {
                $count++;
            }
        }
        if ($count == 0) {
            $insertProduct = new Product();
            $insertProduct->pname = $request->pname;
            $insertProduct->p_code = time() . mt_rand(10, 9999);
            $insertProduct->p_price = $request->p_price;
            $insertProduct->p_saleprice = $request->p_saleprice;
            $insertProduct->p_quantity = $request->p_quantity;
            $insertProduct->p_order = $request->p_order;
            $insertProduct->status = $request->status;
            $insertProduct->cid = $request->c_id;
            $insertProduct->save();
            $last_id = $insertProduct->id;

            //to insert the product image in p_img table and first image is active and another image is inactive 

            foreach ($img as $key => $value) {
                $prodImage = new ProdImage();
                $imgName = time() . mt_rand(1, 2000) . '-' . $value->getClientOriginalName();
                $value->move(public_path('asset/img/product'), $imgName);
                if ($key == 0) {
                    $imgStatus = 'Active';
                } else {
                    $imgStatus = 'Inactive';
                }
                $prodImage->p_id = $last_id;
                $prodImage->p_img = $imgName;
                $prodImage->status = $imgStatus;
                $prodImage->save();
            }
        } else {
            return redirect('addNewProduct')->with('danger', 'Please Select all iamge jpg|png|jpeg');
        }
        return redirect('prodListing')->with('success', 'Record Insert Successfully');
    }

    public function show(Request $request)
    {
        $query = DB::table('products')
            ->join('categorys', 'categorys.id', '=', 'products.cid')
            ->join('p_image', 'p_image.p_id', '=', 'products.id')
            ->select('categorys.c_name', 'categorys.id', 'p_image.*', 'products.*')
            ->where('p_image.status', '=', 'Active');
        if ($request->prodPriceMin && $request->prodPriceMax) {
            $prodMinPrice = $request->prodPriceMin;
            $prodPriceMax = $request->prodPriceMax;
            $prodRecord = $query->whereBetween('products.p_price', [$prodMinPrice, $prodPriceMax])
                ->get();
        } else if ($request->prodQtyMin && $request->prodQtyMax) {
            $prodQtyMin = $request->prodQtyMin;
            $prodQtyMax = $request->prodQtyMax;
            $prodRecord = $query
                ->whereBetween('products.p_quantity', [$prodQtyMin, $prodQtyMax])
                ->get();
        } else if ($request->searchProd) {
            $prodName = $request->searchProd;
            $prodRecord = $query
                ->where('products.pname', 'like', '%' . $prodName . '%')
                ->get();
        } else {
            return redirect('prodListing');
        }
        return view('product.p_listing')->with('prodRecord', $prodRecord);
    }

    public function edit($id)
    {
        //fetch the productimage record and product recored and display it the p_add page
        $prodImageRecord = ProdImage::where('p_id', $id)->get();

        $catAll = Category::all('id', 'c_name');
        $prodRecord = Product::find($id);
        return view('product.p_add', ['prodRecord' => $prodRecord, 'cat_record' => $catAll, 'prodImageRecord' => $prodImageRecord]);
    }

    public function update(Request $request, $id)
    {

        $proValidate = $request->validate([
            'pname' => 'required',
            'p_price' => 'required',
            'p_saleprice' => 'required',
            'p_quantity' => 'required',
            'p_order' => 'required',
            'status' => 'required',
        ]);

        $img = $request->file('p_img');
        $count = 0;
        if ($request->p_img) {
            foreach ($img as $key => $value) {
                $extension = $value->getClientOriginalExtension();
                $allowFileExtension = ['jpeg', 'png', 'jpg'];
                if (in_array($extension, $allowFileExtension)) {
                } else {
                    $count++;
                }
            }
        }
        //if all image extension is correct then insert the product
        if ($count == 0) {
            Product::where('id', $id)->update([
                'pname' => $request->pname,
                'p_price' => $request->p_price,
                'p_saleprice' => $request->p_price,
                'p_quantity' => $request->p_quantity,
                'p_order' => $request->p_order,
                'status' => $request->status,
            ]);


            //to insert the product image in p_img table and first image is active and another image is inactive 

            if ($request->p_img) {

                foreach ($img as $key => $value) {
                    $prodImage = new ProdImage();
                    $imgName = time() . mt_rand(1, 2000) . '-' . $value->getClientOriginalName();
                    $value->move(public_path('asset/img/product'), $imgName);
                    // $imgStatus = 'Inactive';
                    $prodImage->p_id = $id;
                    $prodImage->p_img = $imgName;
                    $prodImage->status = 'Inactive';
                    $prodImage->save();
                }
            }
        } else {
            return redirect()->back()->with('danger', 'Please Select all iamge jpg|png|jpeg');
        }
        return redirect('prodListing')->with('success', 'Record Updated Successfully');
    }
    public function prodStatus($p_id, $i_id)
    {
        ProdImage::where('p_id', $p_id)->update(['status' => 'Inactive']);
        ProdImage::where('id', $i_id)->update(['status' => 'Active']);
        return redirect()->back();
    }
    public function prodImgDelete($i_id)
    {
        $prodimageg = ProdImage::where('p_id', $i_id)->get();
        unlink(public_path('asset/img/product/' . $prodimageg->p_img));
        ProdImage::where('id', $i_id)->delete();
        return redirect()->back();
    }


    public function destroy($id)
    {
        $prodimageg = ProdImage::where('p_id', $id)->get();
        foreach ($prodimageg as $key => $value) {
            if (file_exists(public_path('asset/img/product/' . $value->p_img))) {
                unlink(public_path('asset/img/product/' . $value->p_img));
            }
        }
        ProdImage::where('p_id', $id)->delete();
        Product::where('id', $id)->delete();
        return redirect('prodListing')->with('danger', 'Product Delete Successfully');
    }
}
