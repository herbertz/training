-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2021 at 11:38 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `c_name`, `c_file`, `order`, `status`, `created_at`, `updated_at`) VALUES
(5, 'vishal', '1613024490.png', 20, 'Inactive', '2021-02-11 00:51:30', '2021-02-11 00:51:30'),
(6, 'lalit', '1613024829.png', 30, 'Active', '2021-02-11 00:57:09', '2021-02-11 00:57:09');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_02_10_063405_create_categorys_table', 2),
(4, '2021_02_12_065019_create_products_table', 3),
(5, '2021_02_12_072935_create_p_image_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `p_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_saleprice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `pname`, `cid`, `p_code`, `p_price`, `p_saleprice`, `p_quantity`, `p_order`, `status`, `created_at`, `updated_at`) VALUES
(2, 'jay', 6, '1613210910815', '2', '2', '2', '2', 'Active', '2021-02-13 04:38:30', '2021-02-13 04:38:30'),
(3, 'jay', 6, '16132109645151', '2', '2', '2', '2', 'Active', '2021-02-13 04:39:24', '2021-02-13 04:39:24'),
(4, 'ja', 4, '16132109882250', '1', '0', '2', '2', 'Inactive', '2021-02-13 04:39:48', '2021-02-13 04:39:48'),
(5, 'ja', 4, '16132109948880', '1', '0', '2', '2', 'Inactive', '2021-02-13 04:39:54', '2021-02-13 04:39:54'),
(7, '20', 8, '16132174239369', '20', '20', '20', '20', 'Active', '2021-02-13 06:27:03', '2021-02-13 06:27:03'),
(9, '20', 8, '1613217633293', '20', '20', '20', '20', 'Active', '2021-02-13 06:30:33', '2021-02-13 06:30:33'),
(10, '20', 8, '16132176513830', '20', '20', '20', '20', 'Active', '2021-02-13 06:30:51', '2021-02-13 06:30:51'),
(14, 'bapuji', 8, '16133953964942', '3', '3', '2', '2', 'Inactive', '2021-02-15 07:53:16', '2021-02-16 05:54:03'),
(16, 'aditya', 8, '16134682495445', '100', '100', '100', '100', 'Active', '2021-02-16 04:07:29', '2021-02-16 04:07:29'),
(17, 'hardik', 5, '16134682963190', '200', '200', '200', '200', 'Inactive', '2021-02-16 04:08:16', '2021-02-16 04:08:16'),
(18, 'aakash', 6, '16134683388982', '300', '300', '300', '300', 'Inactive', '2021-02-16 04:08:58', '2021-02-16 04:08:58'),
(19, 'jhanvi', 4, '16134683848158', '500', '500', '500', '500', 'Active', '2021-02-16 04:09:44', '2021-02-16 04:09:44'),
(20, 'disha', 8, '16134684161712', '600', '600', '600', '600', 'Active', '2021-02-16 04:10:16', '2021-02-16 04:10:16'),
(21, 'bhuyangdev', 5, '16134684565833', '0700', '700', '700', '700', 'Inactive', '2021-02-16 04:10:56', '2021-02-16 04:10:56'),
(22, 'tarang', 6, '16134684909961', '900', '900', '900', '900', 'Inactive', '2021-02-16 04:11:30', '2021-02-16 04:11:30');

-- --------------------------------------------------------

--
-- Table structure for table `p_image`
--

CREATE TABLE `p_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` int(11) NOT NULL,
  `p_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `p_image`
--

INSERT INTO `p_image` (`id`, `p_id`, `p_img`, `status`) VALUES
(9, 1, '1613210622313-07.png', 'Active'),
(10, 2, '16132109111180-07.png', 'Inactive'),
(11, 3, '1613210965660-07.png', 'Inactive'),
(12, 4, '16132109881553-07.png', 'Inactive'),
(13, 5, '16132109951539-07.png', 'Inactive'),
(14, 6, '1613211121678-01.png', 'Active'),
(15, 6, '16132111211331-02.png', 'Inactive'),
(16, 6, '1613211121120-03.png', 'Inactive'),
(17, 6, '16132111211186-04.png', 'Inactive'),
(18, 6, '16132111221255-05.png', 'Inactive'),
(19, 6, '16132111221016-06.png', 'Inactive'),
(20, 6, '1613211122553-07.png', 'Inactive'),
(28, 14, '16133953961747-01.png', 'Inactive'),
(29, 14, '16133953961474-02.png', 'Active'),
(30, 14, '1613395396296-03.png', 'Inactive'),
(31, 14, '1613395396718-04.png', 'Inactive'),
(32, 14, '1613395397597-05.png', 'Inactive'),
(33, 14, '1613395397699-06.png', 'Inactive'),
(37, 14, '161345503878-free-fb-f1199-fashion-basket-original-imaf4ec7gamryhn6.jpeg', 'Inactive'),
(38, 16, '16134682491525-01.jpg', 'Active'),
(39, 17, '16134682961434-02.jpg', 'Active'),
(40, 17, '1613468296944-03.jpg', 'Inactive'),
(41, 18, '1613468338307-01.jpg', 'Active'),
(42, 18, '1613468338289-pexels-photo-842811-1024x620.jpeg', 'Inactive'),
(43, 19, '16134683841618-free-fb1-f1160-fashion-basket-original-imaff4gq2vdbtnbt.jpeg', 'Inactive'),
(44, 19, '1613468384117-free-fb1-f1160-fashion-basket-original-imaff4gqf6m8dh8f.jpeg', 'Inactive'),
(45, 19, '161346838461-free-fb1-f1160-fashion-basket-original-imaff4gqtchztsrc.jpeg', 'Active'),
(46, 20, '1613468416980-black1.jpg', 'Active'),
(47, 20, '1613468416791-black2.jpg', 'Inactive'),
(48, 20, '16134684161072-black3.jpg', 'Inactive'),
(49, 21, '161346845619-0-free-1199-fashionuma-0-original-imaf4ec7djyb5ym8.jpeg', 'Active'),
(50, 21, '16134684561817-free-fb-f1199-fashion-basket-original-imaf4ec6rcg4fkfz.jpeg', 'Inactive'),
(51, 21, '16134684561484-free-fb-f1199-fashion-basket-original-imaf4ec7gamryhn6.jpeg', 'Inactive'),
(52, 21, '16134684561987-free-fb-f1199-fashion-basket-original-imaf4ec7yag4tymj.jpeg', 'Inactive'),
(53, 22, '16134684901664-3xl-usts5788-u-s-polo-assn-original-imaf2pb8u7zd2kyz.jpeg', 'Active'),
(54, 22, '1613468490785-s-usts5788-u-s-polo-assn-original-imaf2pb8pwgzrnza.jpeg', 'Inactive'),
(55, 22, '16134684901645-s-usts5788-u-s-polo-assn-original-imaf2pb8uxfexctu.jpeg', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `fullname`, `password`) VALUES
(1, 'hardik', '', 'kanzariya'),
(2, 'j@gmail.com', '', 'jay'),
(3, 'hardik@gmail.com', 'hardik', '$2y$10$pM/QvtP5rRrTYfR.2PYBQOKs0R6mC/B0O6GkqfQvNTeSJBcnk6t.i'),
(4, 'kanzariya@gmail.com', 'hardik', '$2y$10$ohu5gzjyvfMozMmTxy8ma.tZkS27STeUagfxqUMjMTOrRBoFpA7qi'),
(5, 'aakash@gmail.com', 'aakash', '$2y$10$b3qZbNT5gFyxhfoInWoPB.hyNC8sGLTqFPmUEyjJs8m.USQV49BNe'),
(6, 'a@gmail.com', 'aakash', '$2y$10$8d.dwh889sNPtEOKPUoPzuubqrXiUGsoVIt2MUcQ5kr0wjQGSrjbO'),
(7, 'ab@gmail.com', 'hardikk', '$2y$10$CzX8xrVZ8VsqiYQdJb5WluqIaQTjryaXo8yPGq/VXkQ8ITvhd7ho.'),
(8, 's@gmail.com', 'shahil', '$2y$10$lxvErR3.tKApWrA68rKM1O9KmpGEKs2Z22agOhVSZSH.vO5Hxjg/6'),
(9, 'n@gmail.com', 'nancy', '$2y$10$sopNjIfbNZXvpUVi7ExMCOFgTAJoo84Ep8TwZauvgH.gpF.36Ao6.'),
(10, 'b@gmail.com', 'b', '$2y$10$S5DeBnYLHZk8b8IO8dPfF.xC0OCB9D9APDd288foHmiT06iENKuIm');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'c@gmail.com', 'c@gmail.com', NULL, '$2y$10$Dh/VdZZ367Kj0u7yjgPTuuqe8oq3nbsd7QvqFAwWyO8joLoDPnktG', NULL, NULL, NULL),
(2, 'raju', 'r@gmail.com', NULL, '$2y$10$qjjIIne4zceDuXJqLAwNveEIpjgZAB2ZKuYYQQHWNvae6lqj4bUCO', NULL, NULL, NULL),
(3, 'raju', 'r1@gmail.com', NULL, '$2y$10$xnJy/81xbe9D/HHyGYvagO2XgfOHP80NDk/6/1183JG2K0y7WGKku', NULL, NULL, NULL),
(4, 'hardik', 'hardik@gmail.com', NULL, '$2y$10$if3EuQYTb/P6s6MotiQKi.ZVVkXmWES9xznXR0bQanZYygqP0USY6', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorys_c_name_unique` (`c_name`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_image`
--
ALTER TABLE `p_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `p_image`
--
ALTER TABLE `p_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
