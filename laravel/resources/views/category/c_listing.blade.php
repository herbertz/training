@extends('dashboard')
@section('content')
@if(Session::get('cat_success'))
<div class="alert alert-success" role="alert" id="success-alert">
    <strong>{{Session::get('cat_success')}} </strong>
</div>
@endif
@if(Session::get('cat_danger'))
<div class="alert alert-danger" role="alert">
    <strong>{{Session::get('cat_danger')}} </strong>
</div>
@endif
<div class="form-row">
    <div class="form-group col-md-6">
        <a type="button" class="btn btn-primary " href="{{route('addNewCategory')}}">Add New Category</a>
    </div>
    <div class="form-group col-md-5">
        <input type="text" class="form-control col-md-6 float-right" placeholder="search category" name="cat_search" id="cat_search">
    </div>
</div>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>category Name</th>
            <th>image</th>
            <th>order</th>
            <th>status</th>
            <th>Create Date</th>
            <th>Update Date</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($category as $key=>$value)
        <tr>
            <td>{{$value->id}}</td>
            <td>{{$value->c_name}}</td>
            <td><img src="{{url('/asset/img/category/'.$value->c_file)}}" width="70px" height="70px"></td>
            <td>{{$value->order}}</td>
            <td>{{$value->status}}</td>
            <td>{{$value->created_at->format('d/m/Y')}}</td>
            <td>{{$value->updated_at->format('d/m/Y')}}</td>
            <td><a href="catEdit/{{$value->id}}" class="btn btn-success">Edit</a></td>
            <td><button onclick="catDelete({{$value->id}})" class="btn btn-danger">Delete</button></td>
        </tr>
        @endforeach
    </tbody>
</table>
<script src="{{asset('asset/js/jquery.min.js')}}"></script>
<script src="{{asset('asset/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {  
        
        //when press enter then run delete code
        document.addEventListener("keydown", function(e) {
            if (e.keyCode === 13) {
                var catname = $("#cat_search").val()
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('catname')}}',
                    data: {
                        catname: catname
                    },
                    success: function(response) {
                        console.log(response);
                        $('tbody').html(response);
                    },
                });
            }
        });

        $(".alert").fadeTo(2000, 2000).slideUp(2000, function() {
            $(".alert").slideUp(5000);
        });
    });

    function catDelete(id)
    {
        let a = confirm("Are You Sure wan't to delete This Record");
        if(a==true)
        {
            $.ajax({
                type: "get",
                url: "catDelete/"+id,
                success: function (response) {
                    location.reload();
                }
            });
        }
        else 
        {
            console.log("record is safe");
        }
    }

</script>
@endsection