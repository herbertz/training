<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // pname cname productcode price saleprice quantity porder pstatus created_at updated_at
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('pname');
            $table->integer('cid');
            $table->string('p_code');
            $table->string('p_price');
            $table->string('p_saleprice');
            $table->string('p_quantity');
            $table->string('p_order');
            $table->enum('status',['Active','Inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
