-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2021 at 04:22 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `choward`
--
CREATE DATABASE IF NOT EXISTS `choward` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `choward`;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `vBrandName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enStatus` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `enIsDeleted` enum('No','Yes') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cat`
--

CREATE TABLE `cat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `c_file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cat`
--

INSERT INTO `cat` (`id`, `c_name`, `status`, `order`, `c_file`, `created_at`, `updated_at`) VALUES
(3, 'ramnikbhai', '1', 20, '0', '2021-02-25 05:52:38', '2021-03-01 03:42:55'),
(4, 'five', '2', 50, '0', '2021-02-25 06:14:12', '2021-02-25 06:14:12'),
(5, 'hardik kanzariya', '1', 0, '0', '2021-02-25 06:35:56', '2021-02-25 06:35:56'),
(8, 'jayrajsinh 123', '1', 50, '0', '2021-02-25 23:08:08', '2021-02-25 23:08:08'),
(14, 'First Category', '1', 0, '0', '2021-03-04 05:33:54', '2021-03-04 05:33:54'),
(16, 'kishorbhai', '1', 20, '0', '2021-03-04 22:16:36', '2021-03-04 22:16:36'),
(17, 'raju', '1', 50, '0', '2021-03-04 22:16:51', '2021-03-04 22:16:51'),
(18, 'mehulbhai', '1', 40, '0', '2021-03-04 22:16:59', '2021-03-04 22:16:59'),
(19, 'lalakaka', '1', 70, '0', '2021-03-04 22:17:10', '2021-03-04 22:17:10'),
(20, 'shreehari', '1', 70, '0', '2021-03-04 22:17:32', '2021-03-04 22:17:32'),
(21, 'ghanshyam', '1', 50, '0', '2021-03-04 22:17:39', '2021-03-04 22:17:39'),
(22, 'nilkanth', '1', 10, '0', '2021-03-04 22:17:48', '2021-03-04 22:17:48'),
(23, 'vijaybhai', '1', 20, '0', '2021-03-04 22:18:30', '2021-03-04 22:18:30'),
(24, 'nyalkaran', '1', 50, '0', '2021-03-04 22:18:41', '2021-03-04 22:18:41'),
(25, 'gadhpur', '1', 50, '0', '2021-03-04 22:19:05', '2021-03-04 22:19:05'),
(26, 'abc', '1', 5, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'bcd', '1', 10, '0', NULL, NULL),
(28, 'abc', '1', 5, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'bcd', '1', 10, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'bed', '1', 20, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'bef', '1', 30, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'beg', '1', 40, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'bceh', '1', 10, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'bcdd', '1', 10, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'bcdds', '1', 10, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'bcdhf', '1', 10, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'bcdhf', '1', 10, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'bcdsg', '1', 10, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'bcdfgs', '1', 10, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'bcdfgs', '1', 10, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'vishal amd', '1', 56, '0', '2021-03-08 08:16:07', '2021-03-08 08:16:07'),
(42, 'aditya p', '1', 0, '0', '2021-03-09 05:53:48', '2021-03-09 05:53:48');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `iCategoryId` int(10) UNSIGNED NOT NULL,
  `vCategoryName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vCategoryName_ar` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vCategorySlug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vCategoryImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enStatus` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `enIsDeleted` enum('No','Yes') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `catrecord`
--

CREATE TABLE `catrecord` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `iCMSPageId` int(10) UNSIGNED NOT NULL,
  `enUserType` enum('Customer','Partner','Driver') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Customer',
  `vPageNameEN` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vPageNameAR` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txContentEN` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `txContentAR` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_reason`
--

CREATE TABLE `contact_us_reason` (
  `iContactReasonId` int(10) UNSIGNED NOT NULL,
  `tiUserType` tinyint(4) NOT NULL COMMENT '1-Customer, 2-Partner, 3-Driver',
  `vReasonEN` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vReasonAR` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiIsDeleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-No, 1-Yes',
  `tiIsActive` tinyint(4) NOT NULL COMMENT '0-Inactive, 1-Active',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_request`
--

CREATE TABLE `contact_us_request` (
  `iRequestId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `iContactReasonId` int(10) UNSIGNED NOT NULL,
  `txDescription` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_addresses`
--

CREATE TABLE `customer_addresses` (
  `iCustomerAddressId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vFullName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vISDCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vMobileNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vAddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiIsSelected` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - No, 1 - YES',
  `tiAddressType` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - Home, 1 - Office, 2-Others',
  `fLongitude` decimal(10,8) DEFAULT 0.00000000,
  `fLatitude` decimal(10,8) DEFAULT 0.00000000,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `decline_reason`
--

CREATE TABLE `decline_reason` (
  `iDeclineReasonId` int(10) UNSIGNED NOT NULL,
  `vReasonEN` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vReasonAR` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiReasonType` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-Decline, 1-Cancel',
  `tiIsDeleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-No, 1-Yes',
  `tiIsActive` tinyint(4) NOT NULL COMMENT '0-Inactive, 1-Active',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `device_details`
--

CREATE TABLE `device_details` (
  `iDeviceId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vAccessToken` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vDeviceToken` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vDeviceId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiDeviceType` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '0 => Web, 1 => Android,2 => IOS',
  `vDeviceName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED NOT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `device_details`
--

INSERT INTO `device_details` (`iDeviceId`, `iUserId`, `vAccessToken`, `vDeviceToken`, `vDeviceId`, `tiDeviceType`, `vDeviceName`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 13, '71ec86009a137336f63c3558657c236b', 'ddd', '', 0, 'iPhone X', 1614321348, 1614321348),
(2, 14, '52a931e3dd1d4cc19a3ef1dc470ed645', 'dfdf', '', 0, 'iPhone X', 1614169361, 1614169361),
(3, 15, '0bda685881d365e1333bf77d5c60b07e', 'dfdf', '', 0, 'iPhone X', 1614172060, 1614172060),
(4, 16, 'c8b005b4353afaf59c035eb072dcbc5a', 'dfdf', '', 0, 'iPhone X', 1614172355, 1614172355),
(5, 17, 'd3ae2721c5f6b224465d08f37ebda807', 'dfdf', '', 0, 'iPhone X', 1614175920, 1614175920),
(6, 18, 'd12fb647ad27cbd1a754fd38cb33ad53', 'dfdf', '', 0, 'iPhone X', 1614249035, 1614249035),
(7, 19, '396463ed2792ea825fbf147bee055bb0', 'dfdf', '', 0, 'iPhone X', 1614251088, 1614251088),
(8, 20, 'eb944fd6b709ad63ab1ea2af4af36362', 'dfdf', '', 0, 'iPhone X', 1614251150, 1614251150),
(9, 21, 'ab72f4656821dc3303795664a74c5b81', 'dfdf', '', 0, 'iPhone X', 1614321543, 1614321543),
(10, 22, '8384f56a3c99a0b9228a3f3573cc29aa', 'dfdf', '', 0, 'iPhone X', 1614322093, 1614322093),
(11, 23, 'a7200e6f3d572e8e616bcaf8b3db55e9', 'dfdf', '', 0, 'iPhone X', 1614603446, 1614603446);

-- --------------------------------------------------------

--
-- Table structure for table `driver_declined_order`
--

CREATE TABLE `driver_declined_order` (
  `iDeclinedOrderId` int(10) UNSIGNED NOT NULL,
  `iDeclineReasonId` int(10) UNSIGNED NOT NULL,
  `iDriverId` int(10) UNSIGNED NOT NULL,
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `vDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `driver_details`
--

CREATE TABLE `driver_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vLicenceFront` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vLicenceBack` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enVehicleType` enum('Bike','Car') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vVehicleBrandName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vVehicleModelName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vVehicleColor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vVehiclePlateNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vBankName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAccountHolderName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAccountNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vIBAN` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vRoutingNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vLocation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fLatitude` decimal(10,8) DEFAULT NULL,
  `fLongitude` decimal(11,8) DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `iQuestionId` int(10) UNSIGNED NOT NULL,
  `vQuestionEN` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vQuestionAR` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `txAnswerEN` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `txAnswerAR` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiIsDeleted` tinyint(4) NOT NULL COMMENT '1-Yes, 0-No',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2020_02_14_035943_create_admins_table', 1),
(3, '2020_02_20_111632_site-settings', 1),
(4, '2020_10_19_090833_create_device_table', 1),
(5, '2020_10_21_134053_create_categories_table', 1),
(6, '2020_11_24_133657_alter_users', 1),
(7, '2020_11_24_135504_alter_categories_table', 1),
(8, '2020_11_25_052603_alter_user_table', 1),
(9, '2020_11_25_102559_execute_sql_user_table', 1),
(10, '2020_11_26_045123_execute_sql_user_3_table', 1),
(11, '2020_11_27_091816_execute_sql_quries', 1),
(12, '2020_11_27_092544_create_product_table', 1),
(13, '2020_11_27_095830_alter_product_table', 1),
(14, '2020_11_27_103432_direct_sql_queries_1', 1),
(15, '2020_11_27_104359_create_product_variants_table', 1),
(16, '2020_11_27_115555_sql_queries_2', 1),
(17, '2020_11_30_102012_create_brands_table', 1),
(18, '2020_11_30_120914_execute_sql_30-11-2020', 1),
(19, '2020_11_30_141735_create_product_images', 1),
(20, '2020_11_30_143707_execute_sql_product_images', 1),
(21, '2020_12_01_081042_create_product_dislike_table', 1),
(22, '2020_12_01_134143_create_store_table', 1),
(23, '2020_12_01_134554_create_promo_code_table', 1),
(24, '2020_12_01_141206_alter_user_table_01-12-2020', 1),
(25, '2020_12_01_141504_alter_store_table_01-12-2020', 1),
(26, '2020_12_01_141852_create_store_images_table', 1),
(27, '2020_12_02_052921_alter_promo_code1', 1),
(28, '2020_12_02_053415_alter_promo_code', 1),
(29, '2020_12_02_063106_alter_product_table_02-12-2020', 1),
(30, '2020_12_02_075455_remove_data_from_table_02-12-2020', 1),
(31, '2020_12_02_094050_alter_product_description', 2),
(32, '2020_12_02_125756_alter_store_data_table', 2),
(33, '2020_12_03_053239_alter_store_data_table_2', 2),
(34, '2020_12_03_053523_alter_store_data_table_3', 2),
(35, '2020_12_03_081006_create_customer_addresses_table', 2),
(36, '2020_12_04_033634_create_order_master_table', 2),
(37, '2020_12_04_033726_create_ordered_products_table', 2),
(38, '2020_12_04_042409_alter_order_master_table', 2),
(39, '2020_12_04_061516_alter_ordered_products_table1', 2),
(40, '2020_12_04_061752_alter_order_master_table1', 2),
(41, '2020_12_04_065113_alter_ordered_products_table2', 2),
(42, '2020_12_04_090019_alter_store_data_table_4', 2),
(43, '2020_12_04_105624_alter_order_master1', 2),
(44, '2020_12_08_051656_alter_order_master_table2', 2),
(45, '2020_12_09_081408_alter_customer_addresses_table1', 2),
(46, '2020_12_09_113102_alter_order_master_3', 2),
(47, '2020_12_10_060331_alter_order_master_4', 2),
(48, '2020_12_10_115809_alter_order_master5', 2),
(49, '2020_12_11_091102_alter_order_master6', 2),
(50, '2020_12_11_094250_alter_order_master7', 2),
(51, '2020_12_12_052659_create_recent_search_table', 2),
(52, '2020_12_12_072038_alter_order_master8', 2),
(53, '2020_12_12_091546_alter_order_master9', 2),
(54, '2020_12_14_133913_alter_user_table_14122020', 2),
(55, '2020_12_15_053446_alter_device_details_table_15122020', 2),
(56, '2020_12_16_053501_alter_users_table_161220201', 2),
(57, '2020_12_16_061559_alter_users_table_161220202', 2),
(58, '2020_12_16_124619_create_order_allocation', 2),
(59, '2020_12_16_131102_alter_order_allocation', 2),
(60, '2020_12_16_132205_create_driver_details', 2),
(61, '2020_12_16_133754_alter_multiple_tables', 2),
(62, '2020_12_17_050936_alter_driver_details_17122020', 2),
(63, '2020_12_17_065823_alter_order_products_17122020', 2),
(64, '2020_12_21_104859_alter_product_21122020', 2),
(65, '2020_12_22_052545_alter_users_table_22122020', 2),
(66, '2020_12_22_052614_alter_product_table_22122020', 2),
(67, '2020_12_28_045225_create_cms_pages', 2),
(68, '2020_12_28_050959_create_contact_us_reason_table', 2),
(69, '2020_12_28_072001_create_contact_us_request_table', 2),
(70, '2020_12_31_071732_create_faq_table', 2),
(71, '2021_01_01_084438_create_store_orders', 2),
(72, '2021_01_04_053545_alter_order_allocation_04012021', 2),
(73, '2021_01_04_053943_alter_order_allocation_040120212', 2),
(74, '2021_01_04_060608_alter_site_settings', 2),
(75, '2021_01_04_084715_create_user_notification', 2),
(76, '2021_01_04_100349_alter_device_details', 2),
(77, '2021_01_05_060704_create_decline_reason_table', 2),
(78, '2021_01_05_064409_create_driver_declined_order_table', 2),
(79, '2021_01_05_070911_create_sub_order_master', 2),
(80, '2021_01_05_084512_alter_sub_order_master', 2),
(81, '2021_01_05_090411_alter_sub_order_allocation', 2),
(82, '2021_01_06_090435_alter_order_master', 2),
(83, '2021_01_06_093043_alter_order_master', 2),
(84, '2021_01_06_131512_alter_decline_reason_06012021_table', 2),
(85, '2021_02_24_072112_create_catrecord_table', 2),
(86, '2021_02_24_072928_create_cat_table', 3),
(87, '2021_02_24_073343_create_cat_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `ordered_products`
--

CREATE TABLE `ordered_products` (
  `iOrderedProductId` int(10) UNSIGNED NOT NULL,
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iProductId` int(10) UNSIGNED NOT NULL,
  `iProductVariantId` int(10) UNSIGNED NOT NULL,
  `fProductVariantsPrice` double(8,2) NOT NULL,
  `fTotalAmount` double(8,2) NOT NULL,
  `iQty` int(11) NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_allocation`
--

CREATE TABLE `order_allocation` (
  `iOrderAllocationId` int(10) UNSIGNED NOT NULL,
  `iDriverId` int(10) UNSIGNED NOT NULL,
  `tiIsNewOrderPopup` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - pop up not open yet for driver app , 1 - opened ',
  `iCanceledByUserId` int(10) UNSIGNED DEFAULT NULL,
  `tiCancelledBy` tinyint(1) DEFAULT NULL COMMENT '0 - Admin, 1- Driver, 2 - Customer, 3 - Store',
  `tiAllocationStatus` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - Pending, 1 - Accepted, 2 - Rejected, 3 - Cancelled, 4 - On Route, 5 - Arrived, 6 - Confirm , 7 - Delivered',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_master`
--

CREATE TABLE `order_master` (
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `vOrderId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `iPromoCodeId` int(10) UNSIGNED DEFAULT NULL,
  `tiDeliveryType` tinyint(4) DEFAULT NULL COMMENT '1 = ASAP\n            -  2 = Scheduled',
  `dDeliverDate` date DEFAULT NULL,
  `tDeliveryTime` time DEFAULT NULL,
  `tiAddressType` tinyint(4) NOT NULL COMMENT '0 - Home, 1 - Office, 2-Others',
  `vDeliveryAddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fDeliveryLatitude` double(10,8) NOT NULL,
  `fDeliveryLongitude` double(10,8) NOT NULL,
  `dPlaceOrderDate` datetime DEFAULT NULL,
  `vOrderInstruction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fItemTotalAmount` double(10,2) NOT NULL DEFAULT 0.00,
  `fDiscountAmount` double(10,2) NOT NULL DEFAULT 0.00,
  `fTax_charges` double(10,2) NOT NULL DEFAULT 0.00,
  `fDeliverCharges` double(10,2) NOT NULL DEFAULT 0.00,
  `fTotalAmount` double(10,2) NOT NULL DEFAULT 0.00,
  `fTotalPayableAmount` double(10,2) NOT NULL DEFAULT 0.00,
  `tiOrderStatus` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - Cart, 1 - Placed, 2 - Driver Assigned',
  `vPaymentId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiPaymentType` tinyint(4) DEFAULT NULL COMMENT '1-Apple Pay, 2-Google Pay, 3-Choward Wallet, 4-Card',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `iCategoryId` int(10) UNSIGNED NOT NULL,
  `vProductName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vBrandName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `txProductDescription` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vOffers` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vProductImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiIsDeleted` tinyint(4) NOT NULL COMMENT '1 = Yes, 0 = No',
  `tiAdminApproved` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1- Yes, 0-No',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `iProductImageId` int(10) UNSIGNED NOT NULL,
  `iProductId` int(10) UNSIGNED NOT NULL,
  `vImageName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_liked`
--

CREATE TABLE `product_liked` (
  `id` int(10) UNSIGNED NOT NULL,
  `iProductId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `tiLikedProduct` tinyint(4) NOT NULL COMMENT '0 - No, 1 - Yes',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` int(10) UNSIGNED NOT NULL,
  `iProductId` int(10) UNSIGNED NOT NULL,
  `fProductVariantsPrice` double(10,2) NOT NULL DEFAULT 0.00,
  `vProductVariantsQuantity` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promo_code`
--

CREATE TABLE `promo_code` (
  `iPromoCodeId` int(10) UNSIGNED NOT NULL,
  `vPromoCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fAmountOrPercent` double(8,2) NOT NULL DEFAULT 0.00,
  `vPromoDesc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dStartDate` date NOT NULL,
  `dEndDate` date NOT NULL,
  `fMinOrderAmount` int(11) NOT NULL,
  `iMaxUsageLimit` int(11) NOT NULL COMMENT 'if null than no limit',
  `tiPromoType` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1 - Fixed Amount , 2 - Percentage On Total Amount',
  `tiIsDeleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Yes, 0 - No,',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL,
  `enStatus` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recent_search`
--

CREATE TABLE `recent_search` (
  `id` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vSearch` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `iSettingsId` bigint(20) UNSIGNED NOT NULL,
  `vSettingTitle` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `vSettingKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vSettingValue` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `tiIsActive` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `tiIsDeleted` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `iCreatedAt` int(10) UNSIGNED NOT NULL,
  `iUpdatedAt` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`iSettingsId`, `vSettingTitle`, `vSettingKey`, `vSettingValue`, `tiIsActive`, `tiIsDeleted`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 'Driver Order Recieve Distance', 'DRIVER_ORDER_RECIEVE_DISTANCE', '10', 1, 0, 1604926192, 1604926192);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vStoreName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vStoreLocation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vStoreAddressLine2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txStoreDescription` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iIsBranch` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - No, 1 - Yes',
  `vLicenceFront` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vLicenceBack` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vBankName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAccountHolderName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAccountNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vIBAN` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vRoutingNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fStoreLatitude` decimal(10,8) DEFAULT 0.00000000,
  `fStoreLongitude` decimal(10,8) DEFAULT 0.00000000,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `store_images`
--

CREATE TABLE `store_images` (
  `iStoreImageId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `vImageName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `store_orders`
--

CREATE TABLE `store_orders` (
  `iStoreOrderId` int(10) UNSIGNED NOT NULL,
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iOrderAllocationId` int(10) UNSIGNED NOT NULL,
  `tiStatus` tinyint(4) NOT NULL COMMENT '0 - Pending , 1 - Received , 2 - Rejected , 3 - Processed',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_order_master`
--

CREATE TABLE `sub_order_master` (
  `iSubOrderId` int(10) UNSIGNED NOT NULL,
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iOrderAllocationId` int(10) UNSIGNED DEFAULT NULL,
  `tiSubOrderStatus` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - Pending , 1 - Received , 2 - Rejected , 3 - Processed',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vFullName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vISDCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vMobileNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vOTP` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iOTPExpireAt` int(10) UNSIGNED DEFAULT NULL,
  `tiIsMobileVerified` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - No, 1 - Yes',
  `vLanguage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en' COMMENT 'en - English, ar - Arablic',
  `vProfilePic` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vFacebookId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vGoogleId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAppleId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiIsSocialLogin` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - No, 1 - Yes',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vPasswordResetToken` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iPasswordExpireAt` int(11) NOT NULL,
  `enType` enum('Customer','Partner','Driver') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Customer',
  `enStatus` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `tiAdminApproved` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-No, 1- Yes',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiIsGuest` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 = Normal User, 1 = Guest',
  `enIsDeleted` enum('No','Yes') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `iLastLoginAt` int(10) UNSIGNED DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iDeletedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iUserId`, `vFullName`, `email`, `vISDCode`, `vMobileNumber`, `vOTP`, `iOTPExpireAt`, `tiIsMobileVerified`, `vLanguage`, `vProfilePic`, `vFacebookId`, `vGoogleId`, `vAppleId`, `tiIsSocialLogin`, `password`, `vPasswordResetToken`, `iPasswordExpireAt`, `enType`, `enStatus`, `tiAdminApproved`, `remember_token`, `tiIsGuest`, `enIsDeleted`, `iLastLoginAt`, `iCreatedAt`, `iUpdatedAt`, `iDeletedAt`) VALUES
(13, 'Archita Rathod', 'architar.spaceo@gmail.com', '+91', '9228723322', '949713', 1614166515, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$bClXnGAE9PDrIZIdHFUBF.h1thVKrDGvQ4r217ah4hy9i11Bzltv6', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614165915, 1614165915, NULL),
(14, 'Archita Rathod', 'hardik@gmail.com', '+91', '7990915050', '850930', 1614169961, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$3aUxr38EJl8f1EMA120pkuI5Tgd6DnFMtEPNUIIXCREs76z8afxqi', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614169361, 1614169361, NULL),
(15, 'jayraj', 'jay@gmail.com', '+91', '7784445222', '785259', 1614172659, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$50rFdbm6QEs4xuQCX0lbFejrxDI9.yj1RtiWVOKKzF4uW9nfgaQ2u', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614172059, 1614172059, NULL),
(16, 'jayraj', 'jayadskfj@gmail.com', '+91', '7784445225', '658567', 1614172955, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$I2P7B//NW/EfKmZEOLv9xeTSi6IgTPWwX2heTJfUJPn3uS0Iz3H.K', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614172355, 1614172355, NULL),
(17, 'Archita Rathod', 'hardkkkk@gmail.com', '+91', '9228725326', '184736', 1614176520, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$MruQMCZiZSK4.uEloHbCqOc7CIvpz34fV/4rYNvYh4oSAw6xvgpwG', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614175920, 1614175920, NULL),
(18, 'jaydip panchal', 'jaydippanchal@gmail.com', '+91', '7046130888', '284411', 1614249635, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$e.xzG0o2FdkRgjF/S7VX2e5EmNqkBcRkMttCj0fxI7ktJLLOOAfle', NULL, 0, 'Driver', 'Active', 0, '2', 0, 'No', NULL, 1614249035, 1614249035, NULL),
(19, 'Archita Rathod', 'architar.spa@gmail.com', '+91', '9228723789', '231529', 1614251688, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$I2zXJO5rTJqAG.LNm63xdu8OymwQLjsjYUU5Kp99P6y0vH1TGW5Uy', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614251088, 1614251088, NULL),
(20, 'Archita Rathod', 'architar.spadd@gmail.com', '+91', '9228723999', '814546', 1614251742, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$R8G7l6DWRmpZEA4QCUQ/.OvFAKxv0HO2BfshQyrkLG9u0zVU9iWrC', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614251142, 1614251142, NULL),
(21, 'Archita Rathod', 'jayrajsinh@gmail.com', '+91', '78787878', '910413', 1614322143, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$0PGbFzV8XNs256NaKXG4h..tQHSMVWmvc6zp1nUDbCPf/bMYq9cR6', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614321543, 1614321543, NULL),
(22, 'Archita Rathod', 'jayraj@gmail.com', '+91', '1212121212', '161014', 1614322620, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$o5WLi8otbGTfSWHlBEEh7.HMZQLe0XOclG3FZUGd2wPQATwzBsNuq', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614321725, 1614321725, NULL),
(23, 'Archita Rathod', 'archi.spaceo@gmail.com', '+91', '7990915007', '711151', 1614603867, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$lyBxLMKM0794o5u9S4WCB.ZkOb7JlQwOI1YLUel4JpLkf1DoztHNW', NULL, 0, 'Driver', 'Active', 0, '1', 0, 'No', NULL, 1614602678, 1614602678, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE `user_notification` (
  `iUserNotificationId` int(10) UNSIGNED NOT NULL,
  `iReceiveByUserId` int(10) UNSIGNED NOT NULL,
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `vNotificationTitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vNotificationDesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txDynamicField` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vNotificationTag` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiNotificationUserType` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Customer, 2 - Store, 3 - Driver, 4 - Admin',
  `tiIsRead` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Yes, 0 - No',
  `tiIsDeleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Yes, 0 - No',
  `tiIsActive` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Yes, 0 - No',
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iDeletedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat`
--
ALTER TABLE `cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`iCategoryId`);

--
-- Indexes for table `catrecord`
--
ALTER TABLE `catrecord`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`iCMSPageId`);

--
-- Indexes for table `contact_us_reason`
--
ALTER TABLE `contact_us_reason`
  ADD PRIMARY KEY (`iContactReasonId`);

--
-- Indexes for table `contact_us_request`
--
ALTER TABLE `contact_us_request`
  ADD PRIMARY KEY (`iRequestId`),
  ADD KEY `contact_us_request_iuserid_foreign` (`iUserId`),
  ADD KEY `contact_us_request_icontactreasonid_foreign` (`iContactReasonId`);

--
-- Indexes for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD PRIMARY KEY (`iCustomerAddressId`),
  ADD KEY `customer_addresses_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `decline_reason`
--
ALTER TABLE `decline_reason`
  ADD PRIMARY KEY (`iDeclineReasonId`);

--
-- Indexes for table `device_details`
--
ALTER TABLE `device_details`
  ADD PRIMARY KEY (`iDeviceId`),
  ADD KEY `device_details_ideviceid_index` (`iDeviceId`),
  ADD KEY `device_details_iuserid_index` (`iUserId`);

--
-- Indexes for table `driver_declined_order`
--
ALTER TABLE `driver_declined_order`
  ADD PRIMARY KEY (`iDeclinedOrderId`),
  ADD KEY `driver_declined_order_ideclinereasonid_foreign` (`iDeclineReasonId`),
  ADD KEY `driver_declined_order_idriverid_foreign` (`iDriverId`),
  ADD KEY `driver_declined_order_iorderid_foreign` (`iOrderId`);

--
-- Indexes for table `driver_details`
--
ALTER TABLE `driver_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `driver_details_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`iQuestionId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD PRIMARY KEY (`iOrderedProductId`),
  ADD KEY `ordered_products_iorderid_foreign` (`iOrderId`),
  ADD KEY `ordered_products_iproductid_foreign` (`iProductId`),
  ADD KEY `ordered_products_iproductvariantid_foreign` (`iProductVariantId`),
  ADD KEY `ordered_products_istoreid_foreign` (`iStoreId`);

--
-- Indexes for table `order_allocation`
--
ALTER TABLE `order_allocation`
  ADD PRIMARY KEY (`iOrderAllocationId`),
  ADD KEY `order_allocation_idriverid_foreign` (`iDriverId`),
  ADD KEY `order_allocation_icanceledbyuserid_foreign` (`iCanceledByUserId`);

--
-- Indexes for table `order_master`
--
ALTER TABLE `order_master`
  ADD PRIMARY KEY (`iOrderId`),
  ADD KEY `order_master_iuserid_foreign` (`iUserId`),
  ADD KEY `order_master_ipromocodeid_foreign` (`iPromoCodeId`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_iuserid_foreign` (`iUserId`),
  ADD KEY `product_icategoryid_foreign` (`iCategoryId`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`iProductImageId`),
  ADD KEY `product_images_iproductid_foreign` (`iProductId`);

--
-- Indexes for table `product_liked`
--
ALTER TABLE `product_liked`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_liked_iproductid_foreign` (`iProductId`),
  ADD KEY `product_liked_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_variants_iproductid_foreign` (`iProductId`);

--
-- Indexes for table `promo_code`
--
ALTER TABLE `promo_code`
  ADD PRIMARY KEY (`iPromoCodeId`);

--
-- Indexes for table `recent_search`
--
ALTER TABLE `recent_search`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recent_search_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`iSettingsId`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`iStoreId`),
  ADD KEY `store_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `store_images`
--
ALTER TABLE `store_images`
  ADD PRIMARY KEY (`iStoreImageId`),
  ADD KEY `store_images_istoreid_foreign` (`iStoreId`);

--
-- Indexes for table `store_orders`
--
ALTER TABLE `store_orders`
  ADD PRIMARY KEY (`iStoreOrderId`),
  ADD KEY `store_orders_iorderid_foreign` (`iOrderId`),
  ADD KEY `store_orders_istoreid_foreign` (`iStoreId`),
  ADD KEY `store_orders_iorderallocationid_foreign` (`iOrderAllocationId`);

--
-- Indexes for table `sub_order_master`
--
ALTER TABLE `sub_order_master`
  ADD PRIMARY KEY (`iSubOrderId`),
  ADD KEY `sub_order_master_iorderid_foreign` (`iOrderId`),
  ADD KEY `sub_order_master_istoreid_foreign` (`iStoreId`),
  ADD KEY `sub_order_master_iorderallocationid_foreign` (`iOrderAllocationId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iUserId`),
  ADD UNIQUE KEY `users_vemailid_unique` (`email`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`iUserNotificationId`),
  ADD KEY `user_notification_ireceivebyuserid_foreign` (`iReceiveByUserId`),
  ADD KEY `user_notification_iorderid_foreign` (`iOrderId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cat`
--
ALTER TABLE `cat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `iCategoryId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `catrecord`
--
ALTER TABLE `catrecord`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `iCMSPageId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us_reason`
--
ALTER TABLE `contact_us_reason`
  MODIFY `iContactReasonId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us_request`
--
ALTER TABLE `contact_us_request`
  MODIFY `iRequestId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  MODIFY `iCustomerAddressId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `decline_reason`
--
ALTER TABLE `decline_reason`
  MODIFY `iDeclineReasonId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `device_details`
--
ALTER TABLE `device_details`
  MODIFY `iDeviceId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `driver_declined_order`
--
ALTER TABLE `driver_declined_order`
  MODIFY `iDeclinedOrderId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver_details`
--
ALTER TABLE `driver_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `iQuestionId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `ordered_products`
--
ALTER TABLE `ordered_products`
  MODIFY `iOrderedProductId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_allocation`
--
ALTER TABLE `order_allocation`
  MODIFY `iOrderAllocationId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_master`
--
ALTER TABLE `order_master`
  MODIFY `iOrderId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `iProductImageId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_liked`
--
ALTER TABLE `product_liked`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promo_code`
--
ALTER TABLE `promo_code`
  MODIFY `iPromoCodeId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recent_search`
--
ALTER TABLE `recent_search`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `iSettingsId` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `iStoreId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `store_images`
--
ALTER TABLE `store_images`
  MODIFY `iStoreImageId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `store_orders`
--
ALTER TABLE `store_orders`
  MODIFY `iStoreOrderId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_order_master`
--
ALTER TABLE `sub_order_master`
  MODIFY `iSubOrderId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iUserId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `iUserNotificationId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contact_us_request`
--
ALTER TABLE `contact_us_request`
  ADD CONSTRAINT `contact_us_request_icontactreasonid_foreign` FOREIGN KEY (`iContactReasonId`) REFERENCES `contact_us_reason` (`iContactReasonId`) ON DELETE CASCADE,
  ADD CONSTRAINT `contact_us_request_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD CONSTRAINT `customer_addresses_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `driver_declined_order`
--
ALTER TABLE `driver_declined_order`
  ADD CONSTRAINT `driver_declined_order_ideclinereasonid_foreign` FOREIGN KEY (`iDeclineReasonId`) REFERENCES `decline_reason` (`iDeclineReasonId`) ON DELETE CASCADE,
  ADD CONSTRAINT `driver_declined_order_idriverid_foreign` FOREIGN KEY (`iDriverId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE,
  ADD CONSTRAINT `driver_declined_order_iorderid_foreign` FOREIGN KEY (`iOrderId`) REFERENCES `order_master` (`iOrderId`) ON DELETE CASCADE;

--
-- Constraints for table `driver_details`
--
ALTER TABLE `driver_details`
  ADD CONSTRAINT `driver_details_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD CONSTRAINT `ordered_products_iorderid_foreign` FOREIGN KEY (`iOrderId`) REFERENCES `order_master` (`iOrderId`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordered_products_iproductid_foreign` FOREIGN KEY (`iProductId`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordered_products_iproductvariantid_foreign` FOREIGN KEY (`iProductVariantId`) REFERENCES `product_variants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordered_products_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE;

--
-- Constraints for table `order_allocation`
--
ALTER TABLE `order_allocation`
  ADD CONSTRAINT `order_allocation_icanceledbyuserid_foreign` FOREIGN KEY (`iCanceledByUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_allocation_idriverid_foreign` FOREIGN KEY (`iDriverId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `order_master`
--
ALTER TABLE `order_master`
  ADD CONSTRAINT `order_master_ipromocodeid_foreign` FOREIGN KEY (`iPromoCodeId`) REFERENCES `promo_code` (`iPromoCodeId`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_master_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_icategoryid_foreign` FOREIGN KEY (`iCategoryId`) REFERENCES `categories` (`iCategoryId`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_iproductid_foreign` FOREIGN KEY (`iProductId`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_liked`
--
ALTER TABLE `product_liked`
  ADD CONSTRAINT `product_liked_iproductid_foreign` FOREIGN KEY (`iProductId`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_liked_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD CONSTRAINT `product_variants_iproductid_foreign` FOREIGN KEY (`iProductId`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recent_search`
--
ALTER TABLE `recent_search`
  ADD CONSTRAINT `recent_search_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `store_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `store_images`
--
ALTER TABLE `store_images`
  ADD CONSTRAINT `store_images_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE;

--
-- Constraints for table `store_orders`
--
ALTER TABLE `store_orders`
  ADD CONSTRAINT `store_orders_iorderallocationid_foreign` FOREIGN KEY (`iOrderAllocationId`) REFERENCES `order_allocation` (`iOrderAllocationId`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_orders_iorderid_foreign` FOREIGN KEY (`iOrderId`) REFERENCES `order_master` (`iOrderId`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_orders_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE;

--
-- Constraints for table `sub_order_master`
--
ALTER TABLE `sub_order_master`
  ADD CONSTRAINT `sub_order_master_iorderallocationid_foreign` FOREIGN KEY (`iOrderAllocationId`) REFERENCES `order_allocation` (`iOrderAllocationId`) ON DELETE CASCADE,
  ADD CONSTRAINT `sub_order_master_iorderid_foreign` FOREIGN KEY (`iOrderId`) REFERENCES `order_master` (`iOrderId`) ON DELETE CASCADE,
  ADD CONSTRAINT `sub_order_master_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE;

--
-- Constraints for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD CONSTRAINT `user_notification_iorderid_foreign` FOREIGN KEY (`iOrderId`) REFERENCES `order_master` (`iOrderId`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_notification_ireceivebyuserid_foreign` FOREIGN KEY (`iReceiveByUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;
--
-- Database: `choward-test`
--
CREATE DATABASE IF NOT EXISTS `choward-test` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `choward-test`;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin@basecode.com', '$2y$10$Vz7TTJ913ihGErJMDj42Aeh7EuU3ZodJJ89hfvZF/CLiHXWRJ4WU2', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `vBrandName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enStatus` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `enIsDeleted` enum('No','Yes') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `vBrandName`, `enStatus`, `enIsDeleted`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 'Baskin Robbins', 'Active', 'No', NULL, NULL),
(2, 'Betty Crocker', 'Active', 'No', NULL, NULL),
(3, 'Cadbury Cake Bars', 'Active', 'No', NULL, NULL),
(4, 'Dunkin\'s Donuts', 'Active', 'No', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `iCategoryId` int(10) UNSIGNED NOT NULL,
  `vCategoryName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vCategoryName_ar` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vCategorySlug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vCategoryImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enStatus` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `enIsDeleted` enum('No','Yes') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`iCategoryId`, `vCategoryName`, `vCategoryName_ar`, `vCategorySlug`, `vCategoryImage`, `enStatus`, `enIsDeleted`, `iCreatedAt`, `iUpdatedAt`) VALUES
(3, 'Cake', 'Cake', 'cake', 'cake.png', 'Active', 'No', 1606475794, 1606475794),
(4, 'Chocolates', 'Chocolates', 'chocolates', 'chocolate.png', 'Active', 'No', 1606475816, 1606475816),
(5, 'Birthday Cards', 'Birthday Cards', 'birthday-cards', 'card.png', 'Active', 'No', 1606475840, 1606475840),
(6, 'Flowers', 'Flowers', 'flowers', 'flowers.png', 'Active', 'No', 1606475859, 1606475859);

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `iCMSPageId` int(10) UNSIGNED NOT NULL,
  `enUserType` enum('Customer','Partner','Driver') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Customer',
  `vPageNameEN` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vPageNameAR` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txContentEN` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `txContentAR` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`iCMSPageId`, `enUserType`, `vPageNameEN`, `vPageNameAR`, `txContentEN`, `txContentAR`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 'Customer', 'Terms & Conditions', 'Terms & Conditions', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque congue eros ante, dapibus efficitur neque aliquet in. Nulla eu orci felis. Nulla pretium enim at erat vehicula dictum. Nullam lacinia non ligula eu tempor. Cras consequat mollis convallis. Pellentesque ligula ex, feugiat id fringilla vel, condimentum sit amet ex. Quisque ac condimentum erat. Vivamus pharetra ac dui id pharetra. Proin eget vestibulum lectus, quis pellentesque nulla. Nunc consequat lacus nunc, sit amet fermentum felis interdum sed. Sed malesuada ultricies semper. Quisque et purus leo. Fusce porta, eros sit amet sagittis vulputate, tellus urna ultricies nibh, quis rutrum leo ligula eget mauris.\r\n\r\nProin tellus sem, lobortis ultricies ante vel, faucibus placerat orci. Integer molestie velit sed eros pellentesque blandit. Nunc non dolor ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lorem quam, feugiat vel nulla et, tincidunt consequat lectus. Pellentesque dui dolor, pellentesque sit amet convallis quis, ultrices et ante. Duis dapibus accumsan mi id ornare. Donec ac scelerisque nunc, id condimentum lectus. Nullam non velit ante. Sed laoreet varius sem, quis facilisis lorem congue eu. Sed sollicitudin malesuada risus, at venenatis quam tempus ut. Curabitur dictum lectus quam, eget finibus sem imperdiet eu.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque congue eros ante, dapibus efficitur neque aliquet in. Nulla eu orci felis. Nulla pretium enim at erat vehicula dictum. Nullam lacinia non ligula eu tempor. Cras consequat mollis convallis. Pellentesque ligula ex, feugiat id fringilla vel, condimentum sit amet ex. Quisque ac condimentum erat. Vivamus pharetra ac dui id pharetra. Proin eget vestibulum lectus, quis pellentesque nulla. Nunc consequat lacus nunc, sit amet fermentum felis interdum sed. Sed malesuada ultricies semper. Quisque et purus leo. Fusce porta, eros sit amet sagittis vulputate, tellus urna ultricies nibh, quis rutrum leo ligula eget mauris.\r\n\r\nProin tellus sem, lobortis ultricies ante vel, faucibus placerat orci. Integer molestie velit sed eros pellentesque blandit. Nunc non dolor ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lorem quam, feugiat vel nulla et, tincidunt consequat lectus. Pellentesque dui dolor, pellentesque sit amet convallis quis, ultrices et ante. Duis dapibus accumsan mi id ornare. Donec ac scelerisque nunc, id condimentum lectus. Nullam non velit ante. Sed laoreet varius sem, quis facilisis lorem congue eu. Sed sollicitudin malesuada risus, at venenatis quam tempus ut. Curabitur dictum lectus quam, eget finibus sem imperdiet eu.', 1609218030, 1609218030),
(2, 'Customer', 'Privacy Policy', 'Privacy Policy', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque congue eros ante, dapibus efficitur neque aliquet in. Nulla eu orci felis. Nulla pretium enim at erat vehicula dictum. Nullam lacinia non ligula eu tempor. Cras consequat mollis convallis. Pellentesque ligula ex, feugiat id fringilla vel, condimentum sit amet ex. Quisque ac condimentum erat. Vivamus pharetra ac dui id pharetra. Proin eget vestibulum lectus, quis pellentesque nulla. Nunc consequat lacus nunc, sit amet fermentum felis interdum sed. Sed malesuada ultricies semper. Quisque et purus leo. Fusce porta, eros sit amet sagittis vulputate, tellus urna ultricies nibh, quis rutrum leo ligula eget mauris.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque congue eros ante, dapibus efficitur neque aliquet in. Nulla eu orci felis. Nulla pretium enim at erat vehicula dictum. Nullam lacinia non ligula eu tempor. Cras consequat mollis convallis. Pellentesque ligula ex, feugiat id fringilla vel, condimentum sit amet ex. Quisque ac condimentum erat. Vivamus pharetra ac dui id pharetra. Proin eget vestibulum lectus, quis pellentesque nulla. Nunc consequat lacus nunc, sit amet fermentum felis interdum sed. Sed malesuada ultricies semper. Quisque et purus leo. Fusce porta, eros sit amet sagittis vulputate, tellus urna ultricies nibh, quis rutrum leo ligula eget mauris.', 1609218061, 1609218061),
(3, 'Customer', 'Cookie Policy', 'Cookie Policy', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque congue eros ante, dapibus efficitur neque aliquet in. Nulla eu orci felis. Nulla pretium enim at erat vehicula dictum. Nullam lacinia non ligula eu tempor. Cras consequat mollis convallis. Pellentesque ligula ex, feugiat id fringilla vel, condimentum sit amet ex. Quisque ac condimentum erat. Vivamus pharetra ac dui id pharetra. Proin eget vestibulum lectus, quis pellentesque nulla. Nunc consequat lacus nunc, sit amet fermentum felis interdum sed. Sed malesuada ultricies semper. Quisque et purus leo. Fusce porta, eros sit amet sagittis vulputate, tellus urna ultricies nibh, quis rutrum leo ligula eget mauris.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque congue eros ante, dapibus efficitur neque aliquet in. Nulla eu orci felis. Nulla pretium enim at erat vehicula dictum. Nullam lacinia non ligula eu tempor. Cras consequat mollis convallis. Pellentesque ligula ex, feugiat id fringilla vel, condimentum sit amet ex. Quisque ac condimentum erat. Vivamus pharetra ac dui id pharetra. Proin eget vestibulum lectus, quis pellentesque nulla. Nunc consequat lacus nunc, sit amet fermentum felis interdum sed. Sed malesuada ultricies semper. Quisque et purus leo. Fusce porta, eros sit amet sagittis vulputate, tellus urna ultricies nibh, quis rutrum leo ligula eget mauris.', 1609218085, 1609218085),
(4, 'Driver', 'Terms & Conditions', 'Terms & Conditions', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.</p>', 1610009836, 1610009836),
(5, 'Driver', 'Privacy Policy', 'Privacy Policy', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.</p>', 1610009860, 1610009860),
(6, 'Driver', 'About Us', 'About Us', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.</p>', 1610009882, 1610009882);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_reason`
--

CREATE TABLE `contact_us_reason` (
  `iContactReasonId` int(10) UNSIGNED NOT NULL,
  `tiUserType` tinyint(4) NOT NULL COMMENT '1-Customer, 2-Partner, 3-Driver',
  `vReasonEN` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vReasonAR` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiIsDeleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-No, 1-Yes',
  `tiIsActive` tinyint(4) NOT NULL COMMENT '0-Inactive, 1-Active',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us_reason`
--

INSERT INTO `contact_us_reason` (`iContactReasonId`, `tiUserType`, `vReasonEN`, `vReasonAR`, `tiIsDeleted`, `tiIsActive`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 1, 'Reason 1', 'السبب 1', 0, 1, 1609217962, 1609217962),
(2, 1, 'Reason 2', 'السبب 2', 0, 1, 1609217981, 1609217981),
(3, 1, 'Reason 3', 'السبب 3', 0, 1, 1609217995, 1609217995),
(4, 3, 'Reason 1', 'السبب 1', 0, 1, 1610011410, 1610011410),
(5, 3, 'Reason 2', 'السبب 2', 0, 1, 1610011420, 1610011420),
(6, 3, 'Reason 3', 'السبب 3', 0, 1, 1610011430, 1610011430);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_request`
--

CREATE TABLE `contact_us_request` (
  `iRequestId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `iContactReasonId` int(10) UNSIGNED NOT NULL,
  `txDescription` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us_request`
--

INSERT INTO `contact_us_request` (`iRequestId`, `iUserId`, `iContactReasonId`, `txDescription`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 107, 1, 'Testtttty\nF\nF\nF\nF\nT\nF\nT', 1609238847, 1609238847),
(2, 145, 4, 'test 1', 1612532623, 1612532623);

-- --------------------------------------------------------

--
-- Table structure for table `customer_addresses`
--

CREATE TABLE `customer_addresses` (
  `iCustomerAddressId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vFullName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vISDCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vMobileNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vAddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiIsSelected` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - No, 1 - YES',
  `tiAddressType` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - Home, 1 - Office, 2-Others',
  `fLongitude` decimal(10,8) DEFAULT 0.00000000,
  `fLatitude` decimal(10,8) DEFAULT 0.00000000,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_addresses`
--

INSERT INTO `customer_addresses` (`iCustomerAddressId`, `iUserId`, `vFullName`, `vISDCode`, `vMobileNumber`, `vAddress`, `tiIsSelected`, `tiAddressType`, `fLongitude`, `fLatitude`, `iCreatedAt`, `iUpdatedAt`) VALUES
(11, 50, 'Tony Starc', '+966', '9137921629', '257, Test, test, test 2935323', 0, 0, '0.00000000', '0.00000000', 1607506042, 1607948197),
(17, 48, 'IOS Testing', '+1', '9898989898', 'Space-O Technologies, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat, India', 0, 0, '0.00000000', '0.00000000', 1607507388, 1609225812),
(18, 48, 'IOS Testing', '+1', '9632587412', 'Space-O Technologies (Canada), County Court Boulevard, Brampton, ON, Canada', 1, 1, '0.00000000', '0.00000000', 1607507443, 1609225812),
(19, 50, 'Gal Gadot', '+966', '8460015666', '257, Test, test, test 2935323', 1, 1, '0.00000000', '0.00000000', 1607665183, 1607948197),
(20, 56, 'Tesr', '+966', '1234567890', '257, Test, test, test 2935323', 1, 1, '0.00000000', '0.00000000', 1608030644, 1608030658),
(21, 65, 'JJJ', '+91', '789987789', 'F-1, Micheal St. LA', 1, 0, '72.52211090', '23.03361590', 1608179791, 1608179791),
(22, 72, 'John', '+91', '9898898988', 'F-1, Micheal St. LA', 1, 0, '72.52211090', '23.03361590', 1608199923, 1608199923),
(24, 105, 'Mihir space o', '+966', '91379261978', '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 1, 0, '0.00000000', '0.00000000', 1608548633, 1608548721),
(26, 107, 'Test', '+966', '111111111111', 'Morbi, Gujarat, India', 1, 1, '0.00000000', '0.00000000', 1609335100, 1609335100),
(27, 122, 'mihir shah', '+966', '9137921629', '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 1, 0, '0.00000000', '0.00000000', 1609914634, 1609914641),
(28, 121, 'mihir shah', '+966', '9137921629', '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 1, 0, '0.00000000', '0.00000000', 1611380082, 1611380084),
(29, 151, 'Sagar Bh', '+966', '15171517', 'Morbi, Gujarat, India', 1, 0, '0.00000000', '0.00000000', 1611558699, 1611558699);

-- --------------------------------------------------------

--
-- Table structure for table `decline_reason`
--

CREATE TABLE `decline_reason` (
  `iDeclineReasonId` int(10) UNSIGNED NOT NULL,
  `tiUserType` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1-Driver, 2-Store Portal ',
  `vReasonEN` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vReasonAR` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiReasonType` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-Decline, 1-Cancel',
  `tiIsDeleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-No, 1-Yes',
  `tiIsActive` tinyint(4) NOT NULL COMMENT '0-Inactive, 1-Active',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `decline_reason`
--

INSERT INTO `decline_reason` (`iDeclineReasonId`, `tiUserType`, `vReasonEN`, `vReasonAR`, `tiReasonType`, `tiIsDeleted`, `tiIsActive`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 1, 'Reason 1', 'السبب 1', 1, 0, 1, 1610011144, 1610011144),
(2, 1, 'Reason 2', 'السبب 2', 1, 0, 1, 1610011162, 1610011162),
(3, 1, 'Reason 1', 'السبب 1', 0, 0, 1, 1610011172, 1610011172),
(4, 1, 'Reason 2', 'السبب 2', 0, 0, 1, 1610011186, 1610011186),
(5, 2, 'Reason 1', 'Reason 1', 0, 0, 1, 1612441768, 1612441768);

-- --------------------------------------------------------

--
-- Table structure for table `device_details`
--

CREATE TABLE `device_details` (
  `iDeviceId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vAccessToken` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vDeviceToken` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vDeviceId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiDeviceType` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '0 => Web, 1 => Android,2 => IOS',
  `vDeviceName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED NOT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `device_details`
--

INSERT INTO `device_details` (`iDeviceId`, `iUserId`, `vAccessToken`, `vDeviceToken`, `vDeviceId`, `tiDeviceType`, `vDeviceName`, `iCreatedAt`, `iUpdatedAt`) VALUES
(19, 44, '25a7c1b218d741f8db0ae8f635947a37', 'dfdf', '', 0, 'iPhone X', 1606898398, 1606898398),
(21, 46, '53f4f2e403eff9e5bfeb145c328a15c9', 'dfdf', '', 0, 'iPhone X', 1606898457, 1606898457),
(22, 47, 'd32ac23f8a5f50a3e83c6910426d85aa', 'dfdf', '', 0, 'iPhone X', 1606898480, 1606898480),
(23, 48, '31d1db789b7608f10249569ab9b9fe9f', 'arluWP3Nedtriob', '48893690-B599-4F65-AE55-DD0AEE4AF711', 2, 'iPhone 12 Pro Max', 1611557958, 1611557958),
(25, 50, '2c8faadb09a0569a003f9ba9dea35914', NULL, '', 1, 'Xiaomi Redmi Note 7 Pro', 1608092115, 1608092115),
(26, 51, NULL, '1222221212121222222222asasassa', '', 2, NULL, 1607776006, 1607776006),
(27, 53, NULL, '1222221212121asasasas', '', 2, NULL, 1607776173, 1607776173),
(28, 54, NULL, '122222121212122222222222222222222', '', 2, NULL, 1607776308, 1607776308),
(29, 55, NULL, '23333333333333333', '', 0, NULL, 1607777445, 1607777445),
(30, 56, '696ccd2ecd9c839e275e30edba8390cf', 'ch05jidgSyqzciLS4yK7-w:APA91bHSpiOnPmAfbQ6CHyIBuUdidXp_q3ouh7LLWF-vQUoYIoOr8lCsgLdRXDuZiaRy0GIz0m2OkPJa3rpS-DIhOZQ7fkW-4OvTl2Kp4FziqbYW7FryN393j0oLWOh9U4xhMMrVXq08', '', 1, 'HUAWEI BND-AL10', 1608115402, 1608115402),
(31, 57, '6c9a2800c8db6ad51f0c4e7fc66cdc20', '958N5CWiYhzzD17', '', 2, 'iPhone', 1607950432, 1607950432),
(32, 58, '7720d9ff3b2b780e0d7eac4683593b07', 'sdfassssssssss', '', 2, 'iPhone X', 1607953850, 1607953850),
(33, 59, 'b04db1d4504658639b5fe69634247669', 'kdjakdjasdsakldjsaokdjsalkdjsakdsa', '', 2, 'iPhone X', 1607954118, 1607954118),
(34, 60, '726fac211b0fdd1672e7ae7e68129255', 'dadasadsadsakjdhsakjdhsa', '', 2, 'iPhone X', 1607954235, 1607954235),
(35, 61, 'c6d0f708e74106e7ccffca95839d0559', 'dmpdksapksakposadiposadiposadpiosa', '', 2, 'iPhone X', 1607954307, 1607954307),
(36, 62, '122bb3d5c51c85e7360360e2b4f2f92d', 'dx4JK4plt0g7j10EydqsH2:APA91bFgv8nZLk9uk4fpOKukgY4xIoPXZbiKct6g71g--pZ8RJwkNmMsmU__xbN94XvMQwFuzoV5jWhSGRF3ecpKxCHXBU5u-GjbUb7kCKzUhU2wChEUeYJyf4QgQNavB18etjaEt2DO', 'A44C1906-8117-4819-B8FA-8ADD8A60B00A', 2, 'iPhone 12 Pro Max', 1608024552, 1608025076),
(37, 63, '08fb35b516126cf9680f2065b2385ba3', '1FN5GyvmfFTZCOe', '75F29975-C7DB-489A-92A0-F168768E4248', 2, 'iPhone 12 Pro Max', 1608027918, 1608028180),
(38, 64, '16080310e3b11442326ed5ce5b88f05c', '7xoO4TJxzRNE3jC', '61BBF189-EAE1-4D85-AF81-0CEF7D1C97F1', 2, 'iPhone 12 Pro Max', 1608029463, 1608030486),
(39, 65, '13b847af492ad369392576e888888b53', 'dfdf', '12456123456789123456789', 0, 'iPhone X', 1608031105, 1608031105),
(41, 67, '34117e868c137281f265429d9c85137f', 'f5mj_gwLaEyHjd-kgZavwB:APA91bHAr4xo5EoS5ZzLf40PTF71-uSkdwMN_8ANYZF0BMTaoNmQjsZhxN9lp4dnzahFKxOR4N8B3fJWdmvupz9roJpifnWezLlnXzp79QWIOnCqiqqlLSZAPfVsgu1oBgsSUiTpfaZs', '73E860B1-D8CA-436A-A95A-A02A79365CC9', 2, 'iPhone 12 Pro Max', 1608034899, 1608034899),
(43, 69, 'a200898f3ab99eeaf1cb6598e469c43e', 'notFound', 'e940320f484db1ac', 1, 'Xiaomi Redmi Note 7 Pro', 1608091864, 1608091864),
(44, 72, '2275dc0fce871215185d5f2c6f0eff56', 'dfdf', 'adaweqerfs', 0, 'iPhone X', 1608199885, 1608199885),
(46, 102, NULL, NULL, '', 2, 'iPhone 12 Pro Max', 1608292287, 1608292333),
(49, 105, '0e23ac3e9357fbf1d7c3cdc8da636f17', 'notFound', '', 1, 'OPPO CPH1969', 1608532440, 1608532440),
(50, 106, 'fcba1b74816da15b2f7e4882d264a309', 'notFound', '31d3bc7192f6a917', 1, 'Google sdk_gphone_x86', 1609230470, 1609230470),
(52, 108, '054c5570eb1bf0e6f8b0466ee17fcfeb', 'notFound', 'cee4e0c7ac9505a1', 1, 'Unknown Android SDK built for x86', 1609238642, 1609238642),
(55, 116, 'aaf12e3a978ba863e1e660468624f97b', NULL, '', 1, 'Genymobile Samsung Galaxy S10', 1611382074, 1611382074),
(56, 117, '46e64643ed91ea3048f8a00cc8e44631', 'notFound', '', 1, 'OPPO CPH1969', 1609737790, 1609737790),
(57, 118, 'f049975fadd0c001ee101fb20d82ad39', NULL, '', 1, 'OPPO CPH1969', 1609739643, 1609739643),
(58, 119, 'cd6a8362c2da5a4977beca7d14819036', 'notFound', '', 1, 'HUAWEI BND-AL10', 1609830453, 1609830453),
(59, 120, '0d27b84361249032d6c57e437462af3e', 'notFound', '', 1, 'OPPO CPH1969', 1609849095, 1609849095),
(60, 121, '00e322b3ea8713120b96087c545561ba', NULL, '0d8b6271ad268e5c', 1, 'OPPO CPH1969', 1611380732, 1611380732),
(62, 122, NULL, NULL, '', 1, 'OPPO CPH1969', 1609851672, 1609994730),
(63, 123, NULL, NULL, '', 1, 'OPPO CPH1969', 1609994732, 1609994739),
(65, 125, '4c6bc9829346709038e074e79b16a6d7', 'fG0ox5wuSt6lzfzyF0xvl1:APA91bGx-vRypCNGvTRaIqAt3NO7JEbJzfGYIGfU0lRFnkXaOSMdvRJX9SHwdVDAhOpOQgdZv4MLyV7zIoCX5o9qsPX-snKlUilWEkBhnQkwXees57xtPo5dR4B43HJ7vNFr7N-p9zUd', 'b78a9756ecc87616', 1, 'OPPO CPH1969', 1610006177, 1610006177),
(73, 132, NULL, NULL, '', 1, 'Vivo 1920', 1610010475, 1610010531),
(75, 133, NULL, NULL, '', 1, 'Vivo 1920', 1610011209, 1610011246),
(76, 134, '0026a9e8c2aacd03c727a9d621858f4a', 'eNYh0BmXSOONuKx8sEL2x9:APA91bH17Ij2Xeu1PAWpTz1E3B3siRzWn51zPX8P3cXx-Q3YCZOjX_jMbeojA8OBXK91oErlhdTsfvIZhL9MKtaVjTltbZsdP2UaDa6a0noKlZroB3aZz7EIkDPGc-OE7IGxPkNDhX6w', 'd4dd57403ae01114', 1, 'Vivo 1920', 1610014736, 1610014736),
(77, 135, NULL, NULL, '', 1, 'Vivo 1920', 1610015165, 1610086040),
(78, 136, '82d98146cd33deef231b74693dc8ec85', 'eNYh0BmXSOONuKx8sEL2x9:APA91bH17Ij2Xeu1PAWpTz1E3B3siRzWn51zPX8P3cXx-Q3YCZOjX_jMbeojA8OBXK91oErlhdTsfvIZhL9MKtaVjTltbZsdP2UaDa6a0noKlZroB3aZz7EIkDPGc-OE7IGxPkNDhX6w', 'd4dd57403ae01114', 1, 'Vivo 1920', 1610086048, 1610086048),
(79, 141, '7e1e67b47a3f423c7fb66742042f801d', 'notFound', '31d3bc7192f6a917', 1, 'Google sdk_gphone_x86', 1610104441, 1610104441),
(80, 142, '143a721a81a914bee117f7f9e76bc04e', 'fG0ox5wuSt6lzfzyF0xvl1:APA91bGx-vRypCNGvTRaIqAt3NO7JEbJzfGYIGfU0lRFnkXaOSMdvRJX9SHwdVDAhOpOQgdZv4MLyV7zIoCX5o9qsPX-snKlUilWEkBhnQkwXees57xtPo5dR4B43HJ7vNFr7N-p9zUd', 'b78a9756ecc87616', 1, 'OPPO CPH1969', 1610422776, 1610422776),
(81, 143, 'df6d151689655c25fa7b9ef0c37cdf15', 'notFound', '1e0448c8a320a93f', 1, 'Genymobile Samsung Galaxy S10', 1610454765, 1610454765),
(83, 145, 'a563f7cd5904e434944ac57f43617252', 'YYYYYYYYYYYYYYYYYYYYYYY', '', 2, 'dasdsad', 1611644181, 1611644181),
(84, 147, NULL, NULL, '', 1, 'HUAWEI BND-AL10', 1611325337, 1611379495),
(85, 148, 'a8de063d9e3334e379e385578c3a9bb8', NULL, '', 1, 'HUAWEI BND-AL10', 1611560066, 1611560066),
(86, 149, 'd16c3898135b6852f2dcf2bce2b4ba63', 'notFound', '1e0448c8a320a93f', 1, 'Genymobile Samsung Galaxy S10', 1611380322, 1611380322),
(87, 150, 'bf3437f5873cf18894b36f44981cc82b', 'notFound', 'cee4e0c7ac9505a1', 1, 'Unknown Android SDK built for x86', 1611556417, 1611556417),
(88, 151, 'a8e3bfd16b048a18d7a1f65ece6393fa', 'fELegp6jRwujf20eSF_5T7:APA91bFUs0oS-DjribSF-kv4DxxOsyqrOT8qxubtXy9O-fm7YNa_xXvUiZwmHhgOCOisF-kZj6Thrlr9GQC-EeBKJhUxZ3QHUp-q9O4dqTLhVIM86sxGAQPo6-abEP8a_BygmKBI-Fka', '32c032d3323ab692', 1, 'HUAWEI BND-AL10', 1611558216, 1611558216),
(89, 114, 'f886e3655a7230676fe27acf5ee20bc4', 'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', '', 1, 'Samsung Galaxy', 1611643495, 1611643495),
(91, 153, '4361c261e11bea27df7cd6f1680bc825', 'dfdf', 'sadadsasadsad', 0, 'iPhone X', 1613371841, 1613371841);

-- --------------------------------------------------------

--
-- Table structure for table `driver_declined_order`
--

CREATE TABLE `driver_declined_order` (
  `iDeclinedOrderId` int(10) UNSIGNED NOT NULL,
  `iDeclineReasonId` int(10) UNSIGNED NOT NULL,
  `iDriverId` int(10) UNSIGNED NOT NULL,
  `iOrderAllocationId` int(10) UNSIGNED NOT NULL,
  `vDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `driver_details`
--

CREATE TABLE `driver_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vLicenceFront` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vLicenceBack` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enVehicleType` enum('Bike','Car') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vVehicleBrandName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vVehicleModelName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vVehicleColor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vVehiclePlateNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vBankName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAccountHolderName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAccountNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vIBAN` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vRoutingNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vLocation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fLatitude` decimal(10,8) DEFAULT NULL,
  `fLongitude` decimal(11,8) DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `driver_details`
--

INSERT INTO `driver_details` (`id`, `iUserId`, `vLicenceFront`, `vLicenceBack`, `enVehicleType`, `vVehicleBrandName`, `vVehicleModelName`, `vVehicleColor`, `vVehiclePlateNumber`, `vBankName`, `vAccountHolderName`, `vAccountNumber`, `vIBAN`, `vRoutingNumber`, `vLocation`, `fLatitude`, `fLongitude`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 70, 'abc.png', 'abc.png', 'Bike', 'Royal Enfield', 'Royal Enfield Classic 350', 'Black', 'SHJSHH41212', 'sasadsad', 'asdasdas', 'dsadsadsadsadsadsa', 'dasdadasda', 'asdasdsadasdasda', 'adasdasdsadasdasda', NULL, NULL, NULL, NULL),
(2, 71, 'abc.png', 'abc.png', 'Car', 'Yamaha', 'Yamaha YZF R15 V3', 'Blue', 'DHDH455JMDJ', 'sasadsad', 'asdasdas', 'dsadsadsadsadsadsa', 'dasdadasda', 'asdasdsadasdasda', 'adasdasdsadasdasda', NULL, NULL, NULL, NULL),
(3, 114, 'frontLicence.png', 'backLicence.png', 'Bike', 'Honda', 'Hornet', 'Shiny Red', 'AB 12 XYZ 34', 'ABC Bank', 'Josh Mike', '9999 888 777 22', '888 777 33', '44 8888 2222', NULL, NULL, NULL, 1609327160, 1609327160),
(4, 115, 'frontLicence.png', 'backLicence.png', 'Bike', 'Honda', 'Hornet', 'Shiny Red', 'AB 12 XYZ 34', 'ABC Bank', 'Josh Mike', '9999 888 777 22', '888 777 33', '44 8888 2222', NULL, NULL, NULL, 1609327829, 1609327829),
(5, 121, 'null', 'null', '', 'null', 'null', 'null', 'null', 'bank details', 'holder', 'ac no', 'iban', 'routing', NULL, NULL, NULL, 1609850402, 1609850402),
(6, 145, 'frontLicence.png', 'backLicence.png', 'Bike', 'Honda', 'Hornet', 'Shiny Red', 'AB 12 XYZ 34', 'ABC Bank', 'Josh Mike', '4564 456 4566', '123 456 789', '44 8888 99', NULL, NULL, NULL, 1611306442, 1611306442),
(7, 156, '1613631413-431279-752647.png', '1613631413-610400-207072.png', 'Bike', 'Honda', 'splender', 'black', '5555', 'ICICI', 'Driver 21', '789654123654789', '456321', '789654', NULL, NULL, NULL, 1613631413, 1613631695);

-- --------------------------------------------------------

--
-- Table structure for table `driver_ratings`
--

CREATE TABLE `driver_ratings` (
  `iDriverRatingId` int(10) UNSIGNED NOT NULL,
  `iDriverId` int(10) UNSIGNED NOT NULL,
  `iOrderAllocationId` int(10) UNSIGNED NOT NULL,
  `iReviewByUserId` int(10) UNSIGNED NOT NULL,
  `fRating` double(2,1) NOT NULL DEFAULT 0.0,
  `vReview` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `driver_ratings`
--

INSERT INTO `driver_ratings` (`iDriverRatingId`, `iDriverId`, `iOrderAllocationId`, `iReviewByUserId`, `fRating`, `vReview`, `iCreatedAt`, `iUpdatedAt`) VALUES
(2, 145, 78, 151, 2.5, 'Dummy text Dummy text Dummy text Dummy textDummy textDummy text', 1611654059, 1611654059);

-- --------------------------------------------------------

--
-- Table structure for table `driver_running_trips`
--

CREATE TABLE `driver_running_trips` (
  `iDriverRunningTripId` int(10) UNSIGNED NOT NULL,
  `iDriverId` int(10) UNSIGNED NOT NULL,
  `iSubOrderId` int(10) UNSIGNED NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `driver_running_trips`
--

INSERT INTO `driver_running_trips` (`iDriverRunningTripId`, `iDriverId`, `iSubOrderId`, `iCreatedAt`, `iUpdatedAt`) VALUES
(4, 148, 20, 1611565709, 1611565709);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `iQuestionId` int(10) UNSIGNED NOT NULL,
  `vQuestionEN` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vQuestionAR` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `txAnswerEN` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `txAnswerAR` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiIsDeleted` tinyint(4) NOT NULL COMMENT '1-Yes, 0-No',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`iQuestionId`, `vQuestionEN`, `vQuestionAR`, `txAnswerEN`, `txAnswerAR`, `tiIsDeleted`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 'How to accept new order', 'How to accept new order', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.', 0, 1610009523, 1610009523),
(2, 'How to contact admin ?', 'How to contact admin ?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla, mi in porttitor luctus, risus turpis tristique sem, quis sollicitudin est turpis vitae quam. Donec sit amet cursus lacus. Suspendisse potenti. Aenean sagittis mattis velit ut varius. Curabitur eu metus sit amet nulla blandit placerat. Phasellus vel dignissim ipsum, vel efficitur turpis. Praesent id tincidunt leo. Vestibulum aliquet arcu id enim consectetur porttitor. Donec dapibus vel arcu vitae lobortis. Proin vitae lacinia felis. Maecenas in molestie dui. Mauris venenatis libero posuere mollis fermentum. In nec lacus eu metus ultrices vehicula a vitae leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida rhoncus urna. Suspendisse potenti.', 0, 1610009553, 1610009553);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2020_02_14_035943_create_admins_table', 1),
(3, '2020_02_20_111632_site-settings', 1),
(4, '2020_10_19_090833_create_device_table', 1),
(5, '2020_10_21_134053_create_categories_table', 1),
(6, '2020_11_24_133657_alter_users', 2),
(7, '2020_11_24_135504_alter_categories_table', 3),
(8, '2020_11_25_052603_alter_user_table', 4),
(9, '2020_11_25_102559_execute_sql_user_table', 5),
(10, '2020_11_26_045123_execute_sql_user_3_table', 6),
(11, '2020_11_27_091816_execute_sql_quries', 7),
(12, '2020_11_27_092544_create_product_table', 8),
(13, '2020_11_27_095830_alter_product_table', 9),
(14, '2020_11_27_103432_direct_sql_queries_1', 10),
(15, '2020_11_27_104359_create_product_variants_table', 11),
(16, '2020_11_27_115555_sql_queries_2', 12),
(17, '2020_11_30_120914_execute_sql_30-11-2020', 13),
(18, '2020_11_30_141735_create_product_images', 13),
(19, '2020_11_30_143707_execute_sql_product_images', 14),
(20, '2020_11_30_102012_create_brands_table', 15),
(21, '2020_12_01_081042_create_product_dislike_table', 15),
(22, '2020_12_01_134143_create_store_table', 16),
(23, '2020_12_01_141206_alter_user_table_01-12-2020', 17),
(24, '2020_12_01_141504_alter_store_table_01-12-2020', 18),
(25, '2020_12_01_141852_create_store_images_table', 19),
(26, '2020_12_02_063106_alter_product_table_02-12-2020', 20),
(27, '2020_12_01_134554_create_promo_code_table', 21),
(28, '2020_12_02_052921_alter_promo_code1', 21),
(29, '2020_12_02_053415_alter_promo_code', 21),
(30, '2020_12_02_075455_remove_data_from_table_02-12-2020', 21),
(31, '2020_12_02_083859_insert_store_data__02-12-2020', 22),
(32, '2020_12_02_094050_alter_product_description', 23),
(33, '2020_12_02_101006_create_store_categories_table', 24),
(34, '2020_12_02_101557_add_data_store_categories_table', 25),
(35, '2020_12_02_125756_alter_store_data_table', 26),
(36, '2020_12_03_053239_alter_store_data_table_2', 27),
(37, '2020_12_03_053523_alter_store_data_table_3', 28),
(38, '2020_12_04_090019_alter_store_data_table_4', 29),
(39, '2020_12_03_081006_create_customer_addresses_table', 30),
(40, '2020_12_04_033634_create_order_master_table', 30),
(41, '2020_12_04_033726_create_ordered_products_table', 30),
(42, '2020_12_04_042409_alter_order_master_table', 30),
(43, '2020_12_04_061516_alter_ordered_products_table1', 30),
(44, '2020_12_04_061752_alter_order_master_table1', 30),
(45, '2020_12_04_065113_alter_ordered_products_table2', 30),
(46, '2020_12_04_105624_alter_order_master1', 31),
(47, '2020_12_08_051656_alter_order_master_table2', 32),
(48, '2020_12_09_081408_alter_customer_addresses_table1', 33),
(49, '2020_12_09_113102_alter_order_master_3', 34),
(50, '2020_12_10_060331_alter_order_master_4', 35),
(51, '2020_12_10_115809_alter_order_master5', 36),
(52, '2020_12_11_091102_alter_order_master6', 37),
(53, '2020_12_11_094250_alter_order_master7', 37),
(54, '2020_12_12_052659_create_recent_search_table', 37),
(55, '2020_12_12_072038_alter_order_master8', 38),
(56, '2020_12_12_091546_alter_order_master9', 38),
(57, '2020_12_14_133913_alter_user_table_14122020', 39),
(58, '2020_12_15_053446_alter_device_details_table_15122020', 40),
(59, '2020_12_16_124619_create_order_allocation', 41),
(60, '2020_12_16_131102_alter_order_allocation', 42),
(61, '2020_12_16_132205_create_driver_details', 43),
(62, '2020_12_16_133754_alter_multiple_tables', 44),
(63, '2020_12_16_053501_alter_users_table_161220201', 45),
(64, '2020_12_16_061559_alter_users_table_161220202', 45),
(65, '2020_12_17_050936_alter_driver_details_17122020', 46),
(66, '2020_12_17_065823_alter_order_products_17122020', 47),
(67, '2020_12_21_104859_alter_product_21122020', 48),
(68, '2020_12_22_052545_alter_users_table_22122020', 48),
(69, '2020_12_22_052614_alter_product_table_22122020', 48),
(70, '2020_12_28_045225_create_cms_pages', 49),
(71, '2020_12_28_050959_create_contact_us_reason_table', 49),
(72, '2020_12_28_072001_create_contact_us_request_table', 49),
(73, '2020_12_31_071732_create_faq_table', 50),
(74, '2021_01_01_084438_create_store_orders', 51),
(75, '2021_01_04_053545_alter_order_allocation_04012021', 51),
(76, '2021_01_04_053943_alter_order_allocation_040120212', 51),
(77, '2021_01_04_060608_alter_site_settings', 51),
(78, '2021_01_04_084715_create_user_notification', 51),
(79, '2021_01_04_100349_alter_device_details', 51),
(80, '2021_01_05_060704_create_decline_reason_table', 52),
(81, '2021_01_05_064409_create_driver_declined_order_table', 52),
(82, '2021_01_05_070911_create_sub_order_master', 52),
(83, '2021_01_05_084512_alter_sub_order_master', 52),
(84, '2021_01_05_090411_alter_sub_order_allocation', 52),
(85, '2021_01_05_091111_alter_order_allocation', 52),
(86, '2021_01_06_090435_alter_order_master', 53),
(87, '2021_01_06_093043_alter_order_master', 53),
(88, '2021_01_06_131512_alter_decline_reason_06012021_table', 53),
(89, '2021_01_08_094725_alter_driver_declined_order1', 54),
(90, '2021_01_08_100234_alter_driver_declined_order2', 54),
(91, '2021_01_08_123023_alter_store_orders1', 54),
(92, '2021_01_08_123218_alter_store_orders2', 54),
(93, '2021_01_12_083634_alter_decline_reason1', 55),
(94, '2021_01_12_095402_create_store_declined_order', 55),
(95, '2021_01_13_084933_alter_sub_order_master13', 56),
(96, '2021_01_13_120019_alter_store_order13', 56),
(97, '2021_01_13_120706_alter_store_order131', 56),
(98, '2021_01_13_123137_alter_sub_order_master1801', 56),
(99, '2021_01_18_103842_create_driver_running_trips', 56),
(100, '2021_01_18_114546_alter_order_allocation181715', 56),
(101, '2021_01_21_060922_create_driver_ratings', 57),
(102, '2021_01_21_062021_create_store_ratings', 57),
(103, '2021_01_21_062252_create_product_ratings', 57),
(104, '2021_01_26_164905_alter_multiple_tables', 58),
(105, '2021_01_26_165325_alter_multiple_tables2', 59),
(106, '2021_01_26_171428_alter_multiple_tables3', 60),
(107, '2021_02_04_152426_alter_users_table0402', 61),
(108, '2021_02_10_175047_alter_notification_table', 62),
(109, '2021_02_13_105810_create_tests_table', 63),
(110, '2021_02_13_120742_create_tests_table', 64);

-- --------------------------------------------------------

--
-- Table structure for table `ordered_products`
--

CREATE TABLE `ordered_products` (
  `iOrderedProductId` int(10) UNSIGNED NOT NULL,
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iProductId` int(10) UNSIGNED NOT NULL,
  `iProductVariantId` int(10) UNSIGNED NOT NULL,
  `fProductVariantsPrice` double(8,2) NOT NULL,
  `fTotalAmount` double(8,2) NOT NULL,
  `iQty` int(11) NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ordered_products`
--

INSERT INTO `ordered_products` (`iOrderedProductId`, `iOrderId`, `iStoreId`, `iProductId`, `iProductVariantId`, `fProductVariantsPrice`, `fTotalAmount`, `iQty`, `iCreatedAt`, `iUpdatedAt`) VALUES
(55, 41, 14, 24, 35, 100.00, 100.00, 1, 1608190043, 1608190043),
(58, 42, 13, 12, 21, 100.00, 100.00, 1, 1608197965, 1608197975),
(59, 43, 14, 26, 39, 100.00, 200.00, 2, 1608199990, 1608199990),
(72, 53, 13, 11, 19, 100.00, 100.00, 1, 1608299581, 1608299581),
(73, 54, 13, 11, 19, 100.00, 200.00, 2, 1608534787, 1608534789),
(74, 55, 13, 13, 23, 100.00, 400.00, 4, 1608548646, 1608548654),
(76, 57, 13, 11, 19, 100.00, 300.00, 3, 1608553725, 1608553725),
(77, 58, 13, 11, 19, 100.00, 100.00, 1, 1609328298, 1609328298),
(80, 61, 14, 24, 35, 100.00, 100.00, 1, 1609333032, 1609333032),
(81, 61, 13, 11, 19, 100.00, 100.00, 1, 1609498862, 1609498862),
(82, 62, 14, 28, 43, 100.00, 100.00, 1, 1609499265, 1609499265),
(84, 64, 13, 12, 21, 100.00, 100.00, 1, 1609500160, 1609500160),
(85, 65, 13, 14, 25, 100.00, 100.00, 1, 1609500242, 1609500242),
(89, 69, 13, 11, 19, 100.00, 100.00, 1, 1609917900, 1609917900),
(96, 73, 14, 24, 35, 100.00, 200.00, 2, 1610081691, 1610081997),
(99, 76, 13, 11, 19, 100.00, 200.00, 2, 1610454782, 1610538670),
(100, 76, 13, 11, 20, 200.00, 400.00, 2, 1610454876, 1610538814),
(102, 76, 14, 26, 39, 100.00, 100.00, 1, 1610527710, 1610527710),
(104, 79, 13, 11, 19, 100.00, 100.00, 1, 1611052959, 1611052992),
(105, 80, 13, 11, 19, 100.00, 100.00, 1, 1611055271, 1611055271),
(106, 80, 51, 36, 54, 700.00, 700.00, 1, 1611304893, 1611304893),
(107, 80, 51, 37, 55, 35.00, 35.00, 1, 1611304895, 1611304895),
(108, 81, 51, 36, 54, 700.00, 700.00, 1, 1611307620, 1611307620),
(109, 81, 51, 37, 55, 35.00, 35.00, 1, 1611307621, 1611307621),
(110, 82, 51, 37, 55, 35.00, 35.00, 1, 1611379015, 1611379015),
(111, 73, 13, 11, 19, 100.00, 100.00, 1, 1611380049, 1611380049),
(112, 83, 13, 11, 19, 100.00, 100.00, 1, 1611382657, 1611382657),
(113, 84, 51, 36, 54, 700.00, 700.00, 1, 1611383266, 1611383266),
(114, 85, 51, 36, 54, 700.00, 1400.00, 2, 1611385766, 1611385769),
(115, 86, 51, 37, 55, 35.00, 140.00, 4, 1611385832, 1611385836),
(116, 87, 51, 37, 55, 35.00, 35.00, 1, 1611385912, 1611385912),
(117, 88, 48, 33, 51, 120.00, 120.00, 1, 1611401790, 1611401790),
(118, 89, 48, 33, 51, 120.00, 240.00, 2, 1611557748, 1611559328),
(119, 90, 51, 37, 55, 35.00, 35.00, 1, 1611558564, 1611558564),
(120, 91, 51, 37, 55, 35.00, 70.00, 2, 1611558651, 1611558652),
(121, 92, 13, 11, 19, 100.00, 100.00, 1, 1611581768, 1611581768),
(122, 93, 13, 11, 19, 100.00, 100.00, 1, 1611642365, 1611642365),
(123, 93, 14, 24, 35, 100.00, 100.00, 1, 1611642601, 1611642601),
(124, 93, 14, 25, 37, 100.00, 100.00, 1, 1611642628, 1611642628),
(125, 93, 13, 18, 31, 100.00, 100.00, 1, 1611642663, 1611642663),
(126, 94, 13, 11, 19, 100.00, 100.00, 1, 1612951069, 1612951069),
(127, 95, 13, 11, 19, 100.00, 100.00, 1, 1612951338, 1612951338),
(128, 96, 13, 11, 19, 100.00, 100.00, 1, 1612951479, 1612951479),
(129, 97, 13, 11, 19, 100.00, 100.00, 1, 1612951551, 1612951557),
(130, 98, 13, 11, 19, 100.00, 100.00, 1, 1612955888, 1612955888);

-- --------------------------------------------------------

--
-- Table structure for table `order_allocation`
--

CREATE TABLE `order_allocation` (
  `iOrderAllocationId` int(10) UNSIGNED NOT NULL,
  `iSubOrderId` int(10) UNSIGNED NOT NULL,
  `iDriverId` int(10) UNSIGNED NOT NULL,
  `tiIsNewOrderPopup` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - pop up not open yet for driver app , 1 - opened ',
  `iCanceledByUserId` int(10) UNSIGNED DEFAULT NULL,
  `tiCancelledBy` tinyint(1) DEFAULT NULL COMMENT '0 - Admin, 1- Driver, 2 - Customer, 3 - Store',
  `tiAllocationStatus` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - Pending, 1 - Accepted, 2 - Rejected, 3 - Cancelled, 4 - Order Ready for pickup , 5 - Start Pickup, 6 - Picked Up , 7 - Start Delivery, 8 - Delivered',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_allocation`
--

INSERT INTO `order_allocation` (`iOrderAllocationId`, `iSubOrderId`, `iDriverId`, `tiIsNewOrderPopup`, `iCanceledByUserId`, `tiCancelledBy`, `tiAllocationStatus`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 3, 118, 0, NULL, NULL, 0, 1609917904, 1609917904),
(2, 3, 121, 0, NULL, NULL, 1, 1609917905, 1609917905),
(3, 3, 120, 0, NULL, NULL, 0, 1609917905, 1609917905),
(4, 3, 119, 0, NULL, NULL, 0, 1609917905, 1609917905),
(17, 8, 121, 0, NULL, NULL, 0, 1611305122, 1611305122),
(18, 8, 120, 0, NULL, NULL, 0, 1611305122, 1611305122),
(19, 8, 119, 0, NULL, NULL, 0, 1611305122, 1611305122),
(20, 8, 118, 0, NULL, NULL, 0, 1611305122, 1611305122),
(21, 9, 119, 0, NULL, NULL, 0, 1611307645, 1611307645),
(22, 9, 118, 0, NULL, NULL, 0, 1611307645, 1611307645),
(23, 9, 145, 0, NULL, NULL, 8, 1611307645, 1611644946),
(24, 9, 121, 0, NULL, NULL, 0, 1611307645, 1611307645),
(25, 9, 120, 0, NULL, NULL, 0, 1611307645, 1611307645),
(26, 10, 148, 0, NULL, NULL, 0, 1611379086, 1611379086),
(27, 10, 145, 0, NULL, NULL, 0, 1611379086, 1611379086),
(28, 10, 121, 0, NULL, NULL, 0, 1611379086, 1611379086),
(29, 10, 120, 0, NULL, NULL, 0, 1611379086, 1611379086),
(30, 10, 119, 0, NULL, NULL, 0, 1611379087, 1611379087),
(31, 10, 118, 0, NULL, NULL, 0, 1611379087, 1611379087),
(32, 14, 148, 0, NULL, NULL, 0, 1611383293, 1611383293),
(33, 14, 145, 0, NULL, NULL, 0, 1611383293, 1611383293),
(34, 14, 121, 0, NULL, NULL, 0, 1611383293, 1611383293),
(35, 14, 120, 0, NULL, NULL, 0, 1611383293, 1611383293),
(36, 14, 119, 0, NULL, NULL, 0, 1611383293, 1611383293),
(37, 14, 118, 0, NULL, NULL, 0, 1611383293, 1611383293),
(38, 14, 116, 0, NULL, NULL, 0, 1611383293, 1611383293),
(39, 15, 119, 0, NULL, NULL, 0, 1611385959, 1611385959),
(40, 15, 118, 0, NULL, NULL, 0, 1611385959, 1611385959),
(41, 15, 116, 0, NULL, NULL, 1, 1611385959, 1611385959),
(42, 15, 148, 0, NULL, NULL, 8, 1611385959, 1611560386),
(43, 15, 145, 0, NULL, NULL, 0, 1611385959, 1611385959),
(44, 15, 121, 0, NULL, NULL, 0, 1611385959, 1611385959),
(45, 15, 120, 0, NULL, NULL, 0, 1611385959, 1611385959),
(46, 19, 119, 0, NULL, NULL, 0, 1611558627, 1611558627),
(47, 19, 118, 0, NULL, NULL, 0, 1611558627, 1611558627),
(48, 19, 116, 0, NULL, NULL, 0, 1611558627, 1611558627),
(49, 19, 148, 0, NULL, NULL, 0, 1611558627, 1611558627),
(50, 19, 145, 0, NULL, NULL, 0, 1611558627, 1611558627),
(51, 19, 121, 0, NULL, NULL, 0, 1611558627, 1611558627),
(52, 19, 120, 0, NULL, NULL, 0, 1611558627, 1611558627),
(53, 20, 148, 0, NULL, NULL, 7, 1611558728, 1611558728),
(54, 20, 145, 0, NULL, NULL, 0, 1611558728, 1611558728),
(55, 20, 121, 0, NULL, NULL, 0, 1611558728, 1611558728),
(56, 20, 120, 0, NULL, NULL, 0, 1611558728, 1611558728),
(57, 20, 119, 0, NULL, NULL, 0, 1611558728, 1611558728),
(58, 20, 118, 0, NULL, NULL, 0, 1611558728, 1611558728),
(59, 20, 116, 0, NULL, NULL, 0, 1611558728, 1611558728),
(60, 12, 118, 0, NULL, NULL, 0, 1611642491, 1611642491),
(61, 12, 116, 0, NULL, NULL, 0, 1611642491, 1611642491),
(62, 12, 148, 0, NULL, NULL, 0, 1611642491, 1611642491),
(63, 12, 145, 0, NULL, NULL, 0, 1611642491, 1611642491),
(64, 12, 121, 0, NULL, NULL, 0, 1611642491, 1611642491),
(65, 12, 120, 0, NULL, NULL, 0, 1611642491, 1611642491),
(66, 12, 119, 0, NULL, NULL, 0, 1611642491, 1611642491),
(67, 22, 148, 0, NULL, NULL, 0, 1611642960, 1611642960),
(68, 22, 145, 0, NULL, NULL, 0, 1611642960, 1611642960),
(69, 22, 121, 0, NULL, NULL, 0, 1611642960, 1611642960),
(70, 22, 120, 0, NULL, NULL, 0, 1611642960, 1611642960),
(71, 22, 119, 0, NULL, NULL, 0, 1611642960, 1611642960),
(72, 22, 118, 0, NULL, NULL, 0, 1611642960, 1611642960),
(73, 22, 116, 0, NULL, NULL, 0, 1611642960, 1611642960),
(74, 23, 119, 0, NULL, NULL, 0, 1611642992, 1611642992),
(75, 23, 118, 0, NULL, NULL, 0, 1611642992, 1611642992),
(76, 23, 116, 0, NULL, NULL, 0, 1611642992, 1611642992),
(77, 23, 148, 0, NULL, NULL, 0, 1611642992, 1611642992),
(78, 23, 145, 0, NULL, NULL, 8, 1611642992, 1611645362),
(79, 23, 121, 0, NULL, NULL, 0, 1611642992, 1611642992),
(80, 23, 120, 0, NULL, NULL, 0, 1611642992, 1611642992),
(81, 7, 114, 0, NULL, NULL, 0, 1611905107, 1611905107),
(82, 7, 148, 0, NULL, NULL, 0, 1611905107, 1611905107),
(83, 7, 145, 0, NULL, NULL, 0, 1611905107, 1611905107),
(84, 7, 121, 0, NULL, NULL, 0, 1611905107, 1611905107),
(85, 7, 120, 0, NULL, NULL, 0, 1611905107, 1611905107),
(86, 7, 119, 0, NULL, NULL, 0, 1611905107, 1611905107),
(87, 7, 118, 0, NULL, NULL, 0, 1611905107, 1611905107),
(88, 7, 116, 0, NULL, NULL, 0, 1611905107, 1611905107),
(89, 11, 118, 0, NULL, NULL, 0, 1612161810, 1612161810),
(90, 11, 116, 0, NULL, NULL, 0, 1612161810, 1612161810),
(91, 11, 114, 0, NULL, NULL, 0, 1612161810, 1612161810),
(92, 11, 148, 0, NULL, NULL, 0, 1612161810, 1612161810),
(93, 11, 145, 0, NULL, NULL, 0, 1612161810, 1612161810),
(94, 11, 121, 0, NULL, NULL, 0, 1612161810, 1612161810),
(95, 11, 120, 0, NULL, NULL, 0, 1612161810, 1612161810),
(96, 11, 119, 0, NULL, NULL, 0, 1612161810, 1612161810),
(97, 13, 119, 0, NULL, NULL, 0, 1612441911, 1612441911),
(98, 13, 118, 0, NULL, NULL, 0, 1612441911, 1612441911),
(99, 13, 116, 0, NULL, NULL, 0, 1612441911, 1612441911),
(100, 13, 114, 0, NULL, NULL, 0, 1612441911, 1612441911),
(101, 13, 148, 0, NULL, NULL, 0, 1612441911, 1612441911),
(102, 13, 145, 0, NULL, NULL, 3, 1612441911, 1612441911),
(103, 13, 121, 0, NULL, NULL, 0, 1612441911, 1612441911),
(104, 13, 120, 0, NULL, NULL, 0, 1612441911, 1612441911),
(105, 24, 119, 0, NULL, NULL, 0, 1612951139, 1612951139),
(106, 24, 118, 0, NULL, NULL, 0, 1612951139, 1612951139),
(107, 24, 116, 0, NULL, NULL, 0, 1612951139, 1612951139),
(108, 24, 114, 0, NULL, NULL, 0, 1612951139, 1612951139),
(109, 24, 148, 0, NULL, NULL, 0, 1612951139, 1612951139),
(110, 24, 145, 0, NULL, NULL, 0, 1612951139, 1612951139),
(111, 24, 121, 0, NULL, NULL, 0, 1612951139, 1612951139),
(112, 24, 120, 0, NULL, NULL, 0, 1612951139, 1612951139),
(113, 25, 148, 0, NULL, NULL, 0, 1612951378, 1612951378),
(114, 25, 145, 0, NULL, NULL, 0, 1612951379, 1612951379),
(115, 25, 121, 0, NULL, NULL, 0, 1612951379, 1612951379),
(116, 25, 120, 0, NULL, NULL, 0, 1612951379, 1612951379),
(117, 25, 119, 0, NULL, NULL, 0, 1612951379, 1612951379),
(118, 25, 118, 0, NULL, NULL, 0, 1612951379, 1612951379),
(119, 25, 116, 0, NULL, NULL, 0, 1612951379, 1612951379),
(120, 25, 114, 0, NULL, NULL, 0, 1612951379, 1612951379),
(121, 26, 121, 0, NULL, NULL, 0, 1612951507, 1612951507),
(122, 26, 120, 0, NULL, NULL, 0, 1612951507, 1612951507),
(123, 26, 119, 0, NULL, NULL, 0, 1612951507, 1612951507),
(124, 26, 118, 0, NULL, NULL, 0, 1612951508, 1612951508),
(125, 26, 116, 0, NULL, NULL, 0, 1612951508, 1612951508),
(126, 26, 114, 0, NULL, NULL, 0, 1612951508, 1612951508),
(127, 26, 148, 0, NULL, NULL, 0, 1612951508, 1612951508),
(128, 26, 145, 0, NULL, NULL, 0, 1612951508, 1612951508),
(129, 27, 114, 0, NULL, NULL, 0, 1612951594, 1612951594),
(130, 27, 148, 0, NULL, NULL, 0, 1612951597, 1612951597),
(131, 27, 145, 0, NULL, NULL, 0, 1612951597, 1612951597),
(132, 27, 121, 0, NULL, NULL, 0, 1612951597, 1612951597),
(133, 27, 120, 0, NULL, NULL, 0, 1612951597, 1612951597),
(134, 27, 119, 0, NULL, NULL, 0, 1612951597, 1612951597),
(135, 27, 118, 0, NULL, NULL, 0, 1612951598, 1612951598),
(136, 27, 116, 0, NULL, NULL, 0, 1612951598, 1612951598);

-- --------------------------------------------------------

--
-- Table structure for table `order_master`
--

CREATE TABLE `order_master` (
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `vOrderId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `iPromoCodeId` int(10) UNSIGNED DEFAULT NULL,
  `tiDeliveryType` tinyint(4) DEFAULT NULL COMMENT '1 = ASAP\n            -  2 = Scheduled',
  `dDeliverDate` date DEFAULT NULL,
  `tDeliveryTime` time DEFAULT NULL,
  `tiAddressType` tinyint(4) NOT NULL COMMENT '0 - Home, 1 - Office, 2-Others',
  `vDeliveredName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vDeliveredISDCode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vDeliveredMobileNumber` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vDeliveryAddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fDeliveryLatitude` double(10,8) NOT NULL,
  `fDeliveryLongitude` double(10,8) NOT NULL,
  `dPlaceOrderDate` datetime DEFAULT NULL,
  `vOrderInstruction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fItemTotalAmount` double(10,2) NOT NULL DEFAULT 0.00,
  `fDiscountAmount` double(10,2) NOT NULL DEFAULT 0.00,
  `fTax_charges` double(10,2) NOT NULL DEFAULT 0.00,
  `fDeliverCharges` double(10,2) NOT NULL DEFAULT 0.00,
  `fTotalAmount` double(10,2) NOT NULL DEFAULT 0.00,
  `fTotalPayableAmount` double(10,2) NOT NULL DEFAULT 0.00,
  `tiOrderStatus` tinyint(4) NOT NULL DEFAULT 0 COMMENT ' 0 - Cart, 1 - Placed, 2 - Delivered, 3 - Cancelled',
  `vPaymentId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiPaymentType` tinyint(4) DEFAULT NULL COMMENT '1-Apple Pay, 2-Google Pay, 3-Choward Wallet, 4-Card',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_master`
--

INSERT INTO `order_master` (`iOrderId`, `vOrderId`, `iUserId`, `iPromoCodeId`, `tiDeliveryType`, `dDeliverDate`, `tDeliveryTime`, `tiAddressType`, `vDeliveredName`, `vDeliveredISDCode`, `vDeliveredMobileNumber`, `vDeliveryAddress`, `fDeliveryLatitude`, `fDeliveryLongitude`, `dPlaceOrderDate`, `vOrderInstruction`, `fItemTotalAmount`, `fDiscountAmount`, `fTax_charges`, `fDeliverCharges`, `fTotalAmount`, `fTotalPayableAmount`, `tiOrderStatus`, `vPaymentId`, `tiPaymentType`, `iCreatedAt`, `iUpdatedAt`) VALUES
(41, 'CHO2051708222562808', 65, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 'F-1, Micheal St. LA', 23.03361590, 72.52211090, NULL, NULL, 300.00, 0.00, 20.00, 10.00, 330.00, 330.00, 1, 'TESTKKDSADSDSD', 1, 1608189998, 1608190113),
(42, 'CHO72925544711988', 56, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', 0.00000000, 0.00000000, NULL, NULL, 300.00, 0.00, 20.00, 10.00, 330.00, 330.00, 0, NULL, NULL, 1608197323, 1608197975),
(43, 'CHO1194655820314003', 72, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, 'F-1, Micheal St. LA', 23.03361590, 72.52211090, NULL, 'AASAAAAS', 200.00, 0.00, 20.00, 10.00, 230.00, 230.00, 1, 'adadadada', 2, 1608199990, 1608200155),
(53, 'CHO1945207966903394', 68, NULL, 2, '0000-00-00', '07:23:00', 0, NULL, NULL, NULL, '', 0.00000000, 0.00000000, NULL, NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 0, NULL, NULL, 1608299581, 1608299602),
(54, 'CHO413607029207352', 105, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '257, Test, test, test 2935323', 0.00000000, 0.00000000, NULL, NULL, 200.00, 0.00, 20.00, 10.00, 230.00, 230.00, 1, 'dummypaymentId', 0, 1608534787, 1608538539),
(55, 'CHO1386066441231477', 105, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 0.00000000, 0.00000000, NULL, NULL, 400.00, 0.00, 20.00, 10.00, 430.00, 430.00, 1, 'dummypaymentId', 0, 1608548646, 1608548665),
(57, 'CHO94477328757863', 48, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, 'Space-O Technologies (Canada), County Court Boulevard, Brampton, ON, Canada', 0.00000000, 0.00000000, NULL, NULL, 300.00, 0.00, 20.00, 10.00, 330.00, 130.00, 1, NULL, NULL, 1608553725, 1609389683),
(58, 'CHO1744298563187108', 48, NULL, 2, '0000-00-00', '05:08:00', 0, NULL, NULL, NULL, 'Space-O Technologies (Canada), County Court Boulevard, Brampton, ON, Canada', 0.00000000, 0.00000000, NULL, NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1609328297, 1609389683),
(61, 'CHO1428534581504577', 107, NULL, 2, '0000-00-00', '04:33:00', 0, NULL, NULL, NULL, 'Morbi, Gujarat, India', 0.00000000, 0.00000000, NULL, NULL, 200.00, 0.00, 20.00, 10.00, 230.00, 130.00, 1, NULL, NULL, 1609333032, 1609500196),
(62, 'CHO1135082899454937', 107, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, 'Morbi, Gujarat, India', 0.00000000, 0.00000000, NULL, NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1609499265, 1609500196),
(64, 'CHO1568395888492986', 107, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, 'Morbi, Gujarat, India', 0.00000000, 0.00000000, NULL, NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1609500160, 1609500211),
(65, 'CHO1894449490826749', 107, NULL, 2, '0000-00-00', '04:54:00', 0, NULL, NULL, NULL, 'Morbi, Gujarat, India', 0.00000000, 0.00000000, NULL, NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1609500242, 1609500256),
(69, 'CHO421693901859978', 122, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 0.00000000, 0.00000000, '2021-01-06 07:25:04', NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1609917900, 1609917904),
(73, 'CHO2013809416894852', 121, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 0.00000000, 0.00000000, '2021-01-23 11:04:57', NULL, 300.00, 0.00, 20.00, 10.00, 330.00, 330.00, 1, NULL, NULL, 1610081683, 1611380097),
(76, 'CHO339606553154599', 145, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', 0.00000000, 0.00000000, NULL, NULL, 700.00, 0.00, 20.00, 10.00, 730.00, 730.00, 0, NULL, NULL, 1610454782, 1611306542),
(79, 'CHO1690730216617955', 49, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', 0.00000000, 0.00000000, NULL, NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 0, NULL, NULL, 1611052959, 1611052992),
(80, 'CHO1331191545691623', 48, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, 'Space-O Technologies (Canada), County Court Boulevard, Brampton, ON, Canada', 0.00000000, 0.00000000, '2021-01-22 14:11:45', NULL, 835.00, 0.00, 20.00, 10.00, 865.00, 865.00, 1, NULL, NULL, 1611055271, 1611304905),
(81, 'CHO1055615457147640', 48, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, 'Space-O Technologies (Canada), County Court Boulevard, Brampton, ON, Canada', 0.00000000, 0.00000000, '2021-01-22 14:57:10', NULL, 735.00, 0.00, 20.00, 10.00, 765.00, 765.00, 2, NULL, NULL, 1611307620, 1611644946),
(82, 'CHO1788456400230661', 147, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '', 0.00000000, 0.00000000, '2021-01-23 10:47:04', NULL, 35.00, 0.00, 20.00, 10.00, 65.00, 65.00, 1, NULL, NULL, 1611379015, 1611379024),
(83, 'CHO513406431903002', 121, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 0.00000000, 0.00000000, '2021-01-23 11:47:43', NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 3, NULL, NULL, 1611382657, 1612442308),
(84, 'CHO1626299241596692', 121, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 0.00000000, 0.00000000, '2021-01-23 11:57:53', NULL, 700.00, 0.00, 20.00, 10.00, 730.00, 730.00, 1, NULL, NULL, 1611383266, 1611383273),
(85, 'CHO1886123817411119', 121, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 0.00000000, 0.00000000, '2021-01-23 12:39:33', NULL, 1400.00, 0.00, 20.00, 10.00, 1430.00, 1430.00, 2, NULL, NULL, 1611385766, 1611560386),
(86, 'CHO646789040884182', 121, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 0.00000000, 0.00000000, '2021-01-23 12:40:40', NULL, 140.00, 0.00, 20.00, 10.00, 170.00, 170.00, 1, NULL, NULL, 1611385832, 1611385840),
(87, 'CHO1660791977777871', 121, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '1005, 10th Floor, Abhishree Adroit Mansi Circle, beside Gwalia Sweet, Vastrapur, Ahmedabad, Gujarat 380015, India', 0.00000000, 0.00000000, '2021-01-23 12:41:57', NULL, 35.00, 0.00, 20.00, 10.00, 65.00, 65.00, 1, NULL, NULL, 1611385912, 1611385917),
(88, 'CHO160150011242827', 48, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, 'Space-O Technologies (Canada), County Court Boulevard, Brampton, ON, Canada', 0.00000000, 0.00000000, '2021-01-23 17:06:49', NULL, 120.00, 0.00, 20.00, 10.00, 150.00, 150.00, 3, NULL, NULL, 1611401790, 1611402530),
(89, 'CHO1481938897709417', 148, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', 0.00000000, 0.00000000, NULL, NULL, 240.00, 0.00, 20.00, 10.00, 270.00, 270.00, 0, NULL, NULL, 1611557748, 1611560066),
(90, 'CHO476878271903264', 151, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, '', 0.00000000, 0.00000000, '2021-01-25 12:39:34', NULL, 35.00, 0.00, 20.00, 10.00, 65.00, 65.00, 3, NULL, NULL, 1611558564, 1611558633),
(91, 'CHO972878821648546', 151, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, 'Morbi, Gujarat, India', 0.00000000, 0.00000000, '2021-01-25 12:41:54', NULL, 70.00, 0.00, 20.00, 10.00, 100.00, 100.00, 1, NULL, NULL, 1611558651, 1611558714),
(92, 'CHO600312698612495', 151, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, 'Morbi, Gujarat, India', 0.00000000, 0.00000000, '2021-01-25 19:06:18', NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 3, NULL, NULL, 1611581768, 1611581785),
(93, 'CHO871875944826274', 151, NULL, 1, NULL, NULL, 0, 'Sagar Bh', '+966', '15171517', 'Morbi, Gujarat, India', 0.00000000, 0.00000000, '2021-01-26 12:04:36', 'Add \"Happy annivarsary\"', 400.00, 0.00, 20.00, 10.00, 430.00, 430.00, 1, NULL, NULL, 1611642365, 1611642876),
(94, 'CHO915540720343523', 151, NULL, 1, NULL, NULL, 0, 'Sagar Bh', '+966', '15171517', 'Morbi, Gujarat, India', 0.00000000, 0.00000000, '2021-02-10 15:28:45', NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1612951069, 1612951125),
(95, 'CHO1397795296998762', 151, NULL, 1, NULL, NULL, 0, 'Sagar Bh', '+966', '15171517', 'Morbi, Gujarat, India', 0.00000000, 0.00000000, '2021-02-10 15:32:47', NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1612951338, 1612951367),
(96, 'CHO1491772084109985', 151, NULL, 1, NULL, NULL, 0, 'Sagar Bh', '+966', '15171517', 'Morbi, Gujarat, India', 0.00000000, 0.00000000, '2021-02-10 15:35:00', NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1612951479, 1612951500),
(97, 'CHO1106999031872698', 151, NULL, 1, NULL, NULL, 0, 'Sagar Bh', '+966', '15171517', 'Morbi, Gujarat, India', 0.00000000, 0.00000000, '2021-02-10 15:36:21', NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1612951551, 1612951581),
(98, 'CHO1850771305690724', 151, NULL, 1, NULL, NULL, 0, 'Sagar Bh', '+966', '15171517', 'Morbi, Gujarat, India', 0.00000000, 0.00000000, '2021-02-10 17:08:17', NULL, 100.00, 0.00, 20.00, 10.00, 130.00, 130.00, 1, NULL, NULL, 1612955888, 1612957097);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iCategoryId` int(10) UNSIGNED NOT NULL,
  `vProductName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vBrandName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `txProductDescription` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vOffers` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vProductImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiIsDeleted` tinyint(4) NOT NULL COMMENT '1 = Yes, 0 = No',
  `tiAdminApproved` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1- Yes, 0-No',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `iStoreId`, `iCategoryId`, `vProductName`, `vBrandName`, `txProductDescription`, `vOffers`, `vProductImage`, `tiIsDeleted`, `tiAdminApproved`, `iCreatedAt`, `iUpdatedAt`) VALUES
(11, 13, 3, 'Truffle Cake 1', 'Havmor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', NULL, 'cake1.png', 0, 0, 1606454762, 1606454762),
(12, 13, 3, 'Dutch Truffle Cake', 'Havmor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', NULL, 'cake2.png', 0, 0, 1606454762, 1606454762),
(13, 13, 3, 'Truffle Cake', 'Havmor', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', NULL, 'cake3.png', 0, 0, 1606454762, 1606454762),
(14, 13, 4, 'Dark Chocolate', 'Amul', 'Dark rich Chocolate', NULL, NULL, 0, 0, NULL, NULL),
(15, 13, 4, 'Milk Chocolate', 'Nestle', 'Milk Protien Chocolate', NULL, 'choco2.png', 0, 0, 173036, 173036),
(16, 13, 4, 'Belgian Chocolate', 'Cadbury', 'famous Belgian Chocolate', NULL, 'choco3.png', 0, 0, NULL, NULL),
(18, 13, 5, 'Kid Birthday Card', 'Archies', 'Fancy Kid birthday card', NULL, 'card1.png', 0, 0, NULL, NULL),
(19, 13, 5, 'Blue Fancy Card', 'Archies', 'Blue Customized Birthdsaya card', NULL, 'card2.png', 0, 0, NULL, NULL),
(20, 13, 5, '18th Birthday Card', 'Archies', 'Specially design for 18th Birthday', NULL, 'card3.png', 0, 0, NULL, NULL),
(21, 13, 6, 'Lilies', 'Buds_N_Bloom', 'Lilies for Decore', NULL, 'flower1.png', 0, 0, NULL, NULL),
(22, 13, 6, 'Cherry Blossom', 'Buds_N_Bloom', 'Perfect for every occasion', NULL, 'flower2.png', 0, 0, NULL, NULL),
(23, 13, 6, 'Tulip', 'Buds_N_Bloom', 'All time EverGreen Tulip', NULL, 'flower3.png', 0, 0, NULL, NULL),
(24, 14, 3, 'Black Forest', 'Dangee Dums', 'Cake ', NULL, 'cake12.png', 0, 0, NULL, NULL),
(25, 14, 3, 'Fruit Cake', 'Dangee Dums', 'Fruity Juicy', NULL, 'cake13.png', 0, 0, NULL, NULL),
(26, 14, 4, 'Silk', 'Cadbury', 'Rich Silk', NULL, 'choco12.png', 0, 0, NULL, NULL),
(27, 14, 4, 'Kitkat', 'Nestle', 'Have a break', NULL, 'choco13.png', 0, 0, NULL, NULL),
(28, 14, 5, 'Anniversary Card', 'CardsWorld', 'Anniversary Special', NULL, 'card12.png', 0, 0, NULL, NULL),
(29, 14, 5, 'Marriage Card', 'Marriages', 'Marriage Card', NULL, 'card13.png', 0, 0, NULL, NULL),
(30, 14, 6, 'Pink Rose', 'Buds & Petals', 'Pink Rose', NULL, 'flower12.png', 0, 0, NULL, NULL),
(31, 14, 6, 'Red Rose', 'Buds & Petals', 'Valentine\'s Special', NULL, 'flower13.png', 0, 0, NULL, NULL),
(32, 48, 5, 'Beverly Hills', 'Beverly Hills', 'Rmkmqb lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ggpwm', '1609326419-353879-551764.jpeg', 0, 0, 2020, 2020),
(33, 48, 4, 'Dark Fantsy', 'Dangee Dums', 'Dark Rich chocolate', '10 % off', '1609326575-487284-793313.jpeg', 0, 0, 2020, 2020),
(34, 49, 6, 'Pink Rose Boquet', 'Rose World', 'adaddad', '10% off', '1609326878-221705-896711.jpeg', 0, 0, 2020, 2020),
(35, 53, 3, 'test1', 'test1', 'test', NULL, NULL, 0, 0, 2021, 2021),
(36, 51, 3, 'Black Forest Heart Shape Cake', 'Monginis', 'Black Forest is one of the most-sought-after cakes in today\'s era! It\'s not just a cake, but a way to express ones care, emotions, feelings and love to the most important people in our lives.\r\n\r\nKnown to be rich in ingredients, this typical heart shape Black Forest cake consist a fresh mushy red or chocolate sponge at the base smothered with majestic layers of fresh whipped cream and cherries. The decoration also involves bountiful of chocolate curls and fleck of whipped cream which is topped with stunning red cherries on it!\r\n\r\nIdeal gift for any sort of occasion or event! And if your loved one is angry or upset you&rsquo;re just a click away to buy this wonderful cake and make him/her special!', NULL, '1611303845-322538-630157.jpg', 0, 0, 2021, 2021),
(37, 51, 3, 'Shimmer Cake', 'Monginis', 'Ideal gift for any sort of occasion or event! And if your loved one is angry or upset you’re just a click away to buy this wonderful cake and make him/her special!', NULL, '1611303987-802042-654415.jpg', 0, 0, 2021, 2021);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `iProductImageId` int(10) UNSIGNED NOT NULL,
  `iProductId` int(10) UNSIGNED NOT NULL,
  `vImageName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`iProductImageId`, `iProductId`, `vImageName`, `iCreatedAt`, `iUpdatedAt`) VALUES
(9, 11, 'cake1.png', 1606454762, 1606454762),
(10, 11, 'cake2.png', 1606454762, 1606454762),
(11, 12, 'cake1.png', 1606454762, 1606454762),
(12, 12, 'cake2.png', 1606454762, 1606454762),
(13, 13, 'cake1.png', 1606454762, 1606454762),
(14, 13, 'cake2.png', 1606454762, 1606454762);

-- --------------------------------------------------------

--
-- Table structure for table `product_liked`
--

CREATE TABLE `product_liked` (
  `id` int(10) UNSIGNED NOT NULL,
  `iProductId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `tiLikedProduct` tinyint(4) NOT NULL COMMENT '0 - No, 1 - Yes',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_liked`
--

INSERT INTO `product_liked` (`id`, `iProductId`, `iUserId`, `tiLikedProduct`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 11, 48, 1, 2020, 2020),
(2, 12, 48, 0, 2020, 2020),
(3, 13, 48, 1, 2020, 2020),
(4, 13, 50, 0, 2020, 2020),
(5, 12, 50, 0, 2020, 2020),
(6, 11, 50, 1, 2020, 2020),
(8, 11, 49, 0, 2020, 2021),
(9, 18, 68, 1, 2020, 2020),
(10, 14, 68, 0, 2020, 2020),
(11, 11, 68, 1, 2020, 2020),
(12, 25, 48, 1, 2020, 2020),
(13, 11, 107, 1, 2020, 2020),
(28, 12, 49, 0, 2021, 2021),
(29, 13, 49, 0, 2021, 2021),
(30, 24, 49, 0, 2021, 2021);

-- --------------------------------------------------------

--
-- Table structure for table `product_ratings`
--

CREATE TABLE `product_ratings` (
  `iProductRatingId` int(10) UNSIGNED NOT NULL,
  `iProductId` int(10) UNSIGNED NOT NULL,
  `iSubOrderId` int(10) UNSIGNED NOT NULL,
  `iReviewByUserId` int(10) UNSIGNED NOT NULL,
  `fRating` double(2,1) NOT NULL DEFAULT 0.0,
  `vReview` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_ratings`
--

INSERT INTO `product_ratings` (`iProductRatingId`, `iProductId`, `iSubOrderId`, `iReviewByUserId`, `fRating`, `vReview`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 24, 23, 151, 3.5, 'Dummy text Dummy text Dummy text Dummy textDummy textDummy text', 1611654059, 1611654059),
(2, 25, 23, 151, 4.5, 'Dummy text Dummy text Dummy text Dummy textDummy textDummy text', 1611654059, 1611654059);

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` int(10) UNSIGNED NOT NULL,
  `iProductId` int(10) UNSIGNED NOT NULL,
  `fProductVariantsPrice` double(10,2) NOT NULL DEFAULT 0.00,
  `vProductVariantsQuantity` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `iProductId`, `fProductVariantsPrice`, `vProductVariantsQuantity`, `iCreatedAt`, `iUpdatedAt`) VALUES
(19, 11, 100.00, '500 gm', 1606454762, 1606454762),
(20, 11, 200.00, '1 KG', 1606454762, 1606454762),
(21, 12, 100.00, '500 gm', NULL, NULL),
(22, 12, 200.00, '1 KG', NULL, NULL),
(23, 13, 100.00, '500 gm', NULL, NULL),
(24, 13, 200.00, '1 KG', NULL, NULL),
(25, 14, 100.00, '100 gm', NULL, NULL),
(26, 14, 200.00, '200 gm', NULL, NULL),
(27, 15, 100.00, '100 gm', NULL, NULL),
(28, 15, 200.00, '200 gm', NULL, NULL),
(29, 16, 100.00, '100 gm', NULL, NULL),
(30, 16, 200.00, '200 gm', NULL, NULL),
(31, 18, 100.00, '100 gm', NULL, NULL),
(32, 19, 200.00, '100 gm', NULL, NULL),
(33, 20, 300.00, '100 gm', NULL, NULL),
(34, 21, 200.00, '100 gm', NULL, NULL),
(35, 24, 100.00, '500 gm', NULL, NULL),
(36, 24, 200.00, '1 KG', NULL, NULL),
(37, 25, 100.00, '500 gm', NULL, NULL),
(38, 25, 200.00, '1 KG', NULL, NULL),
(39, 26, 100.00, '100 gm', NULL, NULL),
(40, 26, 200.00, '200 gm', NULL, NULL),
(41, 27, 100.00, '100 gm', NULL, NULL),
(42, 27, 200.00, '200 gm', NULL, NULL),
(43, 28, 100.00, '1', NULL, NULL),
(44, 29, 100.00, '1', NULL, NULL),
(46, 30, 50.00, '1', NULL, NULL),
(47, 31, 50.00, '1', NULL, NULL),
(48, 22, 100.00, '100 gm', NULL, NULL),
(49, 23, 100.00, '100 gm', NULL, NULL),
(50, 32, 100.00, 'Xwhje', 1609326427, 1609326427),
(51, 33, 120.00, '250 gm', 1609326575, 1609326575),
(52, 34, 200.00, '20 Roses', 1609326879, 1609326879),
(53, 35, 0.00, '500 gms', 1610105806, 1610105806),
(54, 36, 700.00, '1 Kg', 1611315819, 1611315819),
(55, 37, 35.00, '1 Kg', 1611315779, 1611315779);

-- --------------------------------------------------------

--
-- Table structure for table `promo_code`
--

CREATE TABLE `promo_code` (
  `iPromoCodeId` int(10) UNSIGNED NOT NULL,
  `vPromoCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fAmountOrPercent` double(8,2) NOT NULL DEFAULT 0.00,
  `vPromoDesc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dStartDate` date NOT NULL,
  `dEndDate` date NOT NULL,
  `fMinOrderAmount` int(11) NOT NULL,
  `iMaxUsageLimit` int(11) NOT NULL COMMENT 'if null than no limit',
  `tiPromoType` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1 - Fixed Amount , 2 - Percentage On Total Amount',
  `tiIsDeleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Yes, 0 - No,',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL,
  `enStatus` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promo_code`
--

INSERT INTO `promo_code` (`iPromoCodeId`, `vPromoCode`, `fAmountOrPercent`, `vPromoDesc`, `dStartDate`, `dEndDate`, `fMinOrderAmount`, `iMaxUsageLimit`, `tiPromoType`, `tiIsDeleted`, `iCreatedAt`, `iUpdatedAt`, `enStatus`) VALUES
(1, 'CHOWARD20', 20.00, 'Get 20SAR Flat Off', '2020-12-01', '2021-01-31', 200, 10, 1, 0, 1607573621, 1607573691, 'Active'),
(2, 'CHOWARD50', 50.00, 'Get 50SAR Flat Off', '2020-12-01', '2020-12-31', 500, 10, 1, 0, 1607573677, 1607573677, 'Active'),
(3, 'FESTIVAL BONANAZA', 25.00, 'Get Flat 25SAR off', '2020-12-01', '2021-03-31', 100, 10, 1, 0, 1607573769, 1607573769, 'Active'),
(4, 'CHOWARD30', 30.00, 'Flat 30SAR', '2020-12-01', '2021-03-01', 200, 10, 1, 0, 1607573819, 1607573819, 'Active'),
(5, 'CHOWARD60', 60.00, 'Get 60SAR off', '2020-12-01', '2020-12-31', 500, 10, 1, 0, 1607573871, 1607573871, 'Active'),
(6, 'OFFER DAY', 40.00, 'GET 40SAR off', '2020-11-01', '2020-12-02', 250, 1, 1, 0, 1607573926, 1607573926, 'Active'),
(7, 'CHOWARD10', 10.00, '10SAR Discount', '2020-12-01', '2020-12-24', 100, 10, 1, 0, 1607573971, 1607573971, 'Active'),
(8, 'X-MAS25%', 25.00, 'GET upto 25% off on Order Above 500SAR', '2020-12-24', '2020-12-31', 500, 10, 2, 0, 1607574059, 1607574059, 'Active'),
(9, 'NEWYEAR2021', 21.00, 'Get 21% off in 2021, ORDER ABOVE 300 only', '2020-12-31', '2020-12-05', 300, 10, 2, 0, 1607574130, 1607574130, 'Active'),
(10, 'CHOWARD40%', 40.00, 'Get 40% off on order Above 1000SAR', '2020-12-01', '2021-01-08', 1000, 1, 2, 0, 1607574190, 1607574360, 'Active'),
(11, 'CHOWARD70', 70.00, '70SAR flat off', '2020-12-31', '2021-01-02', 500, 1, 1, 0, 1607574241, 1607574241, 'Active'),
(12, 'FASTFOOD25', 25.00, 'Get 25% on FASTFOOD', '2020-12-30', '2021-01-01', 250, 1, 2, 0, 1607574318, 1607574318, 'Active'),
(13, 'FFZZ75', 75.00, 'GET 75SAR off on 750 Order', '2020-12-31', '2021-01-02', 750, 1, 1, 0, 1607574422, 1607574422, 'Active'),
(14, 'XMAS2020', 20.00, '20% off on All Orders', '2020-12-25', '2020-12-26', 2000, 0, 2, 0, 1607574488, 1607574488, 'Active'),
(15, 'NEW DISCOUNT', 50.00, '50SAR OFF', '2020-12-01', '2020-12-31', 500, 1, 1, 0, 1607574537, 1607574537, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `recent_search`
--

CREATE TABLE `recent_search` (
  `id` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vSearch` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recent_search`
--

INSERT INTO `recent_search` (`id`, `iUserId`, `vSearch`, `iCreatedAt`, `iUpdatedAt`) VALUES
(27, 48, 'dutch', 1607925933, 1607926030),
(28, 48, 'truffle', 1607926010, 1607926033),
(29, 50, 'flower', 1607928980, 1607931256),
(30, 50, 'cake', 1607929060, 1607931283),
(31, 56, 'flower', 1608030819, 1608030836),
(32, 56, 'cake', 1608116784, 1608116784),
(33, 107, 'cake', 1609329985, 1609502886),
(34, 49, 'cake', 1609506082, 1611059801),
(35, 49, 'flower', 1609506102, 1611059807);

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `iSettingsId` bigint(20) UNSIGNED NOT NULL,
  `vSettingTitle` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `vSettingKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vSettingValue` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `tiIsActive` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `tiIsDeleted` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `iCreatedAt` int(10) UNSIGNED NOT NULL,
  `iUpdatedAt` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`iSettingsId`, `vSettingTitle`, `vSettingKey`, `vSettingValue`, `tiIsActive`, `tiIsDeleted`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 'Admin Email', 'ADMIN_EMAIL', 'rushabhm.spaceo@gmail.com', 1, 0, 1604926192, 1604926192),
(2, 'Developer Email', 'DEVELOPER_EMAIL', 'rushabhm.spaceo@gmail.com', 1, 0, 1604926192, 1604926192),
(3, 'Default Pagination', 'PAGINATION_SIZE', '10', 1, 0, 1604926192, 1604926192),
(4, 'Default Language', 'DEFAULT_LANG', 'en', 1, 0, 1604926192, 1604926192),
(5, 'Facebook URL', 'FB_URL', 'http://www.facebook.com', 1, 0, 1604926192, 1604926192),
(6, 'Instagram URL', 'INSTA_URL', 'http://www.instagram.com', 1, 0, 1604926192, 1604926192),
(7, 'Google Plus', 'GPLUS_URL', 'http://www.google.com/plus', 1, 0, 1604926192, 1604926192),
(8, 'Contact Email', 'CONTACT_EMAIL', 'support@basecode.com', 1, 0, 1604926192, 1604926192),
(9, 'Contact Phone', 'CONTACT_PHONE', '+919228723322', 1, 0, 1604926192, 1604926192),
(10, 'Contact Address', 'CONTACT_ADDR', 'Default Address', 1, 0, 1604926192, 1604926192),
(11, 'Multi Language Support', 'IS_MULTILANG', 'No', 1, 0, 1604926192, 1604926192),
(12, 'Driver Order Recieve Distance', 'DRIVER_ORDER_RECIEVE_DISTANCE', '10', 1, 0, 1604926192, 1604926192);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vStoreName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vStoreLocation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vStoreAddressLine2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txStoreDescription` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iIsBranch` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - No, 1 - Yes',
  `vLicenceFront` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vLicenceBack` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vBankName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAccountHolderName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAccountNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vIBAN` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vRoutingNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fStoreLatitude` decimal(10,8) DEFAULT 0.00000000,
  `fStoreLongitude` decimal(10,8) DEFAULT 0.00000000,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`iStoreId`, `iUserId`, `vStoreName`, `vStoreLocation`, `vStoreAddressLine2`, `txStoreDescription`, `iIsBranch`, `vLicenceFront`, `vLicenceBack`, `vBankName`, `vAccountHolderName`, `vAccountNumber`, `vIBAN`, `vRoutingNumber`, `fStoreLatitude`, `fStoreLongitude`, `iCreatedAt`, `iUpdatedAt`) VALUES
(13, 46, 'Flower Shop', 'asdasdasdasdsad', 'asdadasdasdsa', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, 'store2.png', 'store2.png', 'asddasd', 'asdasd', 'adadasdasd', 'asdasdsa', 'sdasdsad', '23.01606000', '72.50649000', NULL, NULL),
(14, 47, 'Cake Shop', 'asdasdasdasdsad', 'dadasdasdasd', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, 'store1.png', 'store1.png', 'asddasd', 'asdasd', 'adadasdasd', 'asdasdsa', 'sdasdsad', '23.01606000', '72.50649000', NULL, NULL),
(43, 101, 'Mocha', 'H.L', 'nr,panjrapol', NULL, 1, '1608284787-159546-229777.jpg', '1608284787-776512-753482.jpg', 'citi', 'Viru Duggal', '54646646', '46464654', '654646464', '23.01606000', '72.50649000', 1608284770, 1608284806),
(44, 104, 'My Store', 'Abc,abc', 'Abc,abc', NULL, 1, '1608298468-353821-731174.png', '1608298468-799845-940039.png', 'asdsad', 'sadsadsa', 'dsadsadsa', 'dsadsa', 'dsadsa', '0.00000000', '0.00000000', 1608298393, 1608298477),
(45, 109, 'Helen\'s Bakery', 'Henri-Spaak-Strasse 142', 'London, Eshaness', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00000000', '0.00000000', 1609312587, 1609312587),
(46, 110, 'Cake Sapde', '21, ABC, NY', 'ABC Streets, LA', NULL, 0, '1609325627-847319-110934.jpeg', '1609325627-975177-270578.jpeg', 'Cake Bank', 'Spade Mark', '98949984', '979797', '98', '0.00000000', '0.00000000', 1609325197, 1609325627),
(47, 111, 'Cake Spade', '21, ABC Street, LA', 'Block B2S, Ny', NULL, 0, '1609325413-710432-668753.jpeg', '1609325413-740475-772972.jpeg', 'Cake Bank', 'Spade Mark', '9999 8888 777 22', '888 55 444 22', '6666 1 55 999', '0.00000000', '0.00000000', 1609325392, 1609325449),
(48, 112, 'Cake Store 31', 'Henri-Spaak-Strasse 142', 'London, Eshaness', NULL, 1, '1609326310-547369-206794.jpeg', '1609326310-663797-912868.jpeg', 'Bank Galaxy', 'mandlorian', '9999 3000 25877', '8855 444 555', '88 99 6666633 1', '0.00000000', '0.00000000', 1609326261, 1609326509),
(49, 113, 'Flower Store 99', 'Black Appt, Nr, Road', 'AB Street, LA', NULL, 0, '1609326746-171352-763845.jpeg', '1609326746-215005-896581.jpeg', 'Lily Bank', 'Rosy Mehra', '888 8555 77 444', '59 77 8 5555', '4777 7 77 7', '0.00000000', '0.00000000', 1609326683, 1609326812),
(50, 137, 'My Store 8', 'Henri-Spaak-Strasse 142', 'London, Eshaness', NULL, 0, '1610088984-312215-999320.png', '1610088984-685243-932087.png', 'asadsadsad', 'sadasd', 'asdasdasd', 'asdasd', 'asdsadsa', '0.00000000', '0.00000000', 1610088954, 1610088994),
(51, 138, 'iOS Store', 'Mansi Circle', 'Ahmedabad', NULL, 0, '1610098557-640161-486230.png', '1610098557-897415-738184.png', 'State Bank Of iOS', 'iOS', '999999999999', '9999', '9999', '0.00000000', '0.00000000', 1610098309, 1610098599),
(52, 139, 'test', 'ttt', 'ttt', NULL, 0, '1610098368-437968-219141.png', '1610098368-745245-374105.png', 'tessss', 'ttt', '1234567890123456', '66666', '6666', '0.00000000', '0.00000000', 1610098329, 1610098404),
(59, 140, 'Test', 'A-102 Suryadeep complex near shastrinagar', 'egqegf', NULL, 0, '1610105293-628579-579206.png', '1610105293-804086-113905.png', 'Axis bank', 'Aakash Brahmbhatt', '101101010101010', '789456123', '123456', '0.00000000', '0.00000000', 1610105151, 1610105329),
(60, 146, 'android store', 'morbi', 'test', NULL, 0, '1611314393-869233-327284.png', '1611314393-430891-434878.png', 'test', 'test', '222222222222', '22222', '2222222', '0.00000000', '0.00000000', 1611314349, 1611314412),
(61, 155, 'shawn 1', 'India', 'Gujrat', NULL, 1, '', '', NULL, NULL, NULL, NULL, NULL, '0.00000000', '0.00000000', 1613630333, 1613630652),
(62, 158, 'store test', 'india', 'gujrat', NULL, 0, '', '', NULL, NULL, NULL, NULL, NULL, '0.00000000', '0.00000000', 1613726992, 1613727137);

-- --------------------------------------------------------

--
-- Table structure for table `store_categories`
--

CREATE TABLE `store_categories` (
  `iStoreCategoriesId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iCategoryId` int(10) UNSIGNED NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_categories`
--

INSERT INTO `store_categories` (`iStoreCategoriesId`, `iStoreId`, `iCategoryId`, `iCreatedAt`, `iUpdatedAt`) VALUES
(5, 14, 3, 1606454762, 1606454762),
(6, 14, 4, NULL, NULL),
(7, 14, 5, 1606454762, 1606454762),
(8, 14, 6, NULL, NULL),
(23, 43, 3, 2020, 2020),
(24, 43, 4, 2020, 2020),
(25, 44, 3, 2020, 2020),
(26, 44, 4, 2020, 2020),
(27, 44, 5, 2020, 2020),
(28, 44, 6, 2020, 2020),
(29, 47, 3, 2020, 2020),
(30, 47, 4, 2020, 2020),
(31, 48, 4, 2020, 2020),
(32, 48, 5, 2020, 2020),
(33, 49, 4, 2020, 2020),
(34, 49, 5, 2020, 2020),
(35, 49, 6, 2020, 2020),
(36, 50, 3, 2021, 2021),
(37, 50, 4, 2021, 2021),
(38, 50, 5, 2021, 2021),
(39, 50, 6, 2021, 2021),
(40, 52, 4, 2021, 2021),
(41, 51, 3, 2021, 2021),
(42, 51, 4, 2021, 2021),
(43, 51, 5, 2021, 2021),
(44, 51, 6, 2021, 2021),
(45, 59, 3, 2021, 2021),
(46, 59, 4, 2021, 2021),
(47, 59, 5, 2021, 2021),
(48, 60, 3, 2021, 2021),
(49, 60, 4, 2021, 2021),
(50, 60, 5, 2021, 2021),
(51, 60, 6, 2021, 2021),
(58, 13, 5, NULL, NULL),
(59, 13, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_declined_order`
--

CREATE TABLE `store_declined_order` (
  `iStoreDeclinedOrder` int(10) UNSIGNED NOT NULL,
  `iDeclineReasonId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iStoreOrderId` int(10) UNSIGNED NOT NULL,
  `vDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `store_images`
--

CREATE TABLE `store_images` (
  `iStoreImageId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `vImageName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_images`
--

INSERT INTO `store_images` (`iStoreImageId`, `iStoreId`, `vImageName`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 13, 'store1.png', 1606454762, 1606454762),
(2, 13, 'store2.png', NULL, NULL),
(3, 14, 'store3.png', 1606454762, 1606454762),
(4, 14, 'store4.png', 1606454762, 1606454762),
(5, 46, '1609325627-613940-527572.jpeg', 1609325627, 1609325627),
(6, 48, '1609326521-365341-734239.jpeg', 1609326521, 1609326521),
(7, 51, '1610098724-337801-405265.jpg', 1610098724, 1610098724),
(8, 61, '1613630333-205315-695079.png', NULL, NULL),
(9, 62, '1613726992-318976-274257.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_orders`
--

CREATE TABLE `store_orders` (
  `iStoreOrderId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iSubOrderId` int(10) UNSIGNED NOT NULL,
  `iOrderAllocationId` int(10) UNSIGNED DEFAULT NULL,
  `tiStatus` tinyint(4) NOT NULL COMMENT ' 0 - Pending, 1 - Received, 2 - Rejected, 3 - Start packing, 4 - Ready for pick-up, 5 - Driver on the way for pickup, 6 - Driver picked the order, 7 - Driver on the way for delivery, 8 - Completed',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_orders`
--

INSERT INTO `store_orders` (`iStoreOrderId`, `iStoreId`, `iSubOrderId`, `iOrderAllocationId`, `tiStatus`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 13, 7, NULL, 4, 1614018600, 1613984377),
(2, 51, 8, NULL, 6, 1611304905, 1611305148),
(3, 51, 9, NULL, 8, 1611307630, 1611644946),
(4, 51, 10, NULL, 6, 1611379024, 1611379119),
(5, 13, 11, NULL, 1, 1613932200, 1612161810),
(6, 14, 12, NULL, 1, 1611380097, 1611642491),
(7, 13, 13, NULL, 10, 1611382663, 1612442308),
(8, 51, 14, NULL, 3, 1611383273, 1611385712),
(9, 51, 15, NULL, 8, 1611385773, 1611560386),
(10, 51, 16, NULL, 0, 1611385840, 1611385840),
(11, 51, 17, NULL, 0, 1611385918, 1611385918),
(12, 48, 18, NULL, 10, 1611401809, 1611402530),
(13, 51, 19, NULL, 10, 1611558574, 1611558633),
(14, 51, 20, NULL, 7, 1611558714, 1611565826),
(15, 13, 21, NULL, 10, 1611581778, 1611581785),
(16, 13, 22, NULL, 1, 1611642876, 1611642960),
(17, 14, 23, NULL, 8, 1611642876, 1611645362),
(18, 13, 24, NULL, 1, 1612951125, 1612951139),
(19, 13, 25, NULL, 1, 1612951367, 1612951378),
(20, 13, 26, NULL, 1, 1612951500, 1612951507),
(21, 13, 27, NULL, 1, 1612951581, 1612951594),
(24, 13, 30, NULL, 0, 1612957097, 1612957097);

-- --------------------------------------------------------

--
-- Table structure for table `store_ratings`
--

CREATE TABLE `store_ratings` (
  `iStoreRatingId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iStoreOrderId` int(10) UNSIGNED NOT NULL,
  `iReviewByUserId` int(10) UNSIGNED NOT NULL,
  `fRating` double(2,1) NOT NULL DEFAULT 0.0,
  `vReview` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_ratings`
--

INSERT INTO `store_ratings` (`iStoreRatingId`, `iStoreId`, `iStoreOrderId`, `iReviewByUserId`, `fRating`, `vReview`, `iCreatedAt`, `iUpdatedAt`) VALUES
(2, 14, 17, 151, 1.5, 'Dummy text Dummy text Dummy text Dummy textDummy textDummy text', 1611654059, 1611654059);

-- --------------------------------------------------------

--
-- Table structure for table `sub_order_master`
--

CREATE TABLE `sub_order_master` (
  `iSubOrderId` int(10) UNSIGNED NOT NULL,
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `iStoreId` int(10) UNSIGNED NOT NULL,
  `iOrderAllocationId` int(10) UNSIGNED DEFAULT NULL,
  `tiCancelledBy` tinyint(1) DEFAULT NULL COMMENT ' 0 - Admin, 1- Driver, 2 - Customer, 3 - Store ',
  `tiSubOrderStatus` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - Pending , 1 - Store Accept , 2 - Store Rejected, 3 - In Process, 4 - Processed, 5 - Driver on the way for pickup, 6 - Driver picked the order, 7 - Driver on the way for delivery, 8 - Completed, 10 - Cancelled',
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_order_master`
--

INSERT INTO `sub_order_master` (`iSubOrderId`, `iOrderId`, `iStoreId`, `iOrderAllocationId`, `tiCancelledBy`, `tiSubOrderStatus`, `iCreatedAt`, `iUpdatedAt`) VALUES
(3, 69, 13, 2, NULL, 1, 1609917904, 1609917904),
(7, 80, 13, NULL, NULL, 4, NULL, 1613984377),
(8, 80, 51, NULL, NULL, 6, NULL, 1611305148),
(9, 81, 51, 23, NULL, 8, NULL, 1611644946),
(10, 82, 51, NULL, NULL, 6, NULL, 1611379119),
(11, 73, 13, NULL, NULL, 1, NULL, 1612161810),
(12, 73, 14, NULL, NULL, 1, NULL, 1611642491),
(13, 83, 13, 102, 2, 10, NULL, 1612442308),
(14, 84, 51, NULL, NULL, 3, NULL, 1611385712),
(15, 85, 51, 41, NULL, 8, NULL, 1611560386),
(16, 86, 51, NULL, NULL, 0, NULL, NULL),
(17, 87, 51, NULL, NULL, 0, NULL, NULL),
(18, 88, 48, NULL, 2, 10, NULL, 1611402530),
(19, 90, 51, NULL, 2, 10, NULL, 1611558633),
(20, 91, 51, 53, NULL, 7, NULL, 1611558762),
(21, 92, 13, NULL, 2, 10, NULL, 1611581785),
(22, 93, 13, NULL, NULL, 1, NULL, 1611642960),
(23, 93, 14, 78, NULL, 8, NULL, 1611645362),
(24, 94, 13, NULL, NULL, 1, NULL, 1612951139),
(25, 95, 13, NULL, NULL, 1, NULL, 1612951378),
(26, 96, 13, NULL, NULL, 1, NULL, 1612951507),
(27, 97, 13, NULL, NULL, 1, NULL, 1612951594),
(30, 98, 13, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vFullName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vProfilePic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vMobileNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `vFullName`, `email`, `password`, `vProfilePic`, `vMobileNumber`) VALUES
(5, 'Andhre Russel', 'andhre.spaceovhuk@gmail.com', '12345678', 'default.png', '9228723322'),
(6, 'Andhre Russel', 'andhre.spaceo123@gmail.com', '12345678', 'default.png', '9228723322'),
(7, 'Andhre Russel', 'andhre.spaceo1hh@gmail.com', '12345678', 'default.png', '9228723322'),
(8, 'Andhre Russel', 'andhre.spaceo777@gmail.com', '12345678', 'default.png', '9228723322'),
(9, 'Andhre Russel', 'andhre.spaceo963@gmail.com', '12345678', 'default.png', '9228723322'),
(10, 'Andhre Russel', 'andhre.spaceo963dr@gmail.com', '12345678', 'default.png', '9228723322'),
(11, 'Andhre Russel', 'andhre.spaceo753@gmail.com', '12345678', 'default.png', '9228723322'),
(12, 'Andhre Russel', 'andhre.spaceo75356@gmail.com', '12345678', 'default.png', '9228723322'),
(13, 'Andhre Russel', 'andhre.spaceo753as56@gmail.com', '12345678', 'default.png', '9228723322'),
(14, 'Andhre Russel', 'andhre.spaceosd53as56@gmail.com', '12345678', 'default.png', '9228723322'),
(15, 'Andhre Russel', 'andhre.spac@gmail.com', '12345678', 'default.png', '9228723322'),
(16, 'ASAS', 's@DF.CVD', 'ASAS', 'default.png', '9228723322'),
(17, 'ASAS', 'sASEDA@DF.CVD', 'ASAS', 'default.png', '9228723322'),
(18, 'ASAS', 'SAM@GMAIL.COM', 'ASAS', 'default.png', '9228723322'),
(19, 'ASAS', 'SAM@GssdMAIL.COM', 'ASAS', 'default.png', '9228723322'),
(20, 'ASAS', 'SAsdM@GssdMAIL.COM', 'ASAS', 'default.png', '9228723322'),
(21, 'ASAS', 'ascsd@GssdMAIL.COM', 'ASAS', 'default.png', '9228723322'),
(22, 'ASAS', 'ascssdd@GssdMAIL.COM', 'ASAS', 'default.png', '9228723322'),
(23, 'ASAS', 'op@gmail.COM', '24154', 'default.png', '9228723322'),
(24, 'ASAS', 'oasdp@gmail.COM', '24154', 'default.png', '9228723322'),
(25, 'ASAS', 'oasasdp@gmail.COM', '24154', 'default.png', '9228723322'),
(26, 'ASAS', 'oasasasdp@gmail.COM', '24154', 'default.png', '9228723322'),
(27, 'ASAS', 'gg@gmail.COM', '24154', 'default.png', '9228723322'),
(28, 'ASAS', 'sgg@gmail.COM', '24154', 'default.png', '9228723322'),
(29, 'ASAS', 'sgqag@gmail.COM', '24154', 'default.png', '9228723322'),
(30, 'ASAS', 'asdcx@gmail.COM', '24154', 'default.png', '9228723322'),
(31, 'ASAS', 'asasasdcx@gmail.COM', '24154', 'default.png', '9228723322'),
(32, 'shawn Giles', 'shawngiles@gmail.com', '12345678', 'default12.png', '9228723322'),
(33, 'Andhre Russel', 'andhre.spaceo@gmail.com', '12345678', 'default.png', '9228723322'),
(36, 'Andhre Russel', 'andhre.spa@gmail.com', '12345678', 'default.png', '9224544745');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iUserId` int(10) UNSIGNED NOT NULL,
  `vFullName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vISDCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vMobileNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vOTP` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iOTPExpireAt` int(10) UNSIGNED DEFAULT NULL,
  `tiIsMobileVerified` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - No, 1 - Yes',
  `vLanguage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en' COMMENT 'en - English, ar - Arablic',
  `vProfilePic` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vFacebookId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vGoogleId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vAppleId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiIsSocialLogin` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 - No, 1 - Yes',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vPasswordResetToken` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iPasswordExpireAt` int(11) NOT NULL,
  `enType` enum('Customer','Partner','Driver') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Customer',
  `enStatus` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `tiAdminApproved` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-No, 1- Yes',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiIsGuest` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 = Normal User, 1 = Guest',
  `tiIsOnline` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0 - Offline, 1 - Online ',
  `enIsDeleted` enum('No','Yes') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `iLastLoginAt` int(10) UNSIGNED DEFAULT NULL,
  `iCreatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL,
  `iDeletedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iUserId`, `vFullName`, `email`, `vISDCode`, `vMobileNumber`, `vOTP`, `iOTPExpireAt`, `tiIsMobileVerified`, `vLanguage`, `vProfilePic`, `vFacebookId`, `vGoogleId`, `vAppleId`, `tiIsSocialLogin`, `password`, `vPasswordResetToken`, `iPasswordExpireAt`, `enType`, `enStatus`, `tiAdminApproved`, `remember_token`, `tiIsGuest`, `tiIsOnline`, `enIsDeleted`, `iLastLoginAt`, `iCreatedAt`, `iUpdatedAt`, `iDeletedAt`) VALUES
(45, 'Jamess Stiger', 'jamesstiger.cis+101@gmail.com', '+91', '7894567890', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$A4AUxXJpKPbLnE05y6XKVeOFK.Oo8ndXlVPepqC5tpl6lEdGLqzBW', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1606898416, 1606898416, NULL),
(46, 'Store Owner 1', 'jamesstiger.cis+201@gmail.com', '+91', '8528528520', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$mp2gFDwZmlpADdhkQoNrvubPEfHyW6DeowUWRPM/l5Et5sX52ElCm', NULL, 0, 'Partner', 'Active', 0, 'xDOw6sFg3wQFe7KVbrTAtN59fr2AXJGOtTtqiqdfE9JARmOc9JRAIfotkcHg', 0, 1, 'No', NULL, 1606898457, 1606898457, NULL),
(47, 'Store Owner 2', 'jamesstiger.cis+202@gmail.com', '+91', '4564654654654', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$b1Wk74ybnfIDDYIoo05w9.G4IAjS2VYSlvEgQTMOOBsefZ2zkPYuO', NULL, 0, 'Partner', 'Active', 0, 'jZzoBTElOJ6PL9viyH51uavVJlNHutexmR6mb4PbVz0gEmfa7QPpaZo3uPbN', 0, 1, 'No', NULL, 1606898480, 1606898480, NULL),
(48, 'iOS Testing', 'iOS@test1.com', '+91', '9898989898', '336112', 1607326449, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$QUxR27PgTQ2y97G5Mv4ZmeobIPLQHhzE7ti1Yz0uNB7JYDZ43pwT.', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1607325847, 1607325847, NULL),
(49, 'Sagar', 'sagar@gmail.con', '+966', '12345678900', '974589', 1607338411, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$8wcvJmSMenB5.sJ3yxdUVun0uLeCa8Ef.H1kHRuuFi/y4bYftl6nK', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1607337810, 1607337810, NULL),
(50, 'Mihir Shah', 'redminote7proa55@gmail.com', '+966', '9137921629', '370930', 1607345192, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$EFyjutdRshx3WfM4RlSlUuO7K1neiwQQf/2.mlXRvH4RMDNPbCane', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1607344592, 1607344592, NULL),
(51, NULL, '', NULL, NULL, NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Customer', 'Active', 0, NULL, 0, 1, 'No', NULL, 1607776006, 1607776006, NULL),
(53, NULL, '', NULL, NULL, NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Customer', 'Active', 0, NULL, 0, 1, 'No', NULL, 1607776173, 1607776173, NULL),
(54, NULL, '', NULL, NULL, NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Customer', 'Inactive', 0, NULL, 0, 1, 'No', NULL, 1607776308, 1607776308, NULL),
(55, NULL, '', NULL, NULL, NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Customer', 'Inactive', 0, NULL, 0, 1, 'No', NULL, 1607777445, 1607777445, NULL),
(56, 'Sagar', 'sagar1@gmail.con', '+966', '1414141414', '491295', 1607947664, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$UPiQQKSGkbX0ZhIvlmm4cuWVhYR06uFUdD1/XyDq8PRwGQ2SyCBaC', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1607947063, 1607947063, NULL),
(57, 'Jaykar Parmar', 'jaykar.swim@gmail.com', NULL, NULL, NULL, NULL, 0, 'en', NULL, NULL, '100246697144381708902', NULL, 1, NULL, NULL, 0, 'Customer', 'Active', 0, NULL, 0, 1, 'No', NULL, 1607950432, 1607950432, NULL),
(63, NULL, '', NULL, NULL, NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Customer', 'Inactive', 0, NULL, 0, 1, 'No', NULL, 1608025358, 1608027918, NULL),
(64, NULL, '', NULL, NULL, NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Customer', 'Inactive', 0, NULL, 0, 1, 'No', NULL, 1608029417, 1608029463, NULL),
(65, 'Archita Rathod', 'architar.spaceo@gmail.com', '+91', '9228723322', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$n5CXrR40TFov4RqYyTQ./ep/qRig7tupsFGcQx4Wp0Get/8FCl54q', NULL, 0, 'Customer', 'Inactive', 0, '1', 0, 1, 'No', NULL, 1608029819, 1608031105, NULL),
(67, 'iOS Testing', 'iOS@test2.com', '+1', '9898989898', '353098', 1608032540, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$v8B0.lMHN7a3zHJKj.1PNuAn.Yrnx.NZCK8L3lKPHmY9rWoboLBsG', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1608031893, 1608031938, NULL),
(68, 'Sagarr', 'sagar2@gmail.com', '+966', '1818181818', '704970', 1608034731, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$lq75LXnsNj5fT4s6zqmmXO6n.QmWfgMoT4L2luMk8Tyu.m9Gy01Aa', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1608033436, 1608034130, NULL),
(70, 'Driver 1', 'jamesstiger.cis+201@gmail.com', '+94', '123456123', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Driver', 'Active', 0, NULL, 0, 1, 'No', NULL, NULL, NULL, NULL),
(71, 'Driver 2', 'jamesstiger.cis+202@gmail.com', '+94', '789456789', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Driver', 'Active', 0, NULL, 0, 1, 'No', NULL, NULL, NULL, NULL),
(72, 'Archita Rathod', 'abc@gmail.com', '+91', '9228723322', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$p.6QjPZ.QSqky6bmn9eLjep5NVLdtffnp/FFelO4IivUoiwjkRd.a', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1608199885, 1608199885, NULL),
(101, 'Viru Paaji', 'mocha@email.com', '+966', '1233', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$.HhmtK7y5rg.vrkpzuD/VOnj6AKlyt58WfAGki5yrm4.ozPr0JvMm', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1608284769, 1608284769, NULL),
(102, 'iOS Testing', 'iOS@test4.com', '+1', '9898989898', '676905', 1608292904, 1, 'en', NULL, NULL, NULL, NULL, 0, 'iOS@1234', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1608292223, 1608292287, NULL),
(104, 'James301', 'jamesstiger.cis+301@gmail.com', '+966', '897789987', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$c0t2PYVR2Q8DFY1dwpSwD.nWtOPBHxL2AJ6UtZVlm8IKMIh5PLr3q', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1608298393, 1608298393, NULL),
(105, 'Mihir shah', 'mihirs.spaceo@gmail.co', '+966', '9137921629', '505560', 1608533041, 1, 'en', NULL, NULL, NULL, NULL, 0, 'abc123', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1608532162, 1608532440, NULL),
(107, 'Sagar', 'sagar@gmail.com', '+966', '1010101010', '990116', 1609334287, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$rQRd96gZNNXo4r.LCZiWTubBKWgCv6tX4aA3RLOtbTeDh1ZTu09Sq', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1609238621, 1609238621, NULL),
(109, 'James401', 'jamesstiger.cis+401@gmail.com', '+966', '456456654', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$ilVtcK7lx4C6TTTxM.jx2uYdcJuh8CcScLzVaOcm/m3BAO/HSYOH.', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1609312586, 1609312586, NULL),
(110, 'Spade Mark', 'cake@spade.com', '+966', '85784578787', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$/kjtoIfhNqBKsN/UlpGiJebvJIqWXsgABUKwqII5f4zPHn/m4EVZG', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1609325196, 1609325196, NULL),
(111, 'Spade Mark', 'cake@spade.com', '+966', '4578467884', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$1sW2WUGw8W/lULobIl30IeghvypAEERDQpBYo6EDUR.l419heVz8.', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1609325391, 1609325391, NULL),
(112, 'Owner 31', 'cake@store31.com', '+966', '8745412478', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$uSbbunoUoSzcbm14gfNv0.1ki175OFVjduiKMw9jmr9UckGggiP2y', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1609326261, 1609326261, NULL),
(113, 'Rosy', 'flower@99.com', '+966', '85475124785', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$6EVwo7M3kGDshceJRzQJtepcTAFe/o5.Ox5Fh.yfj.JIcwjAY4j6i', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1609326682, 1609326682, NULL),
(114, 'Driver 1', 'driver@1.com', '+91', '7845127845', '410895', 1609327732, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$zOGz.58YJnGKiht7aCy.9usElqi1hWaWTMru3QDUqg7j6ZoGPQ4ZS', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1609327132, 1609327132, NULL),
(115, 'Driver 2', 'driver@2.com', '+91', '7845127842', '998326', 1609328403, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$QAWHtoyWPxqSmcwVwL6IEeBKANqXLHDkCheDC8OkQ5f79mZ33wPhe', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1609327803, 1609327803, NULL),
(116, 'Mihir SHah', 'mihirs.spaceo@gmail.com', '+91', '9137921629', '491783', 1611382674, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$G3rg3o8VeAMbGlLM.v4K2OQXTdyUItQ5jZpsHP6GFkfT4hNqTICBK', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1609737544, 1609737544, NULL),
(117, 'mihir shah', 'mihirs.spaceo.@gmail.co', '+966', '91379216291', '060146', 1609738390, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$P1XciCq1LSIzRfik/6E53edJqNLc9BOHd5L1d8s3ScUvfX8HHFr0a', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1609737790, 1609737790, NULL),
(118, 'mihir shah', 'mihirs.spaceo2@gmail.co', '+966', '91379216292', '378483', 1609852272, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$G0xGmhVJFVAyqOTCHo3l6u9KvA1w30vn00doSBJLjmW.gA..f7Esa', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1609738182, 1609738182, NULL),
(119, 'Test', 'test@gmail.con', '+966', '1414141212', '673614', 1609831054, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$ry8sG8jEO8o0TNejbC3InOsTtE1F917WQaxRkmqzxCBGk8ffIp/WC', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1609830453, 1609830453, NULL),
(120, 'mihir', 'mihirs.spaceo3@gmail.com', '+966', '91379216293', '665520', 1609849696, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$C4KNAe2ly46bJ3EtJput4ecqBJAFzcM8Utor0Wi5vnOkFgJKhYst6', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1609849095, 1609849095, NULL),
(121, 'mihir shah', 'mihirs.spaceo4@gmail.com', '+966', '91379216294', '663823', 1609850849, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$6CAAvZcXquBUm/PNjm9.G.brVt/ObKyPoqUP4XdwnYj8LJcZxN.i6', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1609850249, 1609850249, NULL),
(122, 'mihir shah', 'mihirs.spaceo2@gmail.com', '+966', '91379216292', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$1GOKfZdZ8s.l7uVS32//E.hCWWnGcjhtiE6oQ13P0tkrgcmNu4sNu', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1609851613, 1609851672, NULL),
(126, 'egshshdg', 'shsheh@gmail.com', '+966', '4549777979', '395037', 1610008639, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$Lq7J1pkj/VeKB6rSDknCiuoSReBpLgey3/XFFd9jnqmTpbgfViJ2u', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1610008038, 1610008038, NULL),
(127, 'Aaksh brahmbhtt', 'test@gmail.com', '+91', '454949494', '099609', 1610011527, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$lFVBCGtFTIWnRBtDdt5MxOKDU.npMR3i2UoZpbs.bwc7UOu5k/yTS', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1610008638, 1610008638, NULL),
(128, 'shshshsb', 'test1@gmail.com', '+966', '46797978', '197790', 1610009711, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$UK3jofXz/H4vT6T33zdMdeygtU1wwDOMd8i81VgDYXRPhKiVGlD/O', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1610008793, 1610008793, NULL),
(129, 'sgsbsbsb', 'teet2@gmail.com', '+966', '564979797', '166565', 1610009418, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$rad6loxUymIodMdZBjHPF.hW9ORx1CSQ2BicORiIWEeykKpJqMUqS', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1610008818, 1610008818, NULL),
(130, 'sgshshsb', 'hshsb@gmail.com', '+966', '4979798594', '638468', 1610010228, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$A3go0BPNhK95KDysVf7oz.rBPRCd8KLAls49bsE4krvF8JS2gafCK', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1610009567, 1610009567, NULL),
(131, 'testing', 'testing@gmial.com', '+91', '9512020373', '050349', 1610010480, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$oc6.n/tuwi8r5DjEajiO/.rFWl1rVNscsAKQX0Nt5HRc.olbl5BJS', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1610009880, 1610009880, NULL),
(133, 'Jaydip', 'jaydip.spaceo@gmail.com', NULL, NULL, NULL, NULL, 0, 'en', NULL, '2753941361586665', NULL, NULL, 1, NULL, NULL, 0, 'Customer', 'Active', 0, NULL, 0, 1, 'No', NULL, 1610011209, 1610011209, NULL),
(134, 'test yop', 'test@yopmil.com', '+966', '56494979', '183559', 1610015336, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$DUHGTk9rQlDOCjs5Fu/Pde2BuHqWNc4iF3K9U1ezlKUHNH/B0sTxC', '6dbf519725865bb3f3fb6f32e03f510e', 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1610014736, 1610014736, NULL),
(137, 'James 8', 'jamesstiger.cis+202@gmail.com', '+966', '7894567890', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$5.F9reXkHRPiG5kQhoipYOyYnD16oHYtX2M61HcngevqqOoZTDgMq', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1610088953, 1610088953, NULL),
(138, 'iOS', 'ios@store.com', '+91', '9898989898', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$27a9MHyAP7s1WnVXlSXUnu8mIDxovRDHQ2DlNY2oYrbMUrRSL5p5y', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1610098309, 1610098309, NULL),
(139, 'test1', 'test@gmail.coon', '+966', '1212121212', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$gwmjaLhTr.VESsj8CdInJOdmMPdxjrfzdJywgm5/qYQWHdiM6Y3Eq', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1610098329, 1610098329, NULL),
(140, 'Testing team', 'aakash@yopmail.com', '+91', '8780614979', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$bNbMVfMkMF55ZNe8CXcS6.y1tRgNkwL64poeQcQBCG1nWbLJ.8yzS', '5823f43d53affe42e8d9b4c0398aa270', 1610194537, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1610105151, 1610105151, NULL),
(145, 'IOS Driver', 'ios.driver101@gmail.com', '+91', '56785678', '735997', 1611306933, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$DjPb.J6JUmCnqa/CDtPmmeXlK21.Ig/.SmjCbj1CZvFQEYZe4SLPm', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1611306251, 1611306251, NULL),
(146, 'sagar', 'androidstore@gmail.com', '+966', '1111111111', '1234', NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$yRY6xrGrNDRL16gFxmBrNOMt1wJxVoR4N6HCJe.4GLoYz06DevdvW', NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, 1611314348, 1611314348, NULL),
(147, 'Test', 'test2@gmail.com', '+966', '1414141415', '004657', 1611325938, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$3GAAfUufNSKyd5h.bwkB2uSFh0/HuA1WnCMKxCnGHQo1s6Wc2duYO', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1611325337, 1611325337, NULL),
(148, 'Test Driver', 'driver@gmail.com', '+966', '222222222', '052436', 1611379381, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$NHm/1dAqjb9/meEOrqvY3ew.hEtwbt.wxc5tP0/YHilu8ZChkE..2', NULL, 0, 'Driver', 'Active', 0, '1', 0, 1, 'No', NULL, 1611378780, 1611378780, NULL),
(151, 'Android Test', 'android@gmail.com', '+966', '17171717', '104836', 1611558817, 1, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$LJxdK70NVqhardSdVvqhGe1RHgt1dVpwI94pOJTaDfttQhEEeLS1C', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1611558216, 1611558216, NULL),
(152, 'Archita Rathod', 'architar.spaceaso@gmail.com', '+91', '9228723322', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$JQQqQudxYr8s1OllZTR86em7CHMv5F02sEfycyYDyaNcLApafcj6e', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1613371677, 1613371677, NULL),
(153, 'Archita Rathod', 'architar.spacead@gmail.com', '+91', '9228753322', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, '$2y$10$X/Jtsy5xaEkb9.ff9rbXwuxe9Qszdks4lZBtJvH3lxs8/P9uyRkO6', NULL, 0, 'Customer', 'Active', 0, '1', 0, 1, 'No', NULL, 1613371841, 1613371841, NULL),
(154, 'Shawn Giles', 'shawngiles.cis@gmail.com', NULL, '9875641235', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Customer', 'Active', 0, NULL, 0, 1, 'No', NULL, NULL, NULL, NULL),
(155, 'shwan store 1', 'shawngiles.cis+204@gmail.com', '+91', '9874563214', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, NULL, NULL, NULL),
(156, 'Driver 21', 'driver21@gmail.com', '91', '9874563212', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Driver', 'Active', 1, NULL, 0, 1, 'No', NULL, 1613728395, 1613728395, NULL),
(158, 'store test', 'storetest@gmail.com', '+91', '7896541235', NULL, NULL, 0, 'en', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'Partner', 'Active', 0, NULL, 0, 1, 'No', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE `user_notification` (
  `iUserNotificationId` int(10) UNSIGNED NOT NULL,
  `iReceiveByUserId` int(10) UNSIGNED NOT NULL,
  `iOrderId` int(10) UNSIGNED NOT NULL,
  `vNotificationTitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vNotificationDesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txDynamicField` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vNotificationTag` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiNotificationUserType` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Customer, 2 - Store, 3 - Driver, 4 - Admin',
  `tiIsRead` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Yes, 0 - No',
  `tiIsDeleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Yes, 0 - No',
  `tiIsActive` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1 - Yes, 0 - No',
  `iCreatedAt` int(10) DEFAULT NULL,
  `iUpdatedAt` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_notification`
--

INSERT INTO `user_notification` (`iUserNotificationId`, `iReceiveByUserId`, `iOrderId`, `vNotificationTitle`, `vNotificationDesc`, `txDynamicField`, `vNotificationTag`, `tiNotificationUserType`, `tiIsRead`, `tiIsDeleted`, `tiIsActive`, `iCreatedAt`, `iUpdatedAt`) VALUES
(1, 118, 69, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:2:{s:8:\"vOrderId\";s:18:\"CHO421693901859978\";s:11:\"driver_name\";s:10:\"mihir shah\";}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(2, 121, 69, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:2:{s:8:\"vOrderId\";s:18:\"CHO421693901859978\";s:11:\"driver_name\";s:10:\"mihir shah\";}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(3, 120, 69, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:2:{s:8:\"vOrderId\";s:18:\"CHO421693901859978\";s:11:\"driver_name\";s:5:\"mihir\";}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(4, 119, 69, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:2:{s:8:\"vOrderId\";s:18:\"CHO421693901859978\";s:11:\"driver_name\";s:4:\"Test\";}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(17, 122, 69, 'driverAcceptOrderTitle', 'driverAcceptOrderMsg', 'a:2:{s:13:\"customer_name\";s:10:\"mihir shah\";s:11:\"driver_name\";s:10:\"mihir shah\";}', 'order_accepted', 1, 0, 0, 0, NULL, NULL),
(18, 121, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:8;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(19, 120, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:8;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(20, 119, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:8;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(21, 118, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:8;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(22, 119, 81, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1055615457147640\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:9;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(23, 118, 81, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1055615457147640\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:9;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(24, 145, 81, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1055615457147640\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:9;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(25, 121, 81, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1055615457147640\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:9;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(26, 120, 81, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1055615457147640\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:9;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(27, 48, 81, 'driverAcceptOrderTitle', 'driverAcceptOrderMsg', 'a:2:{s:13:\"customer_name\";s:11:\"iOS Testing\";s:11:\"driver_name\";s:10:\"IOS Driver\";}', 'order_accepted', 1, 0, 0, 0, NULL, NULL),
(28, 148, 82, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1788456400230661\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:10;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(29, 145, 82, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1788456400230661\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:10;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(30, 121, 82, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1788456400230661\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:10;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(31, 120, 82, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1788456400230661\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:10;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(32, 119, 82, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1788456400230661\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:10;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(33, 118, 82, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1788456400230661\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:10;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(34, 48, 81, 'driverAcceptOrderTitle', 'driverAcceptOrderMsg', 'a:2:{s:13:\"customer_name\";s:11:\"iOS Testing\";s:11:\"driver_name\";s:10:\"IOS Driver\";}', 'order_accepted', 1, 0, 0, 0, NULL, NULL),
(35, 148, 84, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1626299241596692\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:14;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(36, 145, 84, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1626299241596692\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:14;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(37, 121, 84, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1626299241596692\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:14;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(38, 120, 84, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1626299241596692\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:14;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(39, 119, 84, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1626299241596692\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:14;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(40, 118, 84, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1626299241596692\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:14;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(41, 116, 84, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1626299241596692\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:14;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(42, 119, 85, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1886123817411119\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:15;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(43, 118, 85, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1886123817411119\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:15;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(44, 116, 85, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1886123817411119\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:15;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(45, 148, 85, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1886123817411119\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:15;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(46, 145, 85, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1886123817411119\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:15;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(47, 121, 85, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1886123817411119\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:15;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(48, 120, 85, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1886123817411119\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:15;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(49, 121, 85, 'driverAcceptOrderTitle', 'driverAcceptOrderMsg', 'a:2:{s:13:\"customer_name\";s:10:\"mihir shah\";s:11:\"driver_name\";s:10:\"Mihir SHah\";}', 'order_accepted', 1, 0, 0, 0, NULL, NULL),
(50, 119, 90, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO476878271903264\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:19;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(51, 118, 90, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO476878271903264\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:19;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(52, 116, 90, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO476878271903264\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:19;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(53, 148, 90, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO476878271903264\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:19;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(54, 145, 90, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO476878271903264\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:19;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(55, 121, 90, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO476878271903264\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:19;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(56, 120, 90, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO476878271903264\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:19;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(57, 148, 91, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO972878821648546\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:20;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(58, 145, 91, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO972878821648546\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:20;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(59, 121, 91, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO972878821648546\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:20;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(60, 120, 91, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO972878821648546\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:20;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(61, 119, 91, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO972878821648546\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:20;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(62, 118, 91, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO972878821648546\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:20;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(63, 116, 91, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO972878821648546\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:20;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(64, 151, 91, 'driverAcceptOrderTitle', 'driverAcceptOrderMsg', 'a:2:{s:13:\"customer_name\";s:12:\"Android Test\";s:11:\"driver_name\";s:11:\"Test Driver\";}', 'order_accepted', 1, 0, 0, 0, NULL, NULL),
(65, 118, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:12;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(66, 116, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:12;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(67, 148, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:12;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(68, 145, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:12;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(69, 121, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:12;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(70, 120, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:12;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(71, 119, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:12;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(72, 148, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:22;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(73, 145, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:22;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(74, 121, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:22;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(75, 120, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:22;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(76, 119, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:22;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(77, 118, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:22;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(78, 116, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:22;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(79, 119, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:23;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(80, 118, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:23;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(81, 116, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:23;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(82, 148, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:23;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(83, 145, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:23;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(84, 121, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:23;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(85, 120, 93, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO871875944826274\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:23;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(86, 151, 93, 'driverAcceptOrderTitle', 'driverAcceptOrderMsg', 'a:2:{s:13:\"customer_name\";s:12:\"Android Test\";s:11:\"driver_name\";s:10:\"IOS Driver\";}', 'order_accepted', 1, 0, 0, 0, NULL, NULL),
(87, 47, 93, 'driverOntheWayForPickupTitle', 'driverOntheWayForPickupMsg', 'a:2:{s:13:\"customer_name\";s:12:\"Android Test\";s:11:\"driver_name\";s:10:\"IOS Driver\";}', 'driver_on_the_way_for_pickup', 2, 0, 0, 0, NULL, NULL),
(88, 114, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:8:\"Driver 1\";s:11:\"iSubOrderId\";i:7;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(89, 148, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:7;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(90, 145, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:7;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(91, 121, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:7;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(92, 120, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:7;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(93, 119, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:7;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(94, 118, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:7;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(95, 116, 80, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1331191545691623\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:7;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(96, 118, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:11;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(97, 116, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:11;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(98, 114, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:8:\"Driver 1\";s:11:\"iSubOrderId\";i:11;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(99, 148, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:11;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(100, 145, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:11;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(101, 121, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:11;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(102, 120, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:11;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(103, 119, 73, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO2013809416894852\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:11;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(104, 119, 83, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO513406431903002\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:13;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(105, 118, 83, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO513406431903002\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:13;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(106, 116, 83, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO513406431903002\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:13;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(107, 114, 83, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO513406431903002\";s:11:\"driver_name\";s:8:\"Driver 1\";s:11:\"iSubOrderId\";i:13;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(108, 148, 83, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO513406431903002\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:13;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(109, 145, 83, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO513406431903002\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:13;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(110, 121, 83, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO513406431903002\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:13;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(111, 120, 83, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO513406431903002\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:13;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(112, 121, 83, 'driverAcceptOrderTitle', 'driverAcceptOrderMsg', 'a:2:{s:13:\"customer_name\";s:10:\"mihir shah\";s:11:\"driver_name\";s:10:\"IOS Driver\";}', 'order_accepted', 1, 0, 0, 0, NULL, NULL),
(113, 119, 94, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO915540720343523\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:24;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(114, 118, 94, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO915540720343523\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:24;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(115, 116, 94, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO915540720343523\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:24;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(116, 114, 94, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO915540720343523\";s:11:\"driver_name\";s:8:\"Driver 1\";s:11:\"iSubOrderId\";i:24;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(117, 148, 94, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO915540720343523\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:24;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(118, 145, 94, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO915540720343523\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:24;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(119, 121, 94, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO915540720343523\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:24;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(120, 120, 94, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:18:\"CHO915540720343523\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:24;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(121, 148, 95, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1397795296998762\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:25;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(122, 145, 95, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1397795296998762\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:25;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(123, 121, 95, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1397795296998762\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:25;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(124, 120, 95, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1397795296998762\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:25;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(125, 119, 95, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1397795296998762\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:25;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(126, 118, 95, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1397795296998762\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:25;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(127, 116, 95, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1397795296998762\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:25;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(128, 114, 95, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1397795296998762\";s:11:\"driver_name\";s:8:\"Driver 1\";s:11:\"iSubOrderId\";i:25;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(129, 121, 96, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1491772084109985\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:26;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(130, 120, 96, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1491772084109985\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:26;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(131, 119, 96, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1491772084109985\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:26;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(132, 118, 96, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1491772084109985\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:26;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(133, 116, 96, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1491772084109985\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:26;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(134, 114, 96, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1491772084109985\";s:11:\"driver_name\";s:8:\"Driver 1\";s:11:\"iSubOrderId\";i:26;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(135, 148, 96, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1491772084109985\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:26;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(136, 145, 96, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1491772084109985\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:26;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(137, 114, 97, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1106999031872698\";s:11:\"driver_name\";s:8:\"Driver 1\";s:11:\"iSubOrderId\";i:27;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(138, 148, 97, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1106999031872698\";s:11:\"driver_name\";s:11:\"Test Driver\";s:11:\"iSubOrderId\";i:27;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(139, 145, 97, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1106999031872698\";s:11:\"driver_name\";s:10:\"IOS Driver\";s:11:\"iSubOrderId\";i:27;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(140, 121, 97, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1106999031872698\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:27;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(141, 120, 97, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1106999031872698\";s:11:\"driver_name\";s:5:\"mihir\";s:11:\"iSubOrderId\";i:27;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(142, 119, 97, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1106999031872698\";s:11:\"driver_name\";s:4:\"Test\";s:11:\"iSubOrderId\";i:27;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(143, 118, 97, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1106999031872698\";s:11:\"driver_name\";s:10:\"mihir shah\";s:11:\"iSubOrderId\";i:27;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(144, 116, 97, 'newDeliveryRequestDriTitle', 'newDeliveryRequestDriMsg', 'a:3:{s:8:\"vOrderId\";s:19:\"CHO1106999031872698\";s:11:\"driver_name\";s:10:\"Mihir SHah\";s:11:\"iSubOrderId\";i:27;}', 'new_order', 3, 0, 0, 0, NULL, NULL),
(146, 151, 98, 'newOrderPlacedCustTitle', 'newOrderPlacedCustMsg', 'a:2:{s:13:\"customer_name\";s:12:\"Android Test\";s:8:\"vOrderId\";s:19:\"CHO1850771305690724\";}', 'order_placed', 1, 0, 0, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`iCategoryId`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`iCMSPageId`);

--
-- Indexes for table `contact_us_reason`
--
ALTER TABLE `contact_us_reason`
  ADD PRIMARY KEY (`iContactReasonId`);

--
-- Indexes for table `contact_us_request`
--
ALTER TABLE `contact_us_request`
  ADD PRIMARY KEY (`iRequestId`),
  ADD KEY `contact_us_request_iuserid_foreign` (`iUserId`),
  ADD KEY `contact_us_request_icontactreasonid_foreign` (`iContactReasonId`);

--
-- Indexes for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD PRIMARY KEY (`iCustomerAddressId`),
  ADD KEY `customer_addresses_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `decline_reason`
--
ALTER TABLE `decline_reason`
  ADD PRIMARY KEY (`iDeclineReasonId`);

--
-- Indexes for table `device_details`
--
ALTER TABLE `device_details`
  ADD PRIMARY KEY (`iDeviceId`),
  ADD KEY `device_details_ideviceid_index` (`iDeviceId`),
  ADD KEY `device_details_iuserid_index` (`iUserId`);

--
-- Indexes for table `driver_declined_order`
--
ALTER TABLE `driver_declined_order`
  ADD PRIMARY KEY (`iDeclinedOrderId`),
  ADD KEY `driver_declined_order_ideclinereasonid_foreign` (`iDeclineReasonId`),
  ADD KEY `driver_declined_order_idriverid_foreign` (`iDriverId`),
  ADD KEY `driver_declined_order_iOrderAllocationId_foreign` (`iOrderAllocationId`);

--
-- Indexes for table `driver_details`
--
ALTER TABLE `driver_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `driver_details_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `driver_ratings`
--
ALTER TABLE `driver_ratings`
  ADD PRIMARY KEY (`iDriverRatingId`),
  ADD KEY `driver_ratings_idriverid_foreign` (`iDriverId`),
  ADD KEY `driver_ratings_iorderallocationid_foreign` (`iOrderAllocationId`),
  ADD KEY `driver_ratings_ireviewbyuserid_foreign` (`iReviewByUserId`);

--
-- Indexes for table `driver_running_trips`
--
ALTER TABLE `driver_running_trips`
  ADD PRIMARY KEY (`iDriverRunningTripId`),
  ADD KEY `driver_running_trips_idriverid_foreign` (`iDriverId`),
  ADD KEY `driver_running_trips_isuborderid_foreign` (`iSubOrderId`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`iQuestionId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD PRIMARY KEY (`iOrderedProductId`),
  ADD KEY `ordered_products_iorderid_foreign` (`iOrderId`),
  ADD KEY `ordered_products_iproductid_foreign` (`iProductId`),
  ADD KEY `ordered_products_iproductvariantid_foreign` (`iProductVariantId`),
  ADD KEY `ordered_products_istoreid_foreign` (`iStoreId`);

--
-- Indexes for table `order_allocation`
--
ALTER TABLE `order_allocation`
  ADD PRIMARY KEY (`iOrderAllocationId`),
  ADD KEY `order_allocation_idriverid_foreign` (`iDriverId`),
  ADD KEY `order_allocation_icanceledbyuserid_foreign` (`iCanceledByUserId`),
  ADD KEY `order_allocation_isuborderid_foreign` (`iSubOrderId`);

--
-- Indexes for table `order_master`
--
ALTER TABLE `order_master`
  ADD PRIMARY KEY (`iOrderId`),
  ADD KEY `order_master_iuserid_foreign` (`iUserId`),
  ADD KEY `order_master_ipromocodeid_foreign` (`iPromoCodeId`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_icategoryid_foreign` (`iCategoryId`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`iProductImageId`),
  ADD KEY `product_images_iproductid_foreign` (`iProductId`);

--
-- Indexes for table `product_liked`
--
ALTER TABLE `product_liked`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_liked_iproductid_foreign` (`iProductId`),
  ADD KEY `product_liked_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `product_ratings`
--
ALTER TABLE `product_ratings`
  ADD PRIMARY KEY (`iProductRatingId`),
  ADD KEY `product_ratings_iproductid_foreign` (`iProductId`),
  ADD KEY `product_ratings_isuborderid_foreign` (`iSubOrderId`),
  ADD KEY `product_ratings_ireviewbyuserid_foreign` (`iReviewByUserId`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_variants_iproductid_foreign` (`iProductId`);

--
-- Indexes for table `promo_code`
--
ALTER TABLE `promo_code`
  ADD PRIMARY KEY (`iPromoCodeId`);

--
-- Indexes for table `recent_search`
--
ALTER TABLE `recent_search`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recent_search_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`iSettingsId`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`iStoreId`),
  ADD KEY `store_iuserid_foreign` (`iUserId`);

--
-- Indexes for table `store_categories`
--
ALTER TABLE `store_categories`
  ADD PRIMARY KEY (`iStoreCategoriesId`),
  ADD KEY `store_categories_istoreid_foreign` (`iStoreId`),
  ADD KEY `store_categories_icategoryid_foreign` (`iCategoryId`);

--
-- Indexes for table `store_declined_order`
--
ALTER TABLE `store_declined_order`
  ADD PRIMARY KEY (`iStoreDeclinedOrder`),
  ADD KEY `store_declined_order_ideclinereasonid_foreign` (`iDeclineReasonId`),
  ADD KEY `store_declined_order_istoreid_foreign` (`iStoreId`),
  ADD KEY `store_declined_order_istoreorderid_foreign` (`iStoreOrderId`);

--
-- Indexes for table `store_images`
--
ALTER TABLE `store_images`
  ADD PRIMARY KEY (`iStoreImageId`),
  ADD KEY `store_images_istoreid_foreign` (`iStoreId`);

--
-- Indexes for table `store_orders`
--
ALTER TABLE `store_orders`
  ADD PRIMARY KEY (`iStoreOrderId`),
  ADD KEY `store_orders_istoreid_foreign` (`iStoreId`),
  ADD KEY `store_orders_iorderallocationid_foreign` (`iOrderAllocationId`),
  ADD KEY `store_orders_isuborderid_foreign` (`iSubOrderId`);

--
-- Indexes for table `store_ratings`
--
ALTER TABLE `store_ratings`
  ADD PRIMARY KEY (`iStoreRatingId`),
  ADD KEY `store_ratings_istoreid_foreign` (`iStoreId`),
  ADD KEY `store_ratings_istoreorderid_foreign` (`iStoreOrderId`),
  ADD KEY `store_ratings_ireviewbyuserid_foreign` (`iReviewByUserId`);

--
-- Indexes for table `sub_order_master`
--
ALTER TABLE `sub_order_master`
  ADD PRIMARY KEY (`iSubOrderId`),
  ADD KEY `sub_order_master_iorderid_foreign` (`iOrderId`),
  ADD KEY `sub_order_master_istoreid_foreign` (`iStoreId`),
  ADD KEY `sub_order_master_iorderallocationid_foreign` (`iOrderAllocationId`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tests_email_unique` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iUserId`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`iUserNotificationId`),
  ADD KEY `user_notification_ireceivebyuserid_foreign` (`iReceiveByUserId`),
  ADD KEY `user_notification_iorderid_foreign` (`iOrderId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `iCategoryId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `iCMSPageId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contact_us_reason`
--
ALTER TABLE `contact_us_reason`
  MODIFY `iContactReasonId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contact_us_request`
--
ALTER TABLE `contact_us_request`
  MODIFY `iRequestId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  MODIFY `iCustomerAddressId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `decline_reason`
--
ALTER TABLE `decline_reason`
  MODIFY `iDeclineReasonId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `device_details`
--
ALTER TABLE `device_details`
  MODIFY `iDeviceId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `driver_declined_order`
--
ALTER TABLE `driver_declined_order`
  MODIFY `iDeclinedOrderId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver_details`
--
ALTER TABLE `driver_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `driver_ratings`
--
ALTER TABLE `driver_ratings`
  MODIFY `iDriverRatingId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `driver_running_trips`
--
ALTER TABLE `driver_running_trips`
  MODIFY `iDriverRunningTripId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `iQuestionId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `ordered_products`
--
ALTER TABLE `ordered_products`
  MODIFY `iOrderedProductId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `order_allocation`
--
ALTER TABLE `order_allocation`
  MODIFY `iOrderAllocationId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `order_master`
--
ALTER TABLE `order_master`
  MODIFY `iOrderId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `iProductImageId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_liked`
--
ALTER TABLE `product_liked`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `product_ratings`
--
ALTER TABLE `product_ratings`
  MODIFY `iProductRatingId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `promo_code`
--
ALTER TABLE `promo_code`
  MODIFY `iPromoCodeId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `recent_search`
--
ALTER TABLE `recent_search`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `iSettingsId` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `iStoreId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `store_categories`
--
ALTER TABLE `store_categories`
  MODIFY `iStoreCategoriesId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `store_declined_order`
--
ALTER TABLE `store_declined_order`
  MODIFY `iStoreDeclinedOrder` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `store_images`
--
ALTER TABLE `store_images`
  MODIFY `iStoreImageId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `store_orders`
--
ALTER TABLE `store_orders`
  MODIFY `iStoreOrderId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `store_ratings`
--
ALTER TABLE `store_ratings`
  MODIFY `iStoreRatingId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sub_order_master`
--
ALTER TABLE `sub_order_master`
  MODIFY `iSubOrderId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iUserId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `iUserNotificationId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contact_us_request`
--
ALTER TABLE `contact_us_request`
  ADD CONSTRAINT `contact_us_request_icontactreasonid_foreign` FOREIGN KEY (`iContactReasonId`) REFERENCES `contact_us_reason` (`iContactReasonId`) ON DELETE CASCADE,
  ADD CONSTRAINT `contact_us_request_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD CONSTRAINT `customer_addresses_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `driver_declined_order`
--
ALTER TABLE `driver_declined_order`
  ADD CONSTRAINT `driver_declined_order_iOrderAllocationId_foreign` FOREIGN KEY (`iOrderAllocationId`) REFERENCES `order_allocation` (`iOrderAllocationId`) ON DELETE CASCADE,
  ADD CONSTRAINT `driver_declined_order_ideclinereasonid_foreign` FOREIGN KEY (`iDeclineReasonId`) REFERENCES `decline_reason` (`iDeclineReasonId`) ON DELETE CASCADE,
  ADD CONSTRAINT `driver_declined_order_idriverid_foreign` FOREIGN KEY (`iDriverId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `driver_details`
--
ALTER TABLE `driver_details`
  ADD CONSTRAINT `driver_details_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `driver_ratings`
--
ALTER TABLE `driver_ratings`
  ADD CONSTRAINT `driver_ratings_idriverid_foreign` FOREIGN KEY (`iDriverId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE,
  ADD CONSTRAINT `driver_ratings_iorderallocationid_foreign` FOREIGN KEY (`iOrderAllocationId`) REFERENCES `order_allocation` (`iOrderAllocationId`) ON DELETE CASCADE,
  ADD CONSTRAINT `driver_ratings_ireviewbyuserid_foreign` FOREIGN KEY (`iReviewByUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `driver_running_trips`
--
ALTER TABLE `driver_running_trips`
  ADD CONSTRAINT `driver_running_trips_idriverid_foreign` FOREIGN KEY (`iDriverId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE,
  ADD CONSTRAINT `driver_running_trips_isuborderid_foreign` FOREIGN KEY (`iSubOrderId`) REFERENCES `sub_order_master` (`iSubOrderId`) ON DELETE CASCADE;

--
-- Constraints for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD CONSTRAINT `ordered_products_iorderid_foreign` FOREIGN KEY (`iOrderId`) REFERENCES `order_master` (`iOrderId`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordered_products_iproductid_foreign` FOREIGN KEY (`iProductId`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordered_products_iproductvariantid_foreign` FOREIGN KEY (`iProductVariantId`) REFERENCES `product_variants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordered_products_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE;

--
-- Constraints for table `order_allocation`
--
ALTER TABLE `order_allocation`
  ADD CONSTRAINT `order_allocation_icanceledbyuserid_foreign` FOREIGN KEY (`iCanceledByUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_allocation_idriverid_foreign` FOREIGN KEY (`iDriverId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_allocation_isuborderid_foreign` FOREIGN KEY (`iSubOrderId`) REFERENCES `sub_order_master` (`iSubOrderId`) ON DELETE CASCADE;

--
-- Constraints for table `order_master`
--
ALTER TABLE `order_master`
  ADD CONSTRAINT `order_master_ipromocodeid_foreign` FOREIGN KEY (`iPromoCodeId`) REFERENCES `promo_code` (`iPromoCodeId`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_master_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_icategoryid_foreign` FOREIGN KEY (`iCategoryId`) REFERENCES `categories` (`iCategoryId`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_iproductid_foreign` FOREIGN KEY (`iProductId`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_liked`
--
ALTER TABLE `product_liked`
  ADD CONSTRAINT `product_liked_iproductid_foreign` FOREIGN KEY (`iProductId`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_liked_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `product_ratings`
--
ALTER TABLE `product_ratings`
  ADD CONSTRAINT `product_ratings_iproductid_foreign` FOREIGN KEY (`iProductId`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_ratings_ireviewbyuserid_foreign` FOREIGN KEY (`iReviewByUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_ratings_isuborderid_foreign` FOREIGN KEY (`iSubOrderId`) REFERENCES `sub_order_master` (`iSubOrderId`) ON DELETE CASCADE;

--
-- Constraints for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD CONSTRAINT `product_variants_iproductid_foreign` FOREIGN KEY (`iProductId`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recent_search`
--
ALTER TABLE `recent_search`
  ADD CONSTRAINT `recent_search_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `store_iuserid_foreign` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;

--
-- Constraints for table `store_categories`
--
ALTER TABLE `store_categories`
  ADD CONSTRAINT `store_categories_icategoryid_foreign` FOREIGN KEY (`iCategoryId`) REFERENCES `categories` (`iCategoryId`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_categories_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE;

--
-- Constraints for table `store_declined_order`
--
ALTER TABLE `store_declined_order`
  ADD CONSTRAINT `store_declined_order_ideclinereasonid_foreign` FOREIGN KEY (`iDeclineReasonId`) REFERENCES `decline_reason` (`iDeclineReasonId`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_declined_order_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_declined_order_istoreorderid_foreign` FOREIGN KEY (`iStoreOrderId`) REFERENCES `store_orders` (`iStoreOrderId`) ON DELETE CASCADE;

--
-- Constraints for table `store_images`
--
ALTER TABLE `store_images`
  ADD CONSTRAINT `store_images_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE;

--
-- Constraints for table `store_orders`
--
ALTER TABLE `store_orders`
  ADD CONSTRAINT `store_orders_iorderallocationid_foreign` FOREIGN KEY (`iOrderAllocationId`) REFERENCES `order_allocation` (`iOrderAllocationId`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_orders_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_orders_isuborderid_foreign` FOREIGN KEY (`iSubOrderId`) REFERENCES `sub_order_master` (`iSubOrderId`) ON DELETE CASCADE;

--
-- Constraints for table `store_ratings`
--
ALTER TABLE `store_ratings`
  ADD CONSTRAINT `store_ratings_ireviewbyuserid_foreign` FOREIGN KEY (`iReviewByUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_ratings_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_ratings_istoreorderid_foreign` FOREIGN KEY (`iStoreOrderId`) REFERENCES `store_orders` (`iStoreOrderId`) ON DELETE CASCADE;

--
-- Constraints for table `sub_order_master`
--
ALTER TABLE `sub_order_master`
  ADD CONSTRAINT `sub_order_master_iorderallocationid_foreign` FOREIGN KEY (`iOrderAllocationId`) REFERENCES `order_allocation` (`iOrderAllocationId`) ON DELETE CASCADE,
  ADD CONSTRAINT `sub_order_master_iorderid_foreign` FOREIGN KEY (`iOrderId`) REFERENCES `order_master` (`iOrderId`) ON DELETE CASCADE,
  ADD CONSTRAINT `sub_order_master_istoreid_foreign` FOREIGN KEY (`iStoreId`) REFERENCES `store` (`iStoreId`) ON DELETE CASCADE;

--
-- Constraints for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD CONSTRAINT `user_notification_iorderid_foreign` FOREIGN KEY (`iOrderId`) REFERENCES `order_master` (`iOrderId`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_notification_ireceivebyuserid_foreign` FOREIGN KEY (`iReceiveByUserId`) REFERENCES `users` (`iUserId`) ON DELETE CASCADE;
--
-- Database: `jigar`
--
CREATE DATABASE IF NOT EXISTS `jigar` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `jigar`;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `name`, `email`, `password`) VALUES
(1, 'Jigar', 'jigar@gmail.com', '$2y$10$Fo2U8SkSjEPkGPhj5dARjeWXttmo16GrPOz0KM.BB2Nwe3qMLrRHC');

-- --------------------------------------------------------

--
-- Table structure for table `tblcategory`
--

CREATE TABLE `tblcategory` (
  `cid` int(15) NOT NULL,
  `cname` varchar(50) NOT NULL,
  `corder` int(11) NOT NULL,
  `cstatus` enum('active','inactive') NOT NULL,
  `cimage` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblcategory`
--

INSERT INTO `tblcategory` (`cid`, `cname`, `corder`, `cstatus`, `cimage`, `created_at`, `updated_at`) VALUES
(98, 'zxcvvbbbb', 1, 'inactive', '1612782022.png', '2021-02-08 16:30:22', '2021-02-08 12:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `tblproduct`
--

CREATE TABLE `tblproduct` (
  `pid` int(15) NOT NULL,
  `pname` varchar(50) NOT NULL,
  `cname` int(15) NOT NULL,
  `productcode` varchar(250) NOT NULL,
  `price` int(50) NOT NULL,
  `saleprice` int(50) NOT NULL,
  `quantity` int(50) NOT NULL,
  `porder` int(50) NOT NULL,
  `pstatus` enum('active','inactive') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblproduct`
--

INSERT INTO `tblproduct` (`pid`, `pname`, `cname`, `productcode`, `price`, `saleprice`, `quantity`, `porder`, `pstatus`, `created_at`, `updated_at`) VALUES
(34, 'big dream', 89, '4210', 5, 5, 5, 55, 'active', '2021-01-29 12:49:18', '2021-01-29 12:49:18'),
(52, 'server-product', 89, '1978', 2, 5, 5, 5, 'active', '2021-02-08 09:59:00', '2021-02-08 09:59:00'),
(53, 'fsdfsfsdfsdf', 101, '3538', 6, 6, 6, 6, 'inactive', '2021-02-08 16:33:44', '2021-02-08 16:33:44'),
(54, 'wefwefw', 101, '1444', 6, 55, 9, 6, 'active', '2021-02-08 16:34:37', '2021-02-08 16:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `tblproduct_image`
--

CREATE TABLE `tblproduct_image` (
  `imgid` int(15) NOT NULL,
  `productid` int(15) NOT NULL,
  `imagename` varchar(250) NOT NULL,
  `istatus` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblproduct_image`
--

INSERT INTO `tblproduct_image` (`imgid`, `productid`, `imagename`, `istatus`) VALUES
(169, 34, 'reszing-images-for-the-web-banner.jpg', 'inactive'),
(170, 34, 'Marmolada-Glacier-Colfosco-Italy-hd-wallpaper-by-vincentiu-solomon-iltwmt_0.jpg', 'inactive'),
(171, 34, 'Screenshot from 2020-10-26 14-11-01.png', 'inactive'),
(172, 34, 'Screenshot from 2020-10-26 14-11-29.png', 'inactive'),
(173, 34, 'Screenshot from 2021-01-04 09-54-15.png', 'inactive'),
(174, 34, 'samuele-errico-piccarini-FMbWFDiVRPs-unsplash.jpg', 'active'),
(315, 52, '2.png', 'inactive'),
(316, 52, '3.png', 'inactive'),
(317, 52, '4.png', 'inactive'),
(318, 52, '5.png', 'inactive'),
(319, 52, '6.png', 'inactive'),
(320, 52, '7.png', 'inactive'),
(321, 52, '8.png', 'active'),
(322, 52, '9.png', 'inactive'),
(323, 52, '10.png', 'inactive'),
(324, 52, '12.png', 'inactive'),
(325, 53, '2.png', 'inactive'),
(326, 53, '3.png', 'inactive'),
(327, 53, '4.png', 'inactive'),
(328, 53, '5.png', 'inactive'),
(329, 53, '6.png', 'inactive'),
(330, 53, '7.png', 'active'),
(331, 53, '8.png', 'inactive'),
(332, 54, '34.png', 'inactive'),
(333, 54, '35.png', 'inactive'),
(334, 54, '36.png', 'inactive'),
(335, 54, '37.png', 'active'),
(336, 54, '38.png', 'inactive');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(250) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `email`) VALUES
(1, 'Abc ', 'abc@gmail.com'),
(2, 'Jigar Bhesaniya', 'jigar@gmail.com'),
(3, 'Sahil Crest', 'w@gmail.com'),
(5, 'harley', 'harleysmith.cis@gmail.com'),
(6, 'priya', 'p@gmail.com'),
(7, 'harsh', 'h@gmail.com'),
(8, 'hardik', 'hardik');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcategory`
--
ALTER TABLE `tblcategory`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tblproduct`
--
ALTER TABLE `tblproduct`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `tblproduct_image`
--
ALTER TABLE `tblproduct_image`
  ADD PRIMARY KEY (`imgid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblcategory`
--
ALTER TABLE `tblcategory`
  MODIFY `cid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `tblproduct`
--
ALTER TABLE `tblproduct`
  MODIFY `pid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tblproduct_image`
--
ALTER TABLE `tblproduct_image`
  MODIFY `imgid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=337;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Database: `laravel`
--
CREATE DATABASE IF NOT EXISTS `laravel` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `laravel`;

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `c_name`, `c_file`, `order`, `status`, `created_at`, `updated_at`) VALUES
(15, 'jayrj', 'hrdik.jpg', 10, 'Inactive', '2021-03-05 04:21:37', '2021-03-08 22:46:29'),
(17, 'khas', '0', 10, 'Active', '2021-03-05 04:24:13', '2021-03-05 04:24:13'),
(18, 'khas1', '0', 10, 'Active', '2021-03-05 04:25:10', '2021-03-05 04:25:10'),
(19, 'hardik kanzariya botad', 'hrdik.jpg', 25, 'Active', '2021-03-05 07:38:05', '2021-03-05 07:38:05'),
(21, 'hardik kanzar', 'hrdik.jpg', 25, 'Active', '2021-03-05 07:39:00', '2021-03-05 07:39:00'),
(22, 'vishal parmar', 'hrdik.jpg', 10, 'Active', '2021-03-05 07:42:50', '2021-03-05 07:42:50'),
(23, 'yash', '0', 10, 'Active', '2021-03-08 03:06:33', '2021-03-08 03:06:33'),
(24, 'yashraj films', '0', 10, 'Active', '2021-03-08 07:29:45', '2021-03-08 07:29:45'),
(25, 'crest infotech', '0', 10, 'Active', '2021-03-08 22:07:54', '2021-03-08 22:07:54'),
(26, 'crest ahmedabad', '0', 10, 'Active', '2021-03-08 22:11:23', '2021-03-08 22:11:23'),
(28, 'crest botad', '0', 10, 'Active', '2021-03-08 22:15:31', '2021-03-08 22:15:31');

-- --------------------------------------------------------

--
-- Table structure for table `dttable`
--

CREATE TABLE `dttable` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `salary` int(11) NOT NULL,
  `age` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dttable`
--

INSERT INTO `dttable` (`id`, `name`, `salary`, `age`) VALUES
(1, 'hardik', 20000, 22),
(2, 'jaydip', 2000, 23),
(3, 'hardik', 20000, 22),
(4, 'jaydip', 2000, 23);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_02_10_063405_create_categorys_table', 2),
(4, '2021_02_12_065019_create_products_table', 3),
(5, '2021_02_12_072935_create_p_image_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `p_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_saleprice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `pname`, `cid`, `p_code`, `p_price`, `p_saleprice`, `p_quantity`, `p_order`, `status`, `created_at`, `updated_at`) VALUES
(2, 'jay', 6, '1613210910815', '2', '2', '2', '2', 'Active', '2021-02-13 04:38:30', '2021-02-13 04:38:30'),
(3, 'jay', 6, '16132109645151', '2', '2', '2', '2', 'Active', '2021-02-13 04:39:24', '2021-02-13 04:39:24'),
(4, 'ja', 4, '16132109882250', '1', '0', '2', '2', 'Inactive', '2021-02-13 04:39:48', '2021-02-13 04:39:48'),
(5, 'ja', 4, '16132109948880', '1', '0', '2', '2', 'Inactive', '2021-02-13 04:39:54', '2021-02-13 04:39:54'),
(7, '20', 8, '16132174239369', '20', '20', '20', '20', 'Active', '2021-02-13 06:27:03', '2021-02-13 06:27:03'),
(9, '20', 8, '1613217633293', '20', '20', '20', '20', 'Active', '2021-02-13 06:30:33', '2021-02-13 06:30:33'),
(10, '20', 8, '16132176513830', '20', '20', '20', '20', 'Active', '2021-02-13 06:30:51', '2021-02-13 06:30:51'),
(14, 'bapuji', 8, '16133953964942', '3', '3', '2', '2', 'Inactive', '2021-02-15 07:53:16', '2021-02-16 05:54:03'),
(16, 'aditya', 8, '16134682495445', '100', '100', '100', '100', 'Active', '2021-02-16 04:07:29', '2021-02-16 04:07:29'),
(19, 'jhanvi', 4, '16134683848158', '500', '500', '500', '500', 'Active', '2021-02-16 04:09:44', '2021-02-16 04:09:44'),
(20, 'disha', 8, '16134684161712', '600', '600', '600', '600', 'Active', '2021-02-16 04:10:16', '2021-02-16 04:10:16'),
(23, 'aditya', 6, '16139879772924', '20', '20', '20', '20', 'Inactive', '2021-02-22 04:29:37', '2021-02-22 04:29:37'),
(24, 'jaraj', 14, '16139986432257', '20', '20', '20', '20', 'Active', '2021-02-22 07:27:23', '2021-02-22 07:27:23'),
(25, 'jayralkd', 5, '16149299091260', '20', '10', '10', '10', 'Inactive', '2021-03-05 02:08:29', '2021-03-05 02:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `p_image`
--

CREATE TABLE `p_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` int(11) NOT NULL,
  `p_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `p_image`
--

INSERT INTO `p_image` (`id`, `p_id`, `p_img`, `status`) VALUES
(9, 1, '1613210622313-07.png', 'Active'),
(10, 2, '16132109111180-07.png', 'Inactive'),
(11, 3, '1613210965660-07.png', 'Inactive'),
(12, 4, '16132109881553-07.png', 'Inactive'),
(13, 5, '16132109951539-07.png', 'Inactive'),
(14, 6, '1613211121678-01.png', 'Active'),
(15, 6, '16132111211331-02.png', 'Inactive'),
(16, 6, '1613211121120-03.png', 'Inactive'),
(17, 6, '16132111211186-04.png', 'Inactive'),
(18, 6, '16132111221255-05.png', 'Inactive'),
(19, 6, '16132111221016-06.png', 'Inactive'),
(20, 6, '1613211122553-07.png', 'Inactive'),
(28, 14, '16133953961747-01.png', 'Inactive'),
(29, 14, '16133953961474-02.png', 'Active'),
(30, 14, '1613395396296-03.png', 'Inactive'),
(31, 14, '1613395396718-04.png', 'Inactive'),
(32, 14, '1613395397597-05.png', 'Inactive'),
(33, 14, '1613395397699-06.png', 'Inactive'),
(37, 14, '161345503878-free-fb-f1199-fashion-basket-original-imaf4ec7gamryhn6.jpeg', 'Inactive'),
(38, 16, '16134682491525-01.jpg', 'Active'),
(43, 19, '16134683841618-free-fb1-f1160-fashion-basket-original-imaff4gq2vdbtnbt.jpeg', 'Inactive'),
(44, 19, '1613468384117-free-fb1-f1160-fashion-basket-original-imaff4gqf6m8dh8f.jpeg', 'Inactive'),
(45, 19, '161346838461-free-fb1-f1160-fashion-basket-original-imaff4gqtchztsrc.jpeg', 'Active'),
(46, 20, '1613468416980-black1.jpg', 'Active'),
(47, 20, '1613468416791-black2.jpg', 'Inactive'),
(48, 20, '16134684161072-black3.jpg', 'Inactive'),
(56, 23, '16139879781691-01.png', 'Active'),
(57, 23, '16139879781037-02.png', 'Inactive'),
(58, 23, '16139879781769-03.png', 'Inactive'),
(59, 24, '16139986431774-06.png', 'Active'),
(60, 24, '16139986431346-07.png', 'Inactive'),
(61, 25, '16149299091688-01.png', 'Active'),
(62, 25, '16149299091118-02.png', 'Inactive'),
(63, 25, '16149299091605-03.png', 'Inactive'),
(64, 25, '16149299101270-04.png', 'Inactive'),
(65, 25, '16149299101103-05.png', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `tbperson`
--

CREATE TABLE `tbperson` (
  `id` int(11) NOT NULL COMMENT 'primary key',
  `name` varchar(255) NOT NULL COMMENT 'person name',
  `salary` double NOT NULL COMMENT 'person salary',
  `age` int(11) NOT NULL COMMENT 'person age'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='datatable demo table';

--
-- Dumping data for table `tbperson`
--

INSERT INTO `tbperson` (`id`, `name`, `salary`, `age`) VALUES
(1, 'Tiger Nixon', 320800, 61),
(2, 'Garrett Winters', 170750, 63),
(3, 'Ashton Cox', 86000, 66),
(4, 'Cedric Kelly', 433060, 22),
(5, 'Airi Satou', 162700, 33),
(6, 'Brielle Williamson', 372000, 61),
(7, 'Herrod Chandler', 137500, 59),
(8, 'Rhona Davidson', 327900, 55),
(9, 'Colleen Hurst', 205500, 39),
(10, 'Sonya Frost', 103600, 23),
(11, 'Jena Gaines', 90560, 30),
(12, 'Quinn Flynn', 342000, 22),
(13, 'Charde Marshall', 470600, 36),
(14, 'Haley Kennedy', 313500, 43),
(15, 'Tatyana Fitzpatrick', 385750, 19),
(16, 'Michael Silva', 198500, 66),
(17, 'Paul Byrd', 725000, 64),
(18, 'Gloria Little', 237500, 59),
(19, 'Bradley Greer', 132000, 41),
(20, 'Dai Rios', 217500, 35),
(21, 'Jenette Caldwell', 345000, 30),
(22, 'Yuri Berry', 675000, 40),
(23, 'Caesar Vance', 106450, 21),
(24, 'Doris Wilder', 85600, 23),
(25, 'Angelica Ramos', 1200000, 47),
(26, 'Gavin Joyce', 92575, 42),
(27, 'Jennifer Chang', 357650, 28),
(28, 'Brenden Wagner', 206850, 28),
(29, 'Fiona Green', 850000, 48),
(30, 'Shou Itou', 163000, 20),
(31, 'Michelle House', 95400, 37),
(32, 'Suki Burks', 114500, 53),
(33, 'Prescott Bartlett', 145000, 27),
(34, 'Gavin Cortez', 235500, 22),
(35, 'Martena Mccray', 324050, 46),
(36, 'Unity Butler', 85675, 47),
(37, 'Howard Hatfield', 164500, 51),
(38, 'Hope Fuentes', 109850, 41),
(39, 'Vivian Harrell', 452500, 62),
(40, 'Timothy Mooney', 136200, 37),
(41, 'Jackson Bradshaw', 645750, 65),
(42, 'Olivia Liang', 234500, 64),
(43, 'Bruno Nash', 163500, 38),
(44, 'Sakura Yamamoto', 139575, 37),
(45, 'Thor Walton', 98540, 61),
(46, 'Finn Camacho', 87500, 47),
(47, 'Serge Baldwin', 138575, 64),
(48, 'Zenaida Frank', 125250, 63),
(49, 'Zorita Serrano', 115000, 56),
(50, 'Jennifer Acosta', 75650, 43),
(51, 'Cara Stevens', 145600, 46),
(52, 'Hermione Butler', 356250, 47),
(53, 'Lael Greer', 103500, 21),
(54, 'Jonas Alexander', 86500, 30),
(55, 'Shad Decker', 183000, 51),
(56, 'Michael Bruce', 183000, 29),
(57, 'Donna Snider', 112000, 27);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `fullname`, `password`) VALUES
(1, 'hardik', '', 'kanzariya'),
(2, 'j@gmail.com', '', 'jay'),
(3, 'hardik@gmail.com', 'hardik', '$2y$10$pM/QvtP5rRrTYfR.2PYBQOKs0R6mC/B0O6GkqfQvNTeSJBcnk6t.i'),
(4, 'kanzariya@gmail.com', 'hardik', '$2y$10$ohu5gzjyvfMozMmTxy8ma.tZkS27STeUagfxqUMjMTOrRBoFpA7qi'),
(5, 'aakash@gmail.com', 'aakash', '$2y$10$b3qZbNT5gFyxhfoInWoPB.hyNC8sGLTqFPmUEyjJs8m.USQV49BNe'),
(6, 'a@gmail.com', 'aakash', '$2y$10$8d.dwh889sNPtEOKPUoPzuubqrXiUGsoVIt2MUcQ5kr0wjQGSrjbO'),
(7, 'ab@gmail.com', 'hardikk', '$2y$10$CzX8xrVZ8VsqiYQdJb5WluqIaQTjryaXo8yPGq/VXkQ8ITvhd7ho.'),
(8, 's@gmail.com', 'shahil', '$2y$10$lxvErR3.tKApWrA68rKM1O9KmpGEKs2Z22agOhVSZSH.vO5Hxjg/6'),
(9, 'n@gmail.com', 'nancy', '$2y$10$sopNjIfbNZXvpUVi7ExMCOFgTAJoo84Ep8TwZauvgH.gpF.36Ao6.'),
(10, 'b@gmail.com', 'b', '$2y$10$S5DeBnYLHZk8b8IO8dPfF.xC0OCB9D9APDd288foHmiT06iENKuIm'),
(11, 'hardikkk@gmail.com', 'hardikkk', '$2y$10$Hj1EaVXZ2FJ.uPbw0p0z7OoAS..56k7iAEb4ZXQc5ND1ddkaKYA7K'),
(12, 'jayraj@gmail.com', 'jayraj', '$2y$10$4iZZJR49igi3k4LP17bkG.nM6.DA2deQ1O/yd0cSFFI0m1o9PL0hu');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'c@gmail.com', 'c@gmail.com', NULL, '$2y$10$Dh/VdZZ367Kj0u7yjgPTuuqe8oq3nbsd7QvqFAwWyO8joLoDPnktG', NULL, NULL, NULL),
(2, 'raju', 'r@gmail.com', NULL, '$2y$10$qjjIIne4zceDuXJqLAwNveEIpjgZAB2ZKuYYQQHWNvae6lqj4bUCO', NULL, NULL, NULL),
(3, 'raju', 'r1@gmail.com', NULL, '$2y$10$xnJy/81xbe9D/HHyGYvagO2XgfOHP80NDk/6/1183JG2K0y7WGKku', NULL, NULL, NULL),
(4, 'hardik', 'hardik@gmail.com', NULL, '$2y$10$if3EuQYTb/P6s6MotiQKi.ZVVkXmWES9xznXR0bQanZYygqP0USY6', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorys_c_name_unique` (`c_name`);

--
-- Indexes for table `dttable`
--
ALTER TABLE `dttable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_image`
--
ALTER TABLE `p_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbperson`
--
ALTER TABLE `tbperson`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `dttable`
--
ALTER TABLE `dttable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `p_image`
--
ALTER TABLE `p_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `tbperson`
--
ALTER TABLE `tbperson`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key', AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(10) UNSIGNED NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin DEFAULT NULL,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp(),
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"jigar\",\"table\":\"user\"},{\"db\":\"jigar\",\"table\":\"tblproduct\"},{\"db\":\"jigar\",\"table\":\"tblcategory\"},{\"db\":\"choward\",\"table\":\"categories\"},{\"db\":\"laravel\",\"table\":\"categorys\"},{\"db\":\"choward\",\"table\":\"cat\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT 0,
  `x` float UNSIGNED NOT NULL DEFAULT 0,
  `y` float UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin DEFAULT NULL,
  `data_sql` longtext COLLATE utf8_bin DEFAULT NULL,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2021-03-15 15:21:54', '{\"Console\\/Mode\":\"collapse\",\"ThemeDefault\":\"pmahomme\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
