<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post( '/v1/dev-test', 'Api\V1\DevController@DevTest');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => '/v1/customer', 'middleware' => ['localization']], function ( ) {

    Route::get('/users', 'Api\V1\CustomerController@index')->name('getUserList');
    Route::post('/signup', 'Api\V1\CustomerController@signup')->name('signup');
    Route::post('/signin', 'Api\V1\CustomerController@signin')->name('userSignIn');
    Route::post('/social-signin', 'Api\V1\CustomerController@socialSignin')->name('socialSignin');
    Route::get('/sign-out', 'Api\V1\CustomerController@signout')->name('signout');
    Route::post('/forgot-password', 'Api\V1\CustomerController@forgotPassword')->name('forgotPassword');
    Route::post('/verifyOtp', 'Api\V1\CustomerController@verifyOtp')->name('verifyOtp');
    Route::post('/requestOtp', 'Api\V1\CustomerController@requestOtp')->name('requestOtp');
    Route::post('/set-profile', 'Api\V1\CustomerController@setProfile')->name('setProfile');
    Route::post('/home', 'Api\V1\CustomerController@Home')->name('customerHome');
    Route::post('/near-by-stores', 'Api\V1\CustomerController@nearByStores')->name('customerNearByStores');
    Route::post('/category-products', 'Api\V1\CustomerController@categoryProducts')->name('customercategoryProducts');
    Route::post('/category-products-filters', 'Api\V1\CustomerController@categoryProductsFilters')->name('customercategoryProducts');
    Route::post('/category-list', 'Api\V1\CustomerController@categoryList')->name('categoryList');
    Route::post('/store-list', 'Api\V1\CustomerController@storeList')->name('storeList');
    Route::get('/get-product-details/{iProductId}', 'Api\V1\CustomerController@getProductDetails')->name('getProductDetails');
    Route::post('/liked-product', 'Api\V1\CustomerController@likedProducts')->name('likedProducts');
    Route::get('/get-store-products/{iStoreId}', 'Api\V1\CustomerController@getStoreProducts')->name('getStoreProducts');
    Route::get('/get-store-details/{iStoreId}', 'Api\V1\CustomerController@getStoreDetails')->name('getStoreDetails');
    Route::post('/promocode-list', 'Api\V1\CustomerController@getPromoList')->name('getPromoList');
    Route::post('/add-promocode', 'Api\V1\CustomerController@addPromocode')->name('addPromocode');
    Route::post('/remove-promocode', 'Api\V1\CustomerController@removePromocode')->name('removePromocode');
    Route::post('/add-address', 'Api\V1\CustomerController@addAddress')->name('addAddress');
    Route::post('/edit-address', 'Api\V1\CustomerController@editAddress')->name('editAddress');
    Route::post('/remove-address', 'Api\V1\CustomerController@removeAddress')->name('removeAddress');
    Route::get('/list-address', 'Api\V1\CustomerController@listAddress')->name('listAddress');
    Route::post('/add-to-cart', 'Api\V1\CustomerController@addToCart')->name('addToCart');
    Route::get('/view-cart', 'Api\V1\CustomerController@viewCart')->name('viewCart');
    Route::post('/apply-promocode', 'Api\V1\CustomerController@applyPromoCode')->name('applyPromoCode');
    Route::post('/checkout', 'Api\V1\CustomerController@checkout')->name('checkout');
    Route::post('/remove-promocode', 'Api\V1\CustomerController@removePromoCode')->name('removePromoCode');
    Route::post('/place-order', 'Api\V1\CustomerController@placeOrder')->name('placeOrder');
    Route::post('/my-orders', 'Api\V1\CustomerController@myOrders')->name('myOrders');
    Route::get('/get-checkout-details/{iOrderId}', 'Api\V1\CustomerController@getCheckoutDetails')->name('getCheckoutDetails');
    Route::post('/global-search', 'Api\V1\CustomerController@globalSearch')->name('globalSearch');
    Route::get('/get-recent-search', 'Api\V1\CustomerController@getRecentSearch')->name('getRecentSearch');
    Route::post('/get-favorite-products', 'Api\V1\CustomerController@getFavoriteProducts')->name('getFavoriteProducts');
    Route::post('/change-password', 'Api\V1\CustomerController@changePassword')->name('changePassword');
    Route::get('/get-cms-pages', 'Api\V1\CustomerController@getCmsPages')->name('getCmsPages');
    Route::get('/get-contact-us-reason', 'Api\V1\CustomerController@getContactUsReason')->name('getContactUsReason');
    Route::post('/post-contact-us-request', 'Api\V1\CustomerController@postContactUsRequest')->name('postContactUsRequest');
    

});

Route::group(['prefix' => '/v1/driver', 'middleware' => ['localization']], function ( ) {
    Route::post('/signup', 'Api\V1\DriverController@signup')->name('signup');
    Route::post('/signin', 'Api\V1\DriverController@signInDriver')->name('userSignIn');
    Route::post('/verifyOtp', 'Api\V1\DriverController@verifyOtp')->name('verifyOtp');
    Route::post('/requestOtp', 'Api\V1\DriverController@requestOtp')->name('requestOtp');
    Route::post('/edit-driver-details', 'Api\V1\DriverController@setProfile')->name('setProfile');
    Route::post('/forgot-password', 'Api\V1\DriverController@forgotPassword')->name('forgotPassword');
    Route::get('/get-cms-pages', 'Api\V1\DriverController@getCmsPages')->name('getCmsPages');
    Route::get('/get-faq', 'Api\V1\DriverController@getFAQ')->name('getFAQ');
    
    Route::get('/get-decline-reason/{tiReasonType}', 'Api\V1\DriverController@getDeclineReason')->name('getDeclineReason');
    Route::post('/cancel-decline-order', 'Api\V1\DriverController@cancelDeclineOrder')->name('cancelDeclineOrder');
    Route::post('/home', 'Api\V1\DriverController@Home')->name('driverHome');
    Route::post('/change-order-status', 'Api\V1\DriverController@ChangeOrderStatus')->name('driverChangeOrderStatus');
    
});

Route::post('/v1/category/catInsert', 'Api\V1\CategoryController@catInsert')->name('catInsert');
Route::get('/v1/category/catFind', 'Api\V1\CategoryController@catFind')->name('catFind');
Route::post('/v1/category/catUpdate', 'Api\V1\CategoryController@catUpdate')->name('catUpdate');
Route::get('/v1/category/catDelete', 'Api\V1\CategoryController@catDelete')->name('catDelete');
Route::post('/v1/category/allCategoryRecords', 'Api\V1\CategoryController@allCategoryRecords')->name('allCategoryRecords');
