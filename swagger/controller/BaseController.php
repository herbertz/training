<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DeviceDetail;
use App\Models\User;
/**
* @SWG\Swagger(
* schemes={"http","https"},
* host=API_HOST,
* basePath=API_PATH,
* produces={"application/json","application/xml"},
* consumes={"application/x-www-form-urlencoded"},
* @SWG\Info(
* title=APP_NAME,
* version=API_VERSION,
* description="Private Key : - Hg1dhgKS1A1MT0AI5Pf5ydf7r6vlwgjUfa9s",
* termsOfService="sha256",
* @SWG\Contact(
* name=APP_TEAM,
* email=SUPPORT_EMAIL
* ),
* ),
* )
*/

class BaseController extends Controller
{
    public $headers;
    public $response;
    public function __construct(Request $request)
    {
        $this->headers = getallheaders();
        $this->validateToken($request);
        
    }
    
    private function validateToken($request) {
        
        if (empty($this->headers['token'])) {
            $this->response = ['responseCode'=>config('constant.BAD_REQUEST'),'responseMessage'=> trans('response.token_error')];
        } else if (empty($this->headers['nonce'])) {
            $this->response = ['responseCode'=>config('constant.BAD_REQUEST'),'responseMessage'=> trans('response.nonce_error')];
        } else if (empty($this->headers['timestamp'])) {
            $this->response = ['responseCode'=>config('constant.BAD_REQUEST'),'responseMessage'=> trans('response.timestamp_error')];
        } else {
            $hash_str = "nonce=" . $this->headers['nonce'] . "&timestamp=" . $this->headers['timestamp'] . "|" . config('constant.SECRET_KEY');
            $sig = hash_hmac(env('HASH_KEY'),$hash_str,config('constant.PRIVATE_KEY'));
            $this->headers['token'] = $sig;
            if ($sig !== $this->headers['token']) {
                $this->response = ['responseCode'=>config('constant.BAD_REQUEST'),'responseMessage'=> trans('response.token_invalid_error')];
            } else {
                $this->response = ['responseCode'=>config('constant.OK'),'responseMessage'=> trans('response.token_valid')];
            }
        }
    }

    public function checkAuthorization($request=[]) {

        $authorization = $this->headers['vAccessToken'];
        if (preg_match('/Bearer\s(\S+)/', $authorization, $matches)) {
            
            // if( !empty($matches[1]) && $matches[1] == 'guest' ){
            //     /* GUEST USER */
            //     if( empty($request['vDeviceToken']) ){
            //         $this->response = ['responseCode'=>config('constant.BAD_REQUEST'),'responseMessage'=> trans('response.device_token_required')];
            //     }else{
            //         $deviceModel = DeviceDetail::where(['vDeviceToken' => $request['vDeviceToken']])->first();
            //         if( empty($deviceModel) ){

            //             $userModel = new User();
            //             $userModel->iCreatedAt = $userModel->iUpdatedAt = time();
            //             $userModel->tiIsSocialLogin = 0;
            //             $userModel->enStatus = User::STATUS_INACTIVE;
    
            //             if ($userModel->save()) {
            //                 $deviceModel   = new DeviceDetail();
            //                 $deviceModel->iUserId      = $userModel->iUserId;
            //                 $deviceModel->vDeviceToken = $request['vDeviceToken'];
            //                 $deviceModel->tiDeviceType = $request['tiDeviceType'];
            //                 $deviceModel->iCreatedAt   = $deviceModel->iUpdatedAt = time();
            //                 $deviceModel->save();
            //             }
            //         }
            //         $this->response = ['responseCode'=>config('constant.OK'),'responseMessage'=> trans('response.guestLoginSuccess'),'iUserId' => $deviceModel->iUserId];
            //     }
                
            // }else{
                /* NORMAL USER */
                $modelDevice = DeviceDetail::where(['vAccessToken' => $matches[1]]);
                $modelDevice->whereHas('users', function ($q) {
                    // $q->where('enStatus','Active');
                    $q->where('enIsDeleted', 'No');
                });
                $modelDevice = $modelDevice->first();
                // echo'<pre>'; print_r($modelDevice); exit;
                if (empty($modelDevice)) {
                    $this->response = ['responseCode'=>config('constant.INVALID_AUTHORIZATION'),'responseMessage'=> trans('response.authorization_fail')];
                } else {
                    
                    $this->response = ['responseCode'=>config('constant.OK'),'responseMessage'=> trans('response.success'),'iUserId' => $modelDevice->iUserId];

                }
            // }
        } else {
            
            $this->response = ['responseCode'=>config('constant.INVALID_AUTHORIZATION'),'responseMessage'=> trans('response.authorization_fail')];
        }
    }
}
