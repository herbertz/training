<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CategoryCrud;
use App\Models\User;
use App\Http\Controllers\Api\V1\BaseController;

class CategoryController extends BaseController
{
    /**
     *  @SWG\Post(
     *      path="/category/catInsert",
     *      tags={"Category"},
     *      summary="Category Insert",
     *      operationId="catInsert",
     *      produces={"application/json"},
     *      consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="57f0cb275421171c95294764476df124ebd0b3ccebca603cab5cec795c480238",
     *      ),
     *      @SWG\Parameter(
     *          name="c_name",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          format="string",
     *          description="Category Name",
     *          default="First Category",
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="status",
     *          required=true,
     *          type="string",
     *          description="Category Status Active = 1  and Inactive = 2 ",
     *         	format="string",
     *           enum={
     *               "1",
     *               "2"
     *           },
     *          default="1",
     *     ),
     *     @SWG\Parameter(
     *         description="Category Order",
     *         in="formData",
     *         name="order",
     *         required=true,
     *         type="integer",
     *         format="int32",
     *         default="0",
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "category success response",
     *         @SWG\Schema(ref="#/definitions/CategoryCrudResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 200,
     *                      "responseMessage": "category_insert_success",
     *                  }
     *         }
     *     ),
     *     @SWG\Response(
     *         response = 201,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */

    public function catInsert(Request $request)
    {

        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->response = CategoryCrud::addNewCategory($request);
        }
        return $this->response;
    }

    /**
     *  @SWG\Get(  
     *      path="/category/catFind",
     *      tags={"Category"},
     *      summary="Category Find ",
     *      operationId="catFind",
     *     @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="57f0cb275421171c95294764476df124ebd0b3ccebca603cab5cec795c480238",
     *      ),
     *      @SWG\Parameter(
     *          name="category",
     *          in="header",
     *          required=true,
     *          type="string",
     *          format="string",
     *          default="hardik",
     *     ), 
     *     @SWG\Response(
     *         response = 200,
     *         description = "category success response",
     *         @SWG\Schema(ref="#/definitions/CategoryCrudResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 200,
     *                      "responseMessage": "Category Find successfully",
     *                  }
     *         }
     *     ),
     *     @SWG\Response(
     *         response = 201,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function catFind(Request $request)
    {

        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->response = CategoryCrud::catFindAllRecord($request);
        }
        return $this->response;
    }

    /**
     *  @SWG\Post(
     *      path="/category/catUpdate",
     *      tags={"Category"},
     *      summary="Category Update",
     *      operationId="catUpdate",
     *      produces={"application/json"},
     *      consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="57f0cb275421171c95294764476df124ebd0b3ccebca603cab5cec795c480238",
     *      ),
     *     @SWG\Parameter(
     *         description="Category Id",
     *         in="formData",
     *         name="id",
     *         required=true,
     *         type="integer",
     *         format="int32",
     *         default="1",
     *     ),
     *      @SWG\Parameter(
     *          name="c_name",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          format="string",
     *          description="Category Name",
     *          default="First Category",
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="status",
     *          required=true,
     *          type="string",
     *          description="Category Status Active = 1  and Inactive = 2 ",
     *         	format="string",
     *           enum={
     *               "1",
     *               "2"
     *           },
     *          default="1",
     *     ),
     *     @SWG\Parameter(
     *         description="Category Order",
     *         in="formData",
     *         name="order",
     *         required=true,
     *         type="integer",
     *         format="int32",
     *         default="0",
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "category success response",
     *         @SWG\Schema(ref="#/definitions/CategoryCrudResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 200,
     *                      "responseMessage": "category_insert_success",
     *                  }
     *         }
     *     ),
     *     @SWG\Response(
     *         response = 201,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */

    public function catUpdate(Request $request)
    {

        if ($this->response['responseCode'] == config('constant.OK')) {
            $catEditId = $request->id;
            $this->response = CategoryCrud::catUpdateById($request);
        }
        return $this->response;
    }

    /**
     *  @SWG\Get(
     *      path="/category/catDelete",
     *      tags={"Category"},
     *      summary="Category Delete",
     *      operationId="catDeleteById",
     *     @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="57f0cb275421171c95294764476df124ebd0b3ccebca603cab5cec795c480238",
     *      ),
     *     @SWG\Parameter(
     *         description="Category Id",
     *         in="header",
     *         name="id",
     *         required=true,
     *         type="integer",
     *         format="int32",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "category success response",
     *         @SWG\Schema(ref="#/definitions/CategoryCrudResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 200,
     *                      "responseMessage": "Category Deleted successfully",
     *                  }
     *         }
     *     ),
     *     @SWG\Response(
     *         response = 201,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function catDelete(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->response = CategoryCrud::catDeleteById($request);
        }
        return $this->response;
    }
    /**
     *  @SWG\Post(
     *      path="/category/allCategoryRecords",
     *      tags={"Category"},
     *      summary="Category Records with pagignation",
     *      operationId="allCategoryRecords",
     *      produces={"application/json"},
     *      consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="57f0cb275421171c95294764476df124ebd0b3ccebca603cab5cec795c480238",
     *      ),
     *     @SWG\Parameter(
     *         description="pagination Number",
     *         in="formData",
     *         name="pageno",
     *         required=true,
     *         type="integer",
     *         format="int32",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "category success response",
     *         @SWG\Schema(ref="#/definitions/CategoryCrudResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 200,
     *                      "responseMessage": "category_insert_success",
     *                  }
     *         }
     *     ),
     *     @SWG\Response(
     *         response = 201,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */

    public function allCategoryRecords(Request $request)
    {

        if ($this->response['responseCode'] == config('constant.OK')) {
            
            $this->response = CategoryCrud::allCategoryRecordsWithPagination($request);

        }
        return $this->response;
    }


}
