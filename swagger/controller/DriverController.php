<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\V1\BaseController;
use App\Models\CmsPages;
use App\Models\DeclineReason;
use App\Models\DriverDeclinedOrders;
use App\Models\FAQ;
use App\Models\User;
use App\Models\OrderAllocation;

class DriverController extends BaseController
{
    /**
     *  @SWG\Post(
     *      path="/driver/signup",
     *      tags={"Driver"},
     *      summary="SignUp",
     *      operationId="signUp",
     *      produces={"application/json"},
     *      consumes={"application/x-www-form-urlencoded"},
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="57f0cb275421171c95294764476df124ebd0b3ccebca603cab5cec795c480238",
     *      ),
     *      @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Parameter(
     *          name="vFullName",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="First Name of user",
     *          default="Archita Rathod",
     *     ),
     *     @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Email Id of user",
     *          default="architar.spaceo@gmail.com",
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Password of user",
     *          default="12345678",
     *     ),
     *     @SWG\Parameter(
     *          name="vISDCode",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Mobile STD Code",
     *          default="+91",
     *     ),
     *     @SWG\Parameter(
     *          name="vMobileNumber",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Mobile Number",
     *          default="9228723322",
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="remember_token",
     *          required=false,
     *          type="string",
     *          description="Accept Terms & Condition ? (1=Accept, 2=Reject)",
     *         	format="string",
     *           enum={
     *               "1",
     *               "2"
     *           },
     *          default="1",
     *     ),
     *     @SWG\Parameter(
     *         description="Device Token of device",
     *         in="formData",
     *         name="vDeviceToken",
     *         required=true,
     *         type="string",
     *         format="string",
     *         default="dfdf",
     *     ),
     *     @SWG\Parameter(
     *         description="DeviceType  1=>Android 2=> IOS 0=> Web",
     *         in="formData",
     *         name="tiDeviceType",
     *         required=true,
     *          type="integer",
     *          format="int32",
     *         default="0",
     *     ),
     *     @SWG\Parameter(
     *          name="vDeviceName",
     *          in="formData",
     *          description="Device Name of device",
     *          required=true,
     *          type="string",
     *          format="string",
     *          default="iPhone X",
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Signup success response",
     *         @SWG\Schema(ref="#/definitions/DriverSignUpResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 200,
     *                      "responseMessage": "signup_success",
     *                  }
     *         }
     *     ),
     *     @SWG\Response(
     *         response = 201,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function signup(Request $request)
    {
        
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->response = User::signUpDriver($request);
        }
        return $this->response;
    }

    /**
     *  @SWG\Post(
     *      path="/driver/requestOtp",
     *      tags={"Driver"},
     *      summary="request-for-otp",
     *      produces={"application/json"},
     *      consumes={"application/x-www-form-urlencoded"},
     *      operationId="requestOtp",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="Choward",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1601270242",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="a52753276c854edfd9a55039afa2731b9b6432bbf7f79568b0148ea27ed8018f",
     *      ),
     *      @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Parameter(
     *          name="vISDCode",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          description="Country code",
     *      ),
     *      @SWG\Parameter(
     *          name="vMobileNumber",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          description="Phone number",
     *      ),
     *     @SWG\Parameter(
     *          name="iUserId",
     *          in="formData",
     *          required=true,
     *          type="integer",
     *          format="int32",
     *          default="1",
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "OTP has been sent successfully",
     *         @SWG\Schema(ref="#/definitions/SuccessResponse"),
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function requestOtp(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->response =  User::requestForOtp($request);
        }
        return $this->response;
    }

    /**
     *  @SWG\Post(
     *      path="/driver/verifyOtp",
     *      tags={"Driver"},
     *      summary="Verify User OTP",
     *      produces={"application/json"},
     *      consumes={"application/x-www-form-urlencoded"},
     *      operationId="verifyOtp",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="choward",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1601270242",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="a52753276c854edfd9a55039afa2731b9b6432bbf7f79568b0148ea27ed8018f",
     *      ),
     *     @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Parameter(
     *          name="vISDCode",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          description="Country code",
     *      ),
     *      @SWG\Parameter(
     *          name="vMobileNumber",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          description="Phone number",
     *      ),
     *      @SWG\Parameter(
     *          name="iOTP",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          description="One Time Password",
     *      ),
     *     @SWG\Parameter(
     *          name="iUserId",
     *          in="formData",
     *          required=true,
     *          type="integer",
     *          format="int32",
     *          default="1",
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "OTP has been verify successfully",
     *         @SWG\Schema(ref="#/definitions/SuccessResponse"),
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function verifyOtp(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->response =  User::verifyOtp($request);
        }
        return $this->response;
    }

    /**
     *  @SWG\Post(
     *      path="/driver/edit-driver-details",
     *      tags={"Driver"},
     *      summary="(Edit Driver Details)",
     *      operationId="edit-driver-details",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1ecd24357fecfa0d4266f715fb732c9b3d75b670803e05394c2495eedee51815"
     *      ),
     *     @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Parameter(
     *          name="vAccessToken",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="Bearer 97c654869e5b264dbdc6a236fce1d213",
     *      ),
     *      @SWG\Parameter(
     *          name="vLicenceFront",
     *          in="formData",
     *          type="string",
     *          description="Front Side",
     *          default="frontLicence.png"
     *      ),
     *      @SWG\Parameter(
     *          name="vLicenceBack",
     *          in="formData",
     *          type="string",
     *          description="Back Side",
     *          default="backLicence.png"
     *      ),
     *      @SWG\Parameter(
     *          name="enVehicleType",
     *          in="formData",
     *          type="string",
     *          description="Vehicle Type",
     *         enum={
     *               "Bike",
     *               "Car"
     *           },
     *          default="Bike",
     *      ),
     *      @SWG\Parameter(
     *          name="vVehicleBrandName",
     *          in="formData",
     *          type="string",
     *          description="Vehicle Brand Name",
     *          default="Honda"
     *      ),
     *      @SWG\Parameter(
     *          name="vVehicleModelName",
     *          in="formData",
     *          type="string",
     *          description="Vehicle Model Name",
     *          default="Hornet"
     *      ),
     *      @SWG\Parameter(
     *          name="vVehicleColor",
     *          in="formData",
     *          type="string",
     *          description="Vehicle Color",
     *          default="Shiny Red"
     *      ),
     * *      @SWG\Parameter(
     *          name="vVehiclePlateNumber",
     *          in="formData",
     *          type="string",
     *          description="Vehicle Plate Number",
     *          default="AB 12 XYZ 34"
     *      ),
     *      @SWG\Parameter(
     *          name="vBankName",
     *          in="formData",
     *          type="string",
     *          description="Bank Name",
     *          default="ABC Bank"
     *      ),
     *      @SWG\Parameter(
     *          name="vAccountHolderName",
     *          in="formData",
     *          type="string",
     *          description="Account Holder Name",
     *          default="Josh Mike"
     *      ),
     *      @SWG\Parameter(
     *          name="vAccountNumber",
     *          in="formData",
     *          type="string",
     *          description="Account Number",
     *          default="9999 888 777 22"
     *      ),
     *      @SWG\Parameter(
     *          name="vIBAN",
     *          in="formData",
     *          type="string",
     *          description="IBAN",
     *          default="888 777 33"
     *      ),
     *      @SWG\Parameter(
     *          name="vRoutingNumber",
     *          in="formData",
     *          type="string",
     *          description="Routing Number",
     *          default="44 8888 2222"
     *      ),
     *      @SWG\Response(
     *         response = 200,
     *         description = "Set profile Successfully",
     *         @SWG\Schema(ref="#/definitions/DriverSignUpResponse"),
     *         examples={
     *              "application/json": {
     *                   "responseCode": 200,
     *                   "responseMessage": "Profile Detail has been saved successfully",
     *                   "responseData": {}
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function setProfile(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->checkAuthorization();
            if ($this->response['responseCode'] == config('constant.OK')) {
                $this->response =  User::signupSetDriverProfile($this->response['iUserId'], $request);
            }
        }
        return $this->response;
    }

    /**
     *  @SWG\Post(
     *      path="/driver/forgot-password",
     *      tags={"Driver"},
     *      summary="Forgot Password",
     *      produces={"application/json"},
     *      consumes={"application/x-www-form-urlencoded"},
     *      operationId="forgotPassword",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="choward",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1601270242",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="a52753276c854edfd9a55039afa2731b9b6432bbf7f79568b0148ea27ed8018f",
     *      ),
     *     @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Parameter(
     *          name="vAccessToken",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="Bearer 97c654869e5b264dbdc6a236fce1d213",
     *      ),
     *     @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          description="Email Id of User",
     *          default="architar.spaceo@gmail.com",
     *      ),
     *      @SWG\Response(
     *         response = 200,
     *         description = "Forgot password response",
     *         @SWG\Schema(ref="#/definitions/SuccessResponse"),
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function forgotPassword(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->response =  User::forgotPassword($request);
        }
        return $this->response;
    }

    /**
     *  @SWG\Post(
     *      path="/driver/signin",
     *      tags={"Driver"},
     *      summary="SignIn",
     *      operationId="signIn",
     *      produces={"application/json"},
     *      consumes={"application/x-www-form-urlencoded"},
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="57f0cb275421171c95294764476df124ebd0b3ccebca603cab5cec795c480238",
     *      ),
     *      @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *     @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          format="string",
     *          description="Email Id of user",
     *          default="architar.spaceo@gmail.com",
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          description="Password of user",
     *          required=true,
     *          type="string",
     *          format="string",
     *          default="12345678",
     *     ),
     *     @SWG\Parameter(
     *         description="DeviceType  1=>Android 2=> IOS 0=> Web",
     *         in="formData",
     *         name="tiDeviceType",
     *         required=true,
     *          type="integer",
     *          format="int32",
     *         default="0",
     *     ),
     *     @SWG\Parameter(
     *         description="Device Token of device",
     *         in="formData",
     *         name="vDeviceToken",
     *         required=true,
     *         type="string",
     *         format="string",
     *         default="",
     *     ),
     *     @SWG\Parameter(
     *          name="vDeviceName",
     *          in="formData",
     *          description="Device Name of device",
     *          required=true,
     *          type="string",
     *          format="string",
     *          default="iPhone X",
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Signin success response",
     *         @SWG\Schema(ref="#/definitions/DriverSignUpResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 200,
     *                      "responseMessage": "login successfully",
     *                  }
     *         }
     *     ),
     *     @SWG\Response(
     *         response = "400",
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function signInDriver(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->response = User::signInDriver($request);
        }
        return $this->response;
    }

    /**
     *  @SWG\Get(
     *      path="/driver/get-cms-pages",
     *      tags={"Driver"},
     *      summary="(Get Cms Pages)",
     *      operationId="get-cms-pages",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1ecd24357fecfa0d4266f715fb732c9b3d75b670803e05394c2495eedee51815"
     *      ),
     *     @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Response(
     *         response = 200,
     *         description = "Success",
     *         @SWG\Schema(ref="#/definitions/CmsPagesResponse"),
     *         examples={
     *              "application/json": {
     *                   "responseCode": 200,
     *                   "responseMessage": "Success",
     *                   "responseData": {}
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function getCmsPages(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            // $this->checkAuthorization();
            // if ($this->response['responseCode'] == config('constant.OK')) {
            // }
            $lang = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : 'en';
            $this->response =  CmsPages::getCmsPages(User::USERTYPE_DRIVER, $lang);
        }
        return $this->response;
    }

    /**
     *  @SWG\Get(
     *      path="/driver/get-faq",
     *      tags={"Driver"},
     *      summary="(Get FAQ's)",
     *      operationId="get-faq",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1ecd24357fecfa0d4266f715fb732c9b3d75b670803e05394c2495eedee51815"
     *      ),
     *     @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Response(
     *         response = 200,
     *         description = "Success",
     *         @SWG\Schema(ref="#/definitions/FAQResponse"),
     *         examples={
     *              "application/json": {
     *                   "responseCode": 200,
     *                   "responseMessage": "Success",
     *                   "responseData": {}
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function getFAQ(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            // $this->checkAuthorization();
            // if ($this->response['responseCode'] == config('constant.OK')) {
            // }
            $lang = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : 'en';
            $this->response =  FAQ::getFAQ($lang);
        }
        return $this->response;
    }
    /**
     *  @SWG\Post(
     *      path="/driver/home",
     *      tags={"Driver"},
     *      summary="(Home)",
     *      operationId="home",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1ecd24357fecfa0d4266f715fb732c9b3d75b670803e05394c2495eedee51815"
     *      ),
     *     @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Parameter(
     *          name="vAccessToken",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="Bearer 97c654869e5b264dbdc6a236fce1d213",
     *      ),
     
     *      @SWG\Response(
     *         response = 200,
     *         description = "Success",
     *         @SWG\Schema(ref="#/definitions/DriverHomeResponse"),
     *         examples={
     *              "application/json": {
     *                   "responseCode": 200,
     *                   "responseMessage": "Success",
     *                   "responseData": {}
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function Home(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->checkAuthorization();
            if ($this->response['responseCode'] == config('constant.OK')) {
                $this->response =  OrderAllocation::getDriverAllocateOrders($this->response['iUserId'], $request);
            }
        }
        return $this->response;
    }
    /**
     *  @SWG\Post(
     *      path="/driver/change-order-status",
     *      tags={"Driver"},
     *      summary="(Change Order Status)",
     *      operationId="change-order-status",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1ecd24357fecfa0d4266f715fb732c9b3d75b670803e05394c2495eedee51815"
     *      ),
     *     @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Parameter(
     *          name="vAccessToken",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="Bearer 97c654869e5b264dbdc6a236fce1d213",
     *      ),
     *     @SWG\Parameter(
     *          name="tiStatus",
     *          in="formData",
     *          required=true,
     *          type="integer",
     *          format="int32",
     *          description= "1 - Accepted",
     *     ),
     *     @SWG\Parameter(
     *          name="iOrderAllocationId",
     *          in="formData",
     *          required=true,
     *          type="integer",
     *          format="int32",
     *     ),
     *     @SWG\Parameter(
     *          name="iSubOrderId",
     *          in="formData",
     *          required=true,
     *          type="integer",
     *          format="int32",
     *     ),
     *     @SWG\Parameter(
     *          name="iOrderId",
     *          in="formData",
     *          required=true,
     *          type="integer",
     *          format="int32",
     *     ),
     *      @SWG\Response(
     *         response = 200,
     *         description = "Set profile Successfully",
     *         @SWG\Schema(ref="#/definitions/SuccessResponse"),
     *         examples={
     *              "application/json": {
     *                   "responseCode": 200,
     *                   "responseMessage": "Profile Detail has been saved successfully",
     *                   "responseData": {}
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function ChangeOrderStatus(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->checkAuthorization();
            if ($this->response['responseCode'] == config('constant.OK')) {
                $lang = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : 'en';
                $this->response =  OrderAllocation::changeOrderStatus($this->response['iUserId'], $request, $lang);
            }
        }
        return $this->response;
    }
    /**
     *  @SWG\Get(
     *      path="/driver/get-decline-reason/{tiReasonType}",
     *      tags={"Driver"},
     *      summary="(Get Decline Reason)",
     *      operationId="get-decline-reason",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1ecd24357fecfa0d4266f715fb732c9b3d75b670803e05394c2495eedee51815"
     *      ),
     *     @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *     @SWG\Parameter(
     *          name="tiReasonType",
     *          in="path",
     *          required=true,
     *          type="integer",
     *          format="int32",
     *          default="1",
     *          description = "0-Decline, 1-Cancel"
     *     ),
     *      @SWG\Response(
     *         response = 200,
     *         description = "Success",
     *         @SWG\Schema(ref="#/definitions/DriverDeclineReasonResponse"),
     *         examples={
     *              "application/json": {
     *                   "responseCode": 200,
     *                   "responseMessage": "Success",
     *                   "responseData": {}
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function getDeclineReason(Request $request, $tiReasonType)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            // $this->checkAuthorization();
            // if ($this->response['responseCode'] == config('constant.OK')) {
            // }
            $lang = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : 'en';
            $this->response =  DeclineReason::getDeclineReason($lang, $tiReasonType);
        }
        return $this->response;
    }

    /**
     *  @SWG\Post(
     *      path="/driver/cancel-decline-order",
     *      tags={"Driver"},
     *      summary="(Cancel Decline Order)",
     *      operationId="cancel-decline-order",
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="12345"
     *      ),
     *       @SWG\Parameter(
     *          name="vAccessToken",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="Bearer 97c654869e5b264dbdc6a236fce1d213",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="1ecd24357fecfa0d4266f715fb732c9b3d75b670803e05394c2495eedee51815"
     *      ),
     *     @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Parameter(
     *          name="iDeclineReasonId",
     *          in="formData",
     *          required=true,
     *          format="int32",
     *          type="integer",
     *          description="Order Decline Reason",
     *          default="2",
     *      ),
     *       @SWG\Parameter(
     *          name="iOrderAllocationId",
     *          in="formData",
     *          required=true,
     *          format="int32",
     *          type="integer",
     *          description="Order Allocation Id",
     *          default="1",
     *      ),
     *       @SWG\Parameter(
     *          name="iSubOrderId",
     *          in="formData",
     *          required=true,
     *          format="int32",
     *          type="integer",
     *          description="Sub Order Id",
     *          default="1",
     *      ),
     *       @SWG\Parameter(
     *          name="tiReasonType",
     *          in="formData",
     *          required=true,
     *          format="int32",
     *          type="integer",
     *          default="0",
     *          description = "0-Decline, 1-Cancel"
     *      ),
     *      @SWG\Parameter(
     *          name="vDescription",
     *          in="formData",
     *          format="string",
     *          type="string",
     *          description="Description",
     *          default="Not possible to Deliver at that time",
     *      ),
     *      @SWG\Response(
     *         response = 200,
     *         description = "Success",
     *         @SWG\Schema(ref="#/definitions/SuccessResponse"),
     *         examples={
     *              "application/json": {
     *                   "responseCode": 200,
     *                   "responseMessage": "Success",
     *                   "responseData": {}
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response = 400,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function cancelDeclineOrder(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->checkAuthorization();
            // echo'<pre>'; print_r($this->response); exit;
            if ($this->response['responseCode'] == config('constant.OK')) {
                $this->response =  DriverDeclinedOrders::cancelDeclineOrder($this->response['iUserId'], $request);
            }
        }
        return $this->response;
    }

    /**
     *  @SWG\Post(
     *      path="/driver/demoApi",
     *      tags={"Driver"},
     *      summary="SignUp",
     *      operationId="demoApi",
     *      produces={"application/json"},
     *      consumes={"application/x-www-form-urlencoded"},
     *      @SWG\Parameter(
     *          name="nonce",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="timestamp",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="123456",
     *      ),
     *      @SWG\Parameter(
     *          name="token",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="57f0cb275421171c95294764476df124ebd0b3ccebca603cab5cec795c480238",
     *      ),
     *      @SWG\Parameter(
     *          name="X-localization",
     *          in="header",
     *          required=false,
     *          type="string",
     *          format="string",
     *           enum={
     *               "en",
     *               "ar"
     *           },
     *          default="en",
     *      ),
     *      @SWG\Parameter(
     *          name="vFullName",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="First Name of user",
     *          default="Archita Rathod",
     *     ),
     *     @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Email Id of user",
     *          default="architar.spaceo@gmail.com",
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Password of user",
     *          default="12345678",
     *     ),
     *     @SWG\Parameter(
     *          name="vISDCode",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Mobile STD Code",
     *          default="+91",
     *     ),
     *     @SWG\Parameter(
     *          name="vMobileNumber",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Mobile Number",
     *          default="9228723322",
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="remember_token",
     *          required=false,
     *          type="string",
     *          description="Accept Terms & Condition ? (1=Accept, 2=Reject)",
     *         	format="string",
     *           enum={
     *               "1",
     *               "2"
     *           },
     *          default="1",
     *     ),
     *     @SWG\Parameter(
     *         description="Device Token of device",
     *         in="formData",
     *         name="vDeviceToken",
     *         required=true,
     *         type="string",
     *         format="string",
     *         default="dfdf",
     *     ),
     *     @SWG\Parameter(
     *         description="DeviceType  1=>Android 2=> IOS 0=> Web",
     *         in="formData",
     *         name="tiDeviceType",
     *         required=true,
     *          type="integer",
     *          format="int32",
     *         default="0",
     *     ),
     *     @SWG\Parameter(
     *          name="vDeviceName",
     *          in="formData",
     *          description="Device Name of device",
     *          required=true,
     *          type="string",
     *          format="string",
     *          default="iPhone X",
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Signup success response",
     *         @SWG\Schema(ref="#/definitions/DriverSignUpResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 200,
     *                      "responseMessage": "signup_success",
     *                  }
     *         }
     *     ),
     *     @SWG\Response(
     *         response = 201,
     *         description = "Bad Request",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *              examples={
     *                  "application/json": {
     *                      "responseCode": 204,
     *                      "responseMessage": "Invalid response."
     *              }
     *         },
     *     ),
     *     @SWG\Response(
     *         response="405",
     *         description="Method Not Allowed",
     *         @SWG\Schema(ref="#/definitions/ErrorResponse"),
     *     ),
     * )
     */
    public function demoApi(Request $request)
    {
        if ($this->response['responseCode'] == config('constant.OK')) {
            $this->response = User::signUpDriver($request);
        }
        return $this->response;
    }
}
