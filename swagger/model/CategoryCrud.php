<?php

namespace App\Models;

use App\Models\response\CategoryCrudResponse;
use App\Models\response\CategoryCrudResponseData;
use App\Models\response\categoryResponsePageData;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Validator;
use App\Models\response\ErrorResponse;

class CategoryCrud extends Model
{
    protected $fillable = ['c_name', 'status', 'order', 'c_file', 'created_at', 'updated_at'];
    protected $table = 'cat';
    protected static function validation($type = '')
    {
        if ($type == 'catInsert') {
            $rules = [];
            $rules = [
                'c_name'       => 'required|unique:cat',
                'status'       => 'required',
                'order'        => 'required',
            ];
            return $rules;
        }
        if ($type == 'catUpdate') {
            $rules = [];
            $rules = [
                'c_name'       => 'required',
                'status'       => 'required',
                'order'        => 'required',
                'id'           => 'required|min:1'
            ];
            return $rules;
        }
        if ($type == 'catAllRecords') {
            $rules = [];
            $rules = [
                'pageno'           => 'required'
            ];
            return $rules;
        }
    }

    //add new category record
    public static function addNewCategory(Request $request)
    {
        try {
            $validate = self::validation('catInsert');
            $validator = Validator::make($request->all(), $validate);
            if ($validator->fails()) {
                return ErrorResponse::withData(config('constant.BAD_REQUEST'), $validator->errors()->first())->showEverything();
            } else {
                $catInsert = new CategoryCrud();
                $catInsert->fill($request->all());
                $catInsert->c_name = $request['c_name'];
                $catInsert->status = $request['status'];
                $catInsert->order = $request['order'];
                $catInsert->c_file = '0';

                if ($catInsert->save()) {
                    $catData = self::where('id', $catInsert->id)->first();
                    return CategoryCrudResponse::withData(config('constant.OK'), trans('response.cat_insert'), $catData)->showEverything();
                } else {
                    return ErrorResponse::withData(config('constant.BAD_REQUEST'), trans('response.invalid_request'))->showEverything();
                }
            }
        } catch (\Exception $e) {
            return ErrorResponse::withData(config('constant.BAD_REQUEST'), $e->getMessage() . ' on line ' . $e->getLine())->showEverything();
        }
    }

    public static function catFindAllRecord(Request $request)
    {
        try {
            $catFind = new CategoryCrud();
            //here get method so we use header we can not access  directly value of get method
            $catRecord = $request->header('category');
            $catFind = self::where('c_name', 'like', '%' . $catRecord . '%')
                ->orWhere('id', 'like', '%' . $catRecord . '%')
                ->orWhere('status', 'like', '%' . $catRecord . '%')
                ->orWhere('order', 'like', '%' . $catRecord . '%')
                ->orWhere('created_at', 'like', '%' . $catRecord . '%')
                ->orWhere('updated_at', 'like', '%' . $catRecord . '%')
                ->get();

            if (!empty($catFind)) {
                return CategoryCrudResponse::withData(config('constant.OK'), trans('response.cat_find'), $catFind)->showEverything();
            } else {
                return ErrorResponse::withData(config('constant.BAD_REQUEST'), trans('response.invalid_request'))->showEverything();
            }
        } catch (\Exception $e) {
            return ErrorResponse::withData(config('constant.BAD_REQUEST'), $e->getMessage() . ' on line ' . $e->getLine())->showEverything();
        }
    }

    public static function catDeleteById($request)
    {
        try {

            $deleteId = $request->header('id');

            if (empty($deleteId) || !is_numeric($deleteId)) {
                return ErrorResponse::withData(config('constant.BAD_REQUEST'), trans('response.somethingWentWrong'))->showEverything();
            } else {

                if (!self::find($deleteId)) {
                    return ErrorResponse::withData(config('constant.BAD_REQUEST'), trans('response.idNotExist'))->showEverything();
                } else {
                    $dataDelete = self::find($deleteId);
                    $dataDelete->delete();
                    return ErrorResponse::withData(config('constant.OK'), trans('response.cat_delete'))->showEverything();
                }
            }
        } catch (\Exception $e) {
            return ErrorResponse::withData(config('constant.BAD_REQUEST'), $e->getMessage() . ' on line ' . $e->getLine())->showEverything();
        }
    }
    public static function catUpdateById($request)
    {
        try {
            $validate = self::validation('catUpdate');
            $validator = Validator::make($request->all(), $validate);
            if ($validator->fails()) {
                return ErrorResponse::withData(config('constant.BAD_REQUEST'), $validator->errors()->first())->showEverything();
            } else {
                $catUpdate = new CategoryCrud();
                $id = $request->id;
                if (!empty($id) && $id != 0) {

                    $catUpdate->fill($request->all());

                    $catUpdate::where('id', $id)->update([
                        'c_name' => $request->c_name,
                        'status' => $request->status,
                        'order' => $request->order
                    ]);


                    if (!self::find($id)) {
                        return ErrorResponse::withData(config('constant.BAD_REQUEST'), trans('response.idNotExist'))->showEverything();
                    } else {

                        $catUpdatedId = self::where('id', '=', $id)->get();
                        return CategoryCrudResponse::withData(config('constant.OK'), trans('response.cat_update'), $catUpdatedId)->showEverything();
                    }
                } else {
                    return ErrorResponse::withData(config('constant.BAD_REQUEST'), $validator->errors()->first())->showEverything();
                }
            }
        } catch (\Exception $e) {
            return ErrorResponse::withData(config('constant.BAD_REQUEST'), $e->getMessage() . ' on line ' . $e->getLine())->showEverything();
        }
    }
    public static function allCategoryRecordsWithPagination($request)
    {
        try {
            $validate = self::validation('catAllRecords');
            $validator = Validator::make($request->all(), $validate);
            if ($validator->fails()) {
                return ErrorResponse::withData(config('constant.BAD_REQUEST'), $validator->errors()->first())->showEverything();
            } else {
                $pageNo = $request->pageno;
                if (!empty($pageNo) && $pageNo != 0) {
                    $totalRecords = self::count();
                    if ($totalRecords <= 0) {
                        return ErrorResponse::withData(config('constant.NOT_FOUND'), trans('response.no_data_found'))->showEverything();
                    } else {
                        $dataList = [];
                        $allCategoryRecords = self::selectRaw('cat.*')->offset(($pageNo - 1) * siteConfig('API_PAGINATION_LIMIT'))->limit(siteConfig('API_PAGINATION_LIMIT'))->get();
                        if (!empty($allCategoryRecords)) {
                            foreach ($allCategoryRecords as $value) {
                                $dataList[] = CategoryCrudResponseData::withData(
                                    $value->id,
                                    $value->c_name,
                                    $value->status,
                                    $value->order,
                                    $value->c_file,
                                    $value->created_at,
                                    $value->updated_at
                                )->showEverything();
                            }
                        }

                        $ResponseData = categoryResponsePageData::withData(!empty($totalRecords) ? $pageNo : 0, ceil($totalRecords / siteConfig('API_PAGINATION_LIMIT')), (int) $totalRecords, (int) siteConfig('API_PAGINATION_LIMIT'), $dataList)->showEverything();

                        return CategoryCrudResponse::withData(200, trans('response.success'), $ResponseData)->showEverything();
                    }
                } else {
                    return ErrorResponse::withData(config('constant.BAD_REQUEST'), trans('response.somethingWentWrong'))->showEverything();
                }
            }
        } catch (\Exception $e) {
            return ErrorResponse::withData(config('constant.BAD_REQUEST'), $e->getMessage() . ' on line ' . $e->getLine())->showEverything();
        }
    }
}
