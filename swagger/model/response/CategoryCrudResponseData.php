<?php

namespace App\Models\response;

 
/**
 * @SWG\Definition(
 *   type="object",
 *   @SWG\Xml(name="CategoryCrudResponseData")
 * )
 */

class CategoryCrudResponseData 
{
    
    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    private $id;

    /**
     * @SWG\Property(format="string")
     * @var string
     */
    private $c_name;

    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    private $status;

    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    private $order;

    /**
     * @SWG\Property(format="string")
     * @var int
     */
    private $c_file;

    /**
     * @SWG\Property(format="string")
     * @var int
     */
    private $created_at;

    /**
     * @SWG\Property(format="string")
     * @var int
     */
    private $updated_at;

    
    function getId()
    {
        return $this->id;
    }
    function getCategoryName()
    {
        return $this->c_name;
    }
    function getCategoryStatus()
    {
        return $this->status;
    }
    function getCategoryOrder()
    {
        return $this->order;
    }
    function getCategoryFile()
    {
        return $this->c_file;
    }
    function getCategoryCreatedDate()
    {
        return $this->created_at;
    }
    function getCategoryUpdatedDate()
    {
        return $this->updated_at;
    }

    function setId($id)
    {
        $this->id = $id;
    }
    function setCategoryName($c_name)
    {
        $this->c_name = $c_name;
    }
    function setCategoryStatus($status)
    {
        $this->status = $status;
    }
    function setCategoryOrder($order)
    {
        $this->order = $order;
    }
    function setCategoryFile($c_file)
    {
        $this->c_file = $c_file;
    }
    function setCategoryCreatedDate($created_at)
    {
        $this->created_at = $created_at;
    }
    function setCategoryUpdatedDate($updated_at)
    {
        $this->updated_at = $updated_at;
    }
    
    public static function withData(
        $id,
        $c_name,
        $status,
        $order,
        $c_file,
        $created_at,
        $updated_at
    )
    {
        $instance = new self();
        $instance->setId($id);
        $instance->setCategoryName($c_name);
        $instance->setCategoryStatus($status);
        $instance->setCategoryStatus($order);
        $instance->setCategoryFile($c_file);
        $instance->setCategoryCreatedDate($created_at);
        $instance->setCategoryUpdatedDate($updated_at);
        return $instance;;
    }
    public function showEverything()
    {
        return get_object_vars($this);
    }

}
