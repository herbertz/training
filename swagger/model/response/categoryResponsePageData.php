<?php

namespace App\Models\response;

/**
 * @SWG\Definition(
 *   type="object",
 *   @SWG\Xml(name="categoryResponsePageData")
 * )
 */
class categoryResponsePageData
{

    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    private $currentPage;

    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    private $totalPage;

    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    private $totalRecords;

    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    private $pageOffset;

    /**
     * @var CategoryCrudResponseData
     * @SWG\Property(type="array", @SWG\Items(ref="#/definitions/CategoryCrudResponseData"))
     */
    private $testList;

    function getCurrentPage()
    {
        return $this->currentPage;
    }

    function getTotalPage()
    {
        return $this->totalPage;
    }

    function getTotalRecords()
    {
        return $this->totalRecords;
    }

    function getPageOffset()
    {
        return $this->pageOffset;
    }

    function getTestList()
    {
        return $this->testList;
    }

    function setCurrentPage($currentPage)
    {
        $this->currentPage = (int)$currentPage;
    }

    function setTotalPage($totalPage)
    {
        $this->totalPage = (int)$totalPage;
    }

    function setTotalRecords($totalRecords)
    {
        $this->totalRecords = (int)$totalRecords;
    }

    function setPageOffset($pageOffset)
    {
        $this->pageOffset = (int)$pageOffset;
    }

    function setTestList($testList)
    {
        $this->testList = $testList;
    }

    public static function withData($currentPage, $totalPage, $totalRecords, $pageOffset, $testList)
    {
        $instance = new self();
        $instance->setCurrentPage($currentPage);
        $instance->setTotalPage($totalPage);
        $instance->setTotalRecords($totalRecords);
        $instance->setPageOffset($pageOffset);
        $instance->setTestList($testList);
        return $instance;;
    }

    public function showEverything()
    {
        return get_object_vars($this);
    }
}
